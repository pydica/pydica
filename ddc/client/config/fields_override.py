# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from ddc.client.config.fields_ini import *

# example:
"""
Gebuehr_Frei.bbox = (825, 63, 70, 775)
Gebuehr_Frei.i18n = 'Gebühr frei'
Gebuehr_Frei.coord_down = False
"""

# this link name cannot be computed by field_name.upper()
Arbeitsunfall.link_name = 'ARB_UNFALL'

# this link name cannot be computed by field_name.upper()
Gebuehr_Pflichtig.link_name = 'GEBUEHR_PFL'
