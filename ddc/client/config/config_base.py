#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

'''
Base for field definitions.
It keeps the configuration files as clean as possible
by using a meta class for the field order.
'''

import io
import os
import sys

from six import with_metaclass

import ddc

from collections import namedtuple
c = namedtuple('Coordinates', ('left', 'top', 'right', 'bottom'))


__all__ = ['FieldList']

_fielddef_list = []

class _FieldMeta(type):
    ''' provides automatic field order '''

    def __new__(_mcs, _name, _bases, _dict):
        # cannot use class names here before they are created
        if _name in ('_Down', '_Up', 'FieldBase', '_Helper_Py3', 'NewBase'):
            # NewBase comes from module 'six'
            return type.__new__(_mcs, _name, _bases, _dict)

        ''' the trick in this metaclass is to mark the locals which should
        not be included into the generated class. See the following lines.
        The body of the new class goes from "here" to "there".
        '''

        _keep = 0
        _type = type
        _remcls = (_Up, _Down)
        _ign = 0
        _remove = set(locals()).union({'_remove'})

        # ----- 8< the real class goes "from here"...

        coord_down = _Down in _bases
        field_name = _name.lower()
        link_name = _name.upper()

        def __init__(self, parent):
            'this is a member of a FieldList. It dynamically computes its coordinate system.'
            self.parent = parent
            #self.coord_down = coord_down
            # we even can avoid this, making the instance as tiny as possible

        # ----- 8<  ..."to there"

        # now we get rid of the classes which only stand for a constant
        for _ign in _remcls:
            if _ign in _bases:
                _bases = _ign.__mro__ + _bases
                _bases = list(_bases)
                while _ign in _bases:
                    _bases.remove(_ign)
        _bases = tuple(_bases)

        _keep = dict((key, value) for (key, value) in list(locals().items())
                     if key not in _remove )
        _keep.update(_dict)
        cls = _type.__new__(_mcs, _name, _bases, _keep)

        # we can update properties now, but they are already defined in FieldsBase

        _fielddef_list.append(cls)
        return cls

class FieldMetric(object):
    'basic calculations for convenient properties'

    @property
    def rect(self):
        left, top, right, bottom = self.bbox
        width = right - left
        if self.coord_down:
            return c(left, top, width, bottom-top)
        else:
            parent_width, parent_height = self.parent.size
            return c(left, parent_height-top, width, top-bottom)

    @property
    def x(self):
        return self.bbox[0]

    @property
    def y(self):
        y = self.bbox[1]
        if self.coord_down:
            return y
        else:
            height = self.parent.size[1]
            return height-y

    @property
    def w(self):
        left, top, right, bottom = self.bbox
        return right-left

    @w.setter
    def w(self, w):
        left, top, right, bottom = self.bbox
        right = left + w
        self.bbox = left, top, right, bottom

    @property
    def h(self):
        left, top, right, bottom = self.bbox
        if self.coord_down:
            return bottom-top
        else:
            return top-bottom

    @h.setter
    def h(self, h):
        left, top, right, bottom = self.bbox
        # we cut the box from the bottom
        if self.coord_down:
            bottom = top + h
        else:
            bottom = top - h
        self.bbox = left, top, right, bottom

    @property
    def name(self):
        return self.rec.name

    @property
    def value(self):
        return self.rec.corrected_result

    @value.setter
    def value(self, newval):
        self.update_rec(corrected_result=newval)


class _Helper_Py3(with_metaclass(_FieldMeta, FieldMetric)):
    ''' helper class for python 2/3 compatibility '''

class FieldBase(_Helper_Py3):
    'fields automatically inserted into FieldList'

class _Up(FieldBase):
    'coordinate system used by FIELDS.INI'

class _Down(FieldBase):                           # Qt coordinates'
    'coordinate system used by APO_FSRH.INI'


# lazy initialization of this module to avoid circular import.
# the trick is to replace this module by an instance!
# modelled after a post from Alex Martelli :-)
# http://stackoverflow.com/questions/1462986/lazy-module-variables-can-it-be-done

class _Sneaky(object):
    def __init__(self, name):
        self.module = sys.modules[name]
        sys.modules[name] = self
        self.initializing = True

    def __getattr__(self, name):
        # call module.__init__ after import introspection is done
        if self.initializing and not name[:2] == '__' == name[-2:]:
            self.initializing = False
            __init__(self.module)
        return getattr(self.module, name)

_Sneaky(__name__)

def __init__(module):
    mypath = os.path.dirname(__file__)
    fields_ini_path = os.path.join(mypath, 'fields_ini.py')
    if not os.path.exists(fields_ini_path):
        # generate on the fly before we have finally agreed on it
        from ddc.client.config.initool import generate
        try:
            with io.open(fields_ini_path, 'w', encoding='utf-8') as outfile:
                generate(outfile)
        except AssertionError:
            os.remove(fields_ini_path)
            raise
    import ddc.client.config.fields_override
    assert ddc.client.config.fields_ini._Up, """
        missing structure in fields_ini.py"""


class FieldList(list):
    """ a list of instantiated fields with calculated parameters.
    Can be used in a GUI app with given image dimensions
    """

    def __init__(self, size):
        super(FieldList, self).__init__(self)
        self.size = size

        for fielddef in _fielddef_list:
            self.append(fielddef(self))

if __name__ == '__main__':
    # this is duplication as __main__.
    # for convenience of testing, we fetch the right globals.
    from ddc.client.config.config_base import *
    field_list = FieldList((824, 1248))

