# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from babel.support import LazyProxy


__all__ = ['ALL_FIELD_NAMES']

# Actually the field definition should not be part of pydica. If we follow the
# intended architecture specific fields should be provided by a
# customer-specific plugin.
#
# Hence this module is quite a hack but that's how the code was built in the
# beginning and right now (November 2015) I can't justify to spending time to
# clean this up.
#
# At least it's now easier to get a list of known fields without duplicating
# code.

def all_field_names():
    # prevent recursive imports triggered by CollectionViewer
    from ddc.client.config.config_base import FieldList
    field_names = [field_class.link_name for field_class in FieldList(None)]
    return tuple(field_names)

# all caps to indicate a "constant": Defined fields must not change after
# initial loading.
ALL_FIELD_NAMES = LazyProxy(all_field_names, enable_cache=True)
