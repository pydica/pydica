#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os
import configparser
import ddc

import pkg_resources

def read_data(fname, encoding='cp850'):
    parser = configparser.RawConfigParser()
    parser.read(fname, encoding=encoding)
    assert parser.sections(), """missing field definitions"""
    return parser

def analyze():

    class Viewer:
        pass

    def mkview(items):
        view = Viewer()
        view.__dict__ = dict(items)
        return view

    def get_bbox(obj):
        bbox = obj.left, obj.top, obj.right, obj.bottom
        return tuple(map(int, bbox))

    INI_PATH = pkg_resources.resource_filename('srw.pydica.fields', 'FIELDS.INI')
    ini_data = read_data(INI_PATH)
    APO_PATH = pkg_resources.resource_filename('srw.pydica.fields', 'APO_FSRH.ini')
    apo_data = read_data(APO_PATH)

    # getting the field order
    field_order = ini_data.items('FIELD_ORDER')
    field_order.sort()
    field_order = list(zip(*field_order))[1]

    # turning the ini into a list of nicely visible objects
    field_list = []
    for section in field_order:
        data = mkview(ini_data.items(section))
        data._classname_ = section
        field_list.append(data)

    # building a lookup for apo fields
    apo_lookup = {}
    for section in apo_data.sections():
        data = mkview(apo_data.items(section))
        if hasattr(data, 'name'):
            data._nicename_ = section
            apo_lookup[data.name] = data

    # now generating the output list
    outlist = []
    errors = 0
    for field in field_list:
        res = mkview(())

        # in this area, the used fields are created from the given values.
        # for example see i18n:
        res.i18n = field.name

        # here is the field name extracted, either as exact name from apo_lookup
        # or computed by some heuristics.
        name = field._classname_
        if name in apo_lookup:
            apo_rec = apo_lookup[name]
            res.classname = apo_rec._nicename_
        else:
            # make uppercase after '_'
            guess = name.split('_')
            res.classname = '_'.join(x.capitalize() for x in guess)

        # the bbox is a bit complicated because of different coordinate systems
        bbox = get_bbox(field)
        if bbox == (0, 1, 1, 0):
            if name not in apo_lookup:
                print('problem', name, file=sys.stderr)
                errors += 1
                continue
            apo_rec = apo_lookup[name]
            res.bbox = get_bbox(apo_rec)
            res.coord = '_Down'
        else:
            res.bbox = bbox
            res.coord = '_Up'

        # correction is simply taken as-is.
        res.correction = field.correction

        # more fields should be computed and added here as needed.
        # don't forget to insert them as well into the class generation below.
        # ...

        outlist.append(res)

    if errors:
        raise ValueError('there are errors. please correct the files')

    return outlist

def generate(f):
    import time
    outlist = analyze()
    print('''\
# -*- {coding}: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from collections import namedtuple
c = namedtuple('Coordinates', ('left', 'top', 'right', 'bottom'))

# field definition, generated on {ctime}

# --- do not modify, use fields_override.py ---
# example:
"""
Gebuehr_Frei.bbox = c(left=825, top=63, right=70, bottom=775)
Gebuehr_Frei.i18n = 'Gebühr frei'
Gebuehr_Frei.coord_down = False
"""

from .config_base import _Down, _Up

'''.format(ctime=time.ctime(), coding='coding'), file=f) # avoid parser complaints

    for elem in outlist:
        baseclass = elem.coord

        print(('class ' + elem.classname).ljust(25) + '({baseclass}):'.format(
            baseclass = baseclass), file=f)
        bbox_def = 'c(left=%d, top=%d, right=%d, bottom=%d)' % elem.bbox
        print('    bbox = ' + bbox_def, file=f)
        print('    i18n = ' + repr(elem.i18n), file=f)
        print('    correction = ' + elem.correction, file=f)
        print('', file=f)

    print('', file=f)
    print('# --- end of auto-generated file ---', file=f)

if __name__ == '__main__':
    ini_module_path = os.path.join(ddc.rootpath, 'ddc', 'client', 'config', 'fields_ini.py')
    with open(ini_module_path, 'w') as fp:
        generate(fp)

