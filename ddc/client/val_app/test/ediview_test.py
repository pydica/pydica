# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os
import sys

from pycerberus.api import Validator
from pythonic_testcase import *

from ddc.client.config import ALL_FIELD_NAMES
from ddc.client.val_app.ediview import initialize_ediview
from ddc.client.val_app.gui.collection_viewer import CollectionViewer
from ddc.client.val_app.testutil import fake_controller, fake_gui
from ddc.storage.testhelpers import create_cdb_and_ibf_file, use_tempdir
from ddc.validation.testutil import build_context, fake_plugin


class EdiviewTest(PythonicTestCase):
    def setUp(self):
        super(EdiviewTest, self).setUp()
        self._old_handler = self._replace_excepthook()
        # we need to keep the result from initialize_ediview() to keep the
        # CDB_Reader instace alive (see comment in "initialize_ediview()").
        self._components = None

    def tearDown(self):
        sys.excepthook = self._old_handler
        super(PythonicTestCase, self).tearDown()

    def _replace_excepthook(self):
        # PySide installs a sys.excepthook handler which will just print
        # exceptions to stdout (but won't cause a test error). This handler
        # ensures that exceptions are reported as "errors" (default behavior
        # as if PySide wouldn't be involved at all).
        def exception_reporter(exc_class, exception, traceback):
            outcome = self._outcome
            exc_info = (exc_class, exception, traceback)
            outcome.errors.append((None, exc_info))
        old_handler = sys.excepthook
        sys.excepthook = exception_reporter
        return old_handler

    # --- tests ---------------------------------------------------------------
    def test_can_initialize_ediview(self):
        # this should prevent the most obvious errors in ediview so I don't
        # have run it manually
        pydica_context = self._context()
        gui = fake_gui()
        ediview_controller = fake_controller(CollectionViewer, pydica_context, gui=gui)
        ediview = initialize_ediview(pydica_context, controller=ediview_controller)
        assert_not_none(ediview)

        viewer = ediview[1]
        # Close the batch so it can be opened again by other tests on Windows
        viewer.close_current_batch(store_modified_data=False)

    def _setup_gui(self, env_dir=None):
        pydica_context = self._context(env_dir=env_dir)
        gui = fake_gui()
        ediview_controller = fake_controller(CollectionViewer, pydica_context, gui=gui)
        self._components = initialize_ediview(pydica_context, controller=ediview_controller)
        remote_control = self._components[2]
        return (ediview_controller, remote_control)

    def test_can_display_prescription_with_remote_control(self):
        pic_nrs = (
            '12345600100024',
            '12345600114024',
            '12345600130024',
        )

        with use_tempdir() as env_dir:
            cdb_path = os.path.join(env_dir, 'foo.cdb')
            create_cdb_and_ibf_file(cdb_path, pic_nrs=pic_nrs)
            (ediview_controller, rc) = self._setup_gui(env_dir)

            form_index = 1
            cmd = self._opendel(cdb_path, pic_nrs[form_index], form_nr=form_index+1)
            self._process_cmd(rc, cmd, env_dir)

            batch_state = ediview_controller._batch_state
            assert_equals(cdb_path, batch_state.batch.bunch.cdb,
                message='ensure the generated CDB fixture is open/active')
            assert_equals(1, batch_state.current_form.index)

            # Close the batch so it can be opened again by other tests on Windows
            ediview_controller.close_current_batch(store_modified_data=False)

    def _process_cmd(self, remote_control, cmd, env_dir):
        self._send_rc_command(cmd, env_dir)
        # in production the remote control would do polling but we can
        # just run the actual code (so we can skip the timer but all the
        # other code paths should be identical).
        remote_control.do_action()

    def _opendel(self, cdb_path, pic, *, form_nr=1):
        return 'OPENDEL %s;%d;%s\n' % (cdb_path, form_nr, pic)

    def _delete(self, cdb_path, pic):
        form_nr = 42
        return 'DELETE %s;%d;%s\n' % (cdb_path, form_nr, pic)

    def test_can_delete_prescription_with_remote_control(self):
        pic_nrs = (
            '12345600100024',
            '12345600114024',
            '12345600130024',
        )
        form_index = 1
        deletion_pic = pic_nrs[form_index]
        # This would be a serious mistake in production (duplicate PIC) but
        # is fine for testing: First we need to make sure that the file
        # name is checked by the remote control (even if the PIC matches).
        bar_pic = deletion_pic

        with use_tempdir() as env_dir:
            cdb_path = os.path.join(env_dir, 'foo.cdb')
            create_cdb_and_ibf_file(cdb_path, pic_nrs=pic_nrs)
            bar_path = os.path.join(env_dir, 'bar.cdb')
            create_cdb_and_ibf_file(bar_path, pic_nrs=[bar_pic])
            (ediview_controller, rc) = self._setup_gui(env_dir)

            initial_cmd = self._opendel(cdb_path, pic_nrs[0])
            self._process_cmd(rc, initial_cmd, env_dir)
            batch_state = ediview_controller._batch_state
            assert_equals(cdb_path, batch_state.batch.bunch.cdb,
                message='ensure the generated CDB fixture is open/active')

            del_cmd_in_bar = self._delete(bar_path, deletion_pic)
            self._process_cmd(rc, del_cmd_in_bar, env_dir)
            batch_state = ediview_controller._batch_state
            assert_equals(cdb_path, batch_state.batch.bunch.cdb)
            assert_false(batch_state.batch.form(form_index).is_deleted())

            first_pic = pic_nrs[0]
            open_cmd = self._opendel(cdb_path, first_pic)
            self._process_cmd(rc, open_cmd, env_dir)
            # form to delete (deletion_pic) is #2 but form #1 is displayed
            self._process_cmd(rc, self._delete(cdb_path, deletion_pic), env_dir)
            batch_state = ediview_controller._batch_state
            assert_equals(cdb_path, batch_state.batch.bunch.cdb,
                message='ensure the generated CDB fixture is open/active')
            form = batch_state.batch.form(form_index)
            # unfortunately .pic_nr is always '' here due to incomplete fixture
            # generators
            # assert_equals(first_pic, form.pic_nr)
            assert_false(form.is_deleted())
            user_error = ediview_controller.gui._state.msgboxes.pop()
            assert_equals('critical', user_error[0])
            assert_contains(deletion_pic, user_error[2])

            # ensure form #2 is displayed
            cmds = ''.join([
                self._opendel(cdb_path, deletion_pic),
                self._delete(cdb_path, deletion_pic)
            ])
            self._process_cmd(rc, cmds, env_dir)
            batch_state = ediview_controller._batch_state
            batch = batch_state.batch
            assert_equals(cdb_path, batch.bunch.cdb,
                message='ensure the generated CDB fixture is open/active')
            form = batch.form(form_index)
            assert_equals('DELETED', form.pic_nr)
            assert_true(form.is_deleted())
            # again: our fixture generators are not good enough to check this...
            # assert_equals(deletion_pic, batch.pic_for_form(form_index))
            assert_length(0, ediview_controller.gui._state.msgboxes)

            # Close the batch so it can be opened again by other tests on Windows
            ediview_controller.close_current_batch(store_modified_data=False)

    # --- internal helpers ----------------------------------------------------
    def _context(self, *, env_dir=None):
        validators = []
        for name in ALL_FIELD_NAMES:
            validators.append((name, Validator(required=False)))
        plugin = fake_plugin(dict(validators))
        pydica_context = build_context(plugin, env_dir=env_dir)
        if env_dir:
            pydica_context.settings['control_path'] = env_dir
        return pydica_context

    def _send_rc_command(self, cmd, control_dir):
        cmd_path = os.path.join(control_dir, 'PYD_REM.TXT')
        ctrl_path = os.path.join(control_dir, 'PYD_REM.OK')
        with open(cmd_path, 'w') as fp:
            fp.write(cmd)
        with open(ctrl_path, 'w') as fp:
            fp.write('\n')
