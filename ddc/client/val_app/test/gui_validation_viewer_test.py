# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os
import shutil
from tempfile import mkdtemp

from pycerberus.api import Validator
from pythonic_testcase import *
from srw.rdblib import guess_path, DataBunch
from srw.rdblib.cdb import create_cdb_with_dummy_data
from srw.rdblib.ibf import create_ibf

from ddc.client.config import ALL_FIELD_NAMES
from ddc.client.val_app.__main__ import ValidationViewer
from ddc.client.val_app.testutil import fake_controller, fake_gui
from ddc.storage import Batch
from ddc.tool.cdb_collection import CDB_Collection
from ddc.validation.testutil import build_context, fake_plugin


class GUITest(PythonicTestCase):
    def setUp(self):
        super(GUITest, self).setUp()
        self.temp_dir = mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.temp_dir)
        super(GUITest, self).tearDown()

    # --- tests ---------------------------------------------------------------


    def test_open_sequence_happy_path(self):
        # mostly some kind of "smoke test" just to verify we can have "unit"
        # tests checking functionality in CollectionViewer
        batch = self._create_batch_in_tempdir()
        validators = []
        for name in ALL_FIELD_NAMES:
            validators.append((name, Validator(required=False)))
        plugin = fake_plugin(dict(validators))
        pydica_context = build_context(plugin)
        seq = CDB_Collection(batch)

        gui = fake_gui()
        controller = fake_controller(ValidationViewer, pydica_context, gui=gui)
        controller.open_sequence(seq)

        batch_state = controller._batch_state
        assert_equals(batch, batch_state.batch)
        assert_equals(0, batch_state.current_form.index)
        # close all open files - otherwise Windows won't be able to remove
        # the temp dir
        batch.close()

    def _create_batch_in_tempdir(self, rdb=False):
        nr_forms = 2
        extension = 'RDB' if rdb else 'CDB'
        xdb_path = os.path.join(self.temp_dir, '00042100.'+extension)
        ibf_path = guess_path(xdb_path, type_='ibf')
        create_cdb_with_dummy_data(nr_forms=nr_forms, filename=xdb_path, field_names=ALL_FIELD_NAMES)
        create_ibf(
            nr_images=nr_forms,
            filename=ibf_path,
            fake_tiffs=False,
            create_directory=True
        )
        bunch = DataBunch(cdb=xdb_path, ibf=ibf_path, db=None, ask=None)
        batch = Batch.init_from_bunch(bunch, create_persistent_db=True)
        return batch
