# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from ddc.lib.attribute_dict import AttrDict
from ddc.lib.progress_utils import SuppressProgress

from srw.pydica.fields import CHECKBOXES


__all__ = ['fake_controller', 'fake_signal']

def fake_action():
    return AttrDict(
        setDisabled = lambda v: None,
    )

def fake_button():
    return AttrDict(
        clicked=AttrDict(
            connect=lambda action: None,
        ),
        setToolTip=lambda s: None,
    )

def fake_menu():
    return AttrDict(
        addAction=lambda action: None,
    )

def fake_signal():
    return AttrDict(connect=lambda handler: None)

def fake_widget():
    return AttrDict(
        # value-related
        setCheckState = lambda state: None,
        isEnabled = lambda: True,
        setReadOnly = lambda ro: None,
        setText = lambda text: None,

        # styles
        style_name = None,
        setStyleSheet = lambda style: None,

        # events/signals
        textEdited = fake_signal(),
        getting_touched = fake_signal(),
        focusOutEvent = lambda event: None,

        # API
        hasFocus = lambda: False,
        setFocus = lambda: None,
        selectAll = lambda: None,
    )


pixmap = AttrDict(
    width=lambda: 1192,
    height=lambda: 832,
)

def fake_qt_instance():
    return AttrDict(
        processEvents=lambda: None,
    )

def fake_progress_widget(progress_reporter=None):
    if progress_reporter is None:
        progress_reporter = lambda qt_app: SuppressProgress()

    return AttrDict(
        is_cancelled=False,
        set_max_value=lambda max_value: None,
        show=lambda worker: worker(),
        update_progress=lambda i, details=None: None,
        exec_=lambda: None,
        progress_reporter=progress_reporter,
    )

def fake_gui(progress_reporter=None):
    msgboxes = []
    return AttrDict(
        _state = AttrDict(msgboxes=msgboxes),

        # simple attributes
        aspect_factor = 1.0,

        # GUI references
        mirror_edit = AttrDict(mirror=lambda widget: None),
        text_page=AttrDict(font_scaled=fake_signal()),
        text_scrollarea = AttrDict(ensureWidgetVisible=lambda widget: None),
        image_scrollarea = None,
        image_label=AttrDict(painter=None, pixmap=lambda: pixmap),
        zoom_scrollarea = None,
        zoom_label=AttrDict(painter=None, setPixmap=lambda pixmap: None),
        prev_button=fake_button(),
        next_button=fake_button(),

        # actions/menus
        deleteFormAct = fake_action(),
        undeleteFormAct = fake_action(),
        viewMenu=fake_menu(),

        # "public" API
        open=lambda img: None,
        ensure_text_visible=lambda: None,
        # stubbed out because the GridBuilder has a lot of more GUI references
        # and we can test most of the interesting code without the GUI builder
        # (connecting fields+widgets is done in the FakeController).
        initialize_grid=lambda field_list, keypress_filter: None,
        set_field_info = lambda field_name, error_msg: None,
        msgbox_info = lambda title, msg: msgboxes.append(('information', title, msg)),
        msgbox_warning = lambda title, msg: msgboxes.append(('warning', title, msg)),
        msgbox_critical = lambda title, msg: msgboxes.append(('critical', title, msg)),

        # GUI testing mocks
        qt_app=lambda: fake_qt_instance(),
        progress_window=lambda: fake_progress_widget(progress_reporter),

        # unsorted...
        set_display_validation=lambda v: None,
        set_validation_interaction=lambda v: None,
        display_validation=None,
        recipe_no_edit=AttrDict(returnPressed=fake_signal()),
        stackedWidget=AttrDict(setCurrentIndex=lambda nr: None),
        num_recipies=AttrDict(setText=lambda s: None),
        repaint=lambda: None,
        set_form_info=lambda pic=None, form_index=None, filename=None: None,
        setWindowTitle=lambda s: None,
        title_text=None,
    )


def fake_controller(viewer_class, *args, **kwargs):
    class FakeController(viewer_class):
        def create_actions(self):
            pass

        def create_menus(self):
            pass

        def _setup_focus_timer(self):
            class State(object):
                def __init__(self, value=None):
                    self.value = value

                def set(self, value):
                    self.value = value

            _state = State()
            return AttrDict(
                start = lambda: _state.value(),
                timeout=AttrDict(
                    connect = lambda handler: _state.set(handler),
                    disconnect = lambda: None,
                ),
            )

        def _initialize_field_model(self, *args):
            # fields must be connected to a widget but I'd like to avoid calling the
            # GridBuilder (see fake_gui()) which touches a lot of GUI code.
            field_list = super(FakeController, self)._initialize_field_model(*args)
            for field in field_list:
                field.is_checkbox = (field.name in CHECKBOXES)
                field.is_lineedit = not field.is_checkbox
                field.widget = fake_widget()
            return field_list

    return FakeController(*args, **kwargs)

