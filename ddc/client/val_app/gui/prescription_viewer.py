# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os

from PySide import QtCore, QtGui

from ddc.lib.attribute_dict import AttrDict
from .dialog.progressdialog import ProgressWindow
from .fieldpainting import FieldPainter
from .gridbuilder import GridBuilder
from .msgbox import msgbox_ok_cancel
from .ui_mainwindow import Ui_MainWindow


__all__ = ['PrescriptionViewer']

class PrescriptionViewer(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None, *, redirector=None):
        super(PrescriptionViewer, self).__init__(parent)
        self._redirector = redirector
        self.text_grid = None

        self.setupUi(self)

        # program logo, for the fun of it
        self.setWindowIcon(QtGui.QIcon(':/newPrefix/logo icon.png'))

        self.scale_factor = 1.0
        self.aspect_factor = 1.0
        # alignment of the image label
        # XXX make this an option
        align = QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft
        text_align = QtCore.Qt.AlignTop | QtCore.Qt.AlignRight

        self.image_label.setAlignment(align)
        self.zoom_label.setAlignment(align)

        self.image_scrollarea.setBackgroundRole(QtGui.QPalette.Dark)
        self.zoom_scrollarea.setBackgroundRole(QtGui.QPalette.Dark)
        self.image_scrollarea.setAlignment(align)
        self.zoom_scrollarea.setAlignment(align)

        self.text_scrollarea.setAlignment(text_align)
        self.text_page = self.page_0 # use this name from now on
        self.text_page.scroll_area = self.text_scrollarea

        self.menus = AttrDict()
        self.actions = AttrDict()
        self._forminfo_labels = self.setup_forminfo_labels()

        # set a default screen size
        # XXX make this an option / use setting from last time?
        self.resize(1024, 768)

        self.createActions()
        self.createMenus()

        self.fitToWindow()

        self.title_text = "DSZ Client Validator"
        self.setWindowTitle(self.title_text)

        #desk = QtGui.QApplication.desktop()
        '''
        Unfortunately, we cannot use the dpi info from Qt or even Mac OS.
        This needs to be configured per device.
        Right now I'm using the exact data for the Thunderbolt display.
        '''
        self.screen_dpi = 108.79 # 27" Mac
        # in order to get this generally correct, the 'edid' must be
        # found and parsed. Will do that, soon.

        img_dpi = 200 # just a guess for now, adjusted after open()
        self.init_scale = float(self.screen_dpi) / img_dpi

        # setting a good scale increment that meets a DIN fraction
        # and happens to be the equal-temperament tuning
        self.scale_delta = 2**(1./12)   # or 1200 cent

    def initialize_grid(self, field_list, keypress_filter):
        self.text_grid = GridBuilder(
            field_list,
            self.text_page,
            eventfilter_textfields=keypress_filter
        )

    def setup_forminfo_labels(self):
        status_left = QtGui.QLabel()
        status_mid = QtGui.QLabel()
        status_right = QtGui.QLabel()
        self.statusBar.addWidget(status_left)
        self.statusBar.addWidget(status_mid)
        self.statusBar.addWidget(status_right)
        return AttrDict(
            status_left=status_left,
            status_mid=status_mid,
            status_right=status_right
        )



    # Tab handling is a bit tricky because it is intercepted
    # internally.
    # for now, we don't modify the tab movements,
    # but we make sure that the image gets a refresh.

    def focusNextPrevChild(self, flag):
        ret = super(PrescriptionViewer, self).focusNextPrevChild(flag)
        painter = self.image_label.painter
        if painter:
            # XXX refactor. This should not be defined here.
            self.image_label.update()
            if isinstance(painter, FieldPainter):
                painter.ensure_focused_field_visible(self.image_scrollarea)
        painter = self.zoom_label.painter
        if painter:
            # XXX refactor. This should not be defined here.
            self.zoom_label.update()
            if isinstance(painter, FieldPainter):
                painter.ensure_focused_field_visible(self.zoom_scrollarea)
        return ret

    def open(self, filename=None):
        if not filename:
            filename, _ = QtGui.QFileDialog.getOpenFileName(self, "Open Image File",
                                                            QtCore.QDir.currentPath())
        if not filename:
            return

        if isinstance(filename, tuple):
            hack = filename # un-hack later but soon
            filename, buff = filename
            ba = QtCore.QByteArray(buff)
            io = QtCore.QBuffer(ba)
            reader = QtGui.QImageReader(io)
            image = reader.read()
        else:
            hack = False
            image = QtGui.QImage(filename)
        if image.isNull():
            self.msgbox_info('Recipe Viewer', 'Cannot load %s.' % filename)
            return
        try:
            os.chdir(os.path.dirname(filename))
        except:
            pass

        img_dpi = image.physicalDpiY()

        _, ext = os.path.splitext(filename)
        if ext.lower() in ('.tif', '.tiff'):
            class hack:
                correct = (0, 0, 1192, 832) # argh, clean this up!
                therect = (0, 0, image.width(), image.height())

            if hasattr(hack, 'correct'):
                # the image needs to be scaled before use!
                img_dpi = 200
                cr = hack.correct
                w, h = cr[2], cr[3]
                image = image.scaled(w, h, aspectMode=QtCore.Qt.IgnoreAspectRatio,
                                     mode=QtCore.Qt.FastTransformation)
                rx, ry = hack.correct[2:]
                vx, vy = hack.therect[2:]
                # identical: korr = (ry/vy)/(rx/vx)
                korr = ry*vx / (rx*vy)
                self.aspect_factor = korr
        self.init_scale = float(self.screen_dpi) / img_dpi
        self.image_label.setPixmap(QtGui.QPixmap.fromImage(image))
        self.zoom_label.setPixmap(QtGui.QPixmap.fromImage(image))
        self.scaleImage(1.0)

        a6w_200 = 1165.354
        a6h_200 =  826.772


    def zoomIn(self):
        self.resetActions()
        self.scaleImage(self.scale_delta)

    def zoomOut(self):
        self.resetActions()
        self.scaleImage(1/self.scale_delta)

    def normalSize(self):
        self.resetActions()
        self.normalSizeAct.setEnabled(False)
        self.normalSizeAct.setChecked(True)
        self.scale_factor = 1.0
        self.scaleImage(1.0)

    def fitToWindow(self):
        self.resetActions()
        self.fitToWindowAct.setEnabled(False)
        self.fitToWindowAct.setChecked(True)
        self.image_scrollarea.setWidgetResizable(True)

    def split_vertical(self):
        split_state = self.splitVerticalAct.isChecked()
        orients = (QtCore.Qt.Horizontal, QtCore.Qt.Vertical)
        self.h_splitter_image_vs_fields.setOrientation(orients[split_state])

    def createActions(self):
        self.openAct = QtGui.QAction("Open Image File...", self, shortcut="Ctrl+Shift+O",
                triggered=self.open)

        self.exitAct = QtGui.QAction("E&xit", self, shortcut="Ctrl+Q",
                triggered=self.close)

        self.zoomInAct = QtGui.QAction("Zoom &In (25%)", self,
                shortcut="Ctrl++", enabled=True, triggered=self.zoomIn)

        self.zoomOutAct = QtGui.QAction("Zoom &Out (25%)", self,
                shortcut="Ctrl+-", enabled=True, triggered=self.zoomOut)

        self.normalSizeAct = QtGui.QAction("&Normal Size", self,
                shortcut="Ctrl+0", enabled=True, checkable=True, triggered=self.normalSize)

        self.fitToWindowAct = QtGui.QAction("Zoom to &Fit", self,
                shortcut="Ctrl+9", enabled=True, checkable=True, triggered=self.fitToWindow)

        self.splitVerticalAct = QtGui.QAction("&Vertical split", self,
                shortcut="Ctrl+V", enabled=True, checkable=True, triggered=self.split_vertical)

        # actions for validation menu
        self.actions['displayValidationToggle'] = \
            QtGui.QAction("Validierungsergebnisse anzeigen", self,
                enabled=True, checkable=True, triggered=self.set_display_validation)
        self.actions['validationInteractionToggle'] = \
            QtGui.QAction("Validierung steuert Bearbeitung", self,
                enabled=True, checkable=True, triggered=self.set_validation_interaction)


    def createMenus(self):
        self.fileMenu = QtGui.QMenu("&File", self)
        self.fileMenu.addAction(self.openAct)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.exitAct)

        self.viewMenu = QtGui.QMenu("&View", self)
        self.viewMenu.addAction(self.zoomInAct)
        self.viewMenu.addAction(self.zoomOutAct)
        self.viewMenu.addAction(self.normalSizeAct)
        self.viewMenu.addAction(self.fitToWindowAct)
        self.viewMenu.addAction(self.splitVerticalAct)

        self.menuBar.addMenu(self.fileMenu)
        self.menuBar.addMenu(self.viewMenu)

        validation = QtGui.QMenu("Validation", self)
        validation.addAction(self.actions.displayValidationToggle)
        validation.addAction(self.actions.validationInteractionToggle)
        self.menus['validation'] = validation
        self.menuBar.addMenu(self.menus.validation)

    def open_progress(self):
        from .dialog.progressdialog import ProgressWindow
        window = ProgressWindow()
        window.exec_()

    def resetActions(self):
        self.fitToWindowAct.setEnabled(True)
        self.fitToWindowAct.setChecked(False)
        self.normalSizeAct.setEnabled(True)
        self.normalSizeAct.setChecked(False)
        self.image_scrollarea.setWidgetResizable(False)
        self.zoom_scrollarea.setWidgetResizable(False)

    def ensure_text_visible(self):
        '''
        Make the text visible.
        With the help of auto_resize, a resize event does the job.
        '''
        self.text_page.auto_resize = True
        dimensions = (5000, 6000)
        # big screen
        if (self.width() > 1280):
            dimensions = (7000, 6000)
        self.h_splitter_image_vs_fields.setSizes(dimensions)

        # The interface is divided by a vertical splitter ("v_splitter_overview_vs_field")
        # Actually it should be possible to configure the initial sizes in
        # the UI file (which can be modified with the QT Designer).
        # However I was unable to configure an aspect ratio of roughly 4:1 so
        # I'm not spending more hours on this but just do it explicitely in the
        # code instead.
        field_detail_size_percent = 20
        field_detail_size_min = 120
        total_size = sum(self.v_splitter_overview_vs_field.sizes())
        field_detail_size = int((field_detail_size_percent / 100) * total_size)
        if field_detail_size < field_detail_size_min:
            field_detail_size = field_detail_size_min
        overview_size = total_size - field_detail_size
        v_splitter_parts = (overview_size, field_detail_size)
        self.v_splitter_overview_vs_field.setSizes(v_splitter_parts)

    def scaleImage(self, factor):
        self.scale_factor *= factor
        scale = self.init_scale * self.scale_factor
        self.scroll_area_image_contents.resize(scale * self.image_label.pixmap().size())
        # quick hack: set a decent default for zoom
        zoom_scale = self.init_scale * 2
        self.scroll_area_zoom_contents.resize(zoom_scale * self.zoom_label.pixmap().size())

        self.adjustScrollBar(self.image_scrollarea.horizontalScrollBar(), factor)
        self.adjustScrollBar(self.image_scrollarea.verticalScrollBar(), factor)
        self.adjustScrollBar(self.zoom_scrollarea.horizontalScrollBar(), factor)
        self.adjustScrollBar(self.zoom_scrollarea.verticalScrollBar(), factor)

        self.zoomInAct.setEnabled(self.scale_factor < 3.0)
        self.zoomOutAct.setEnabled(self.scale_factor > 0.1)

    def adjustScrollBar(self, scrollBar, factor):
        scrollBar.setValue(int(factor * scrollBar.value()
                                + ((factor - 1) * scrollBar.pageStep()/2)))

    def closeEvent(self, event):
        '''capture the close event and redirect'''
        # the owner of this instance may redefine the closeEvent if it is inserted
        # as self._redirector.
        if self._redirector:
            self._redirector.closeEvent(event)
        else:
            event.accept()

    # Validations actions
    def set_display_validation(self, toggle_value=None):
        if toggle_value is not None:
            self.actions.displayValidationToggle.setChecked(toggle_value)

    @property
    def display_validation(self):
        return self.actions.displayValidationToggle.isChecked()

    def set_validation_interaction(self, toggle_value=None):
        if toggle_value is not None:
            self.actions.validationInteractionToggle.setChecked(toggle_value)

    @property
    def validation_interaction(self):
        return self.actions.validationInteractionToggle.isChecked()

    def set_field_info(self, field_display_name, error_message):
        self.l_field_name.setText(field_display_name)
        self.l_field_errormessage.setText(error_message)

        stat_fld = self.messagelist_widget
        stat_fld.clear()
        stat_fld.addItem(field_display_name)
        stat_fld.addItem(error_message)

    def set_form_info(self, pic, form_index, filename=None):
        form_nr_str = str(form_index + 1)
        pic_text = ('PIC: %s' % pic) if (pic is not None) else ''
        rec_text = 'REC: %s' % form_nr_str
        self.recipe_no_edit.setText(form_nr_str)
        self.status_mid.setText(pic_text)
        self.status_right.setText(rec_text)
        if filename is not None:
            self.status_left.setText(filename)
            self._forminfo_labels.status_left.setText(filename)

        self._forminfo_labels.status_mid.setText(pic_text)
        self._forminfo_labels.status_right.setText(rec_text)


    # -------------------------------------------------------------------------
    # hooks to stub out real "GUI" code in tests
    def msgbox_ok_cancel(self, title, text, default='CANCEL', use_cache=False):
        return msgbox_ok_cancel(
            title=title, text=text, gui=self,
            default=default,
            use_cache=use_cache
        )

    def msgbox_info(self, title, text):
        return QtGui.QMessageBox.information(self, title, text)

    def msgbox_warning(self, title, text):
        return QtGui.QMessageBox.warning(self, title, text)

    def msgbox_critical(self, title, text):
        return QtGui.QMessageBox.critical(self, title, text)

    def progress_window(self):
        return ProgressWindow()

    def qt_app(self):
        return QtGui.QApplication.instance()

