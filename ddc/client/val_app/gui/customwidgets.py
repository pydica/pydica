# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from PySide import QtCore, QtGui


__all__ = ['MyImageLabel', 'MyTextWidget', 'MyScrollArea', 'MirrorLineEdit', 'PydicaLineEdit']

class MyImageLabel(QtGui.QLabel):
    """
    An extension to QLabel to support our own drawing.
    The purpose is to provide a hook, but insert the code
    later using a delegate.
    """
    def __init__(self, parent=None, f=0):
        super(MyImageLabel, self).__init__(parent, f)
        # we use a delegate function from the gui
        self.painter = None

    def sizePolicy(self):
        policy = super(MyImageLabel, self).sizePolicy()
        policy.hasHeightForWidth = lambda: True
        return policy

    def heightForWidth(self, width):
        pixmap = self.pixmap()
        if not pixmap:
            height = width
            return height
        aspect_ratio = pixmap.height() / pixmap.width()
        height = int(width * aspect_ratio)
        # Depending on the exact size of the scroll area and this image label
        # QT might continuously toggle the scrollbar display (hide/show) and
        # resizing the inner widgets (including this one).
        # Somehow this happens when the parent size is a bit smaller than the
        # actual image area. On Windows 10 the min_margin must be bigger than
        # on Fedora 24 (GNOME 3.20).
        # The problem could be toggled either with specific display resolutions
        # (most famously 1920x1080/FHD) or by manually moving the horizontal
        # splitter up and down.
        #
        # Wild guess: QT 4 does auto-expansion of the image contents but does
        # not check if this height would require a scrollbar, forcing it to
        # resize quickly.
        # The width of scrollbars is different for each desktop shell
        # (e.g. 17px in Windows 10, 13px in GNOME 3.20).
        parent_height = self.parent().height()
        min_margin = 12
        # With min_margin=12 I was unable to reproduce the flickering scrollbar
        # at all (not even by adjusting the splitter manually).
        if parent_height <= height <= (parent_height + min_margin):
            return (parent_height - min_margin)
        return height

    def paintEvent(self, e):
        # this line does the default stuff: drawing our recipe
        super(MyImageLabel, self).paintEvent(e)
        # then, we add our own drawing primitives, enclosed into standard stuff.
        if self.painter and self.pixmap():
            qp = QtGui.QPainter()
            qp.begin(self)
            qp.setWindow(self.pixmap().rect())
            qp.setViewport(self.rect())
            self.painter.paint(qp)
            qp.end()

    def resizeEvent(self, e):
        s = e.size()

        w, h = s.width(), s.height()
        prop_h = self.heightForWidth(w)

        if h != prop_h:
            #self.resize(w, prop_h)
            # this happens only when setWidgetResizable is set,
            # so we can expect that the actual height is smaller.
            c = QtCore.Qt
            align = self.alignment()
            if align & c.AlignTop:
                # aling to top
                corr_y = 0
            elif align & c.AlignVCenter:
                # align to center
                corr_y = (h - prop_h) / 2
            else:
                # align to bottom
                corr_y = h - prop_h
            self.setGeometry(0, corr_y, w, prop_h)


class _Mediator(QtCore.QObject):
    '''
    This is a helper class for defining signals.
    Signal classes need to be directly inherited from QObject.
    This trick to overcome this limitation is to use a delegate
    via properties.
    '''

    font_scaled = QtCore.Signal()

    def __init__(self):
        super(_Mediator, self).__init__()


class MyTextWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(MyTextWidget, self).__init__(parent)
        self._mediator = _Mediator()
        self.builder = None
        self.scroll_area = None
        self.auto_resize = False

    def resizeEvent(self, e):
        if not self.builder or not self.auto_resize:
            return
        w = e.size().width()
        oldw = e.oldSize().width()
        ww = self.layout().totalMinimumSize().width()
        if self.scroll_area:
            w = self.scroll_area.viewport().width()
        if oldw < w:
            neww = max(w, ww)
        else:
            neww = min(w, ww)
        self.builder.set_best_fitting_scale(neww)
        self.font_scaled.emit()
        return True

    @property
    def font_scaled(self):
        return self._mediator.font_scaled


class MyScrollArea(QtGui.QScrollArea):
    def resizeEvent(self, e):
        return super(MyScrollArea, self).resizeEvent(e)


class PydicaLineEdit(QtGui.QLineEdit):
    # gettingTouched is an important signal which is emitted for any event
    # interaction with this line edit field (unless suppressed by the event
    # filter). The mirrored widget is not updated without this signal.
    getting_touched = QtCore.Signal(QtCore.QEvent, name='gettingTouched')

    def __init__(self, *args, eventfilter=None, **kwargs):
        super(PydicaLineEdit, self).__init__(*args, **kwargs)
        self._eventfilter = eventfilter
        # actually I expected that installing an event filter should be possible
        # outside of __init__() (also with an abitrary callable) but somehow
        # that did not work for me (Fedora 23, PySide 1.2.2, QT 4.8.7).
        if eventfilter is not None:
            self.installEventFilter(self)

    def eventFilter(self, widget, event):
        # actually the "QT way" (as I perceive it) for notifying other
        # components (like the main controller) would be to emit a custom
        # signal.
        # However I think this is not the best option here because signals are
        # asynchronous and there is no notion of "handling" a signal (which
        # includes to stop further processing).
        # This means we can not just intercept certain events (e.g. "ENTER
        # pressed") - or at least it is not really "clean" because the LineEdit
        # never knows if there are any listeners registered for a given signal.
        # (so blocking the event in all cases makes an assumption I am not
        # really comfortable with for a general LineEdit).
        if not isinstance(event, QtCore.QEvent):
            # There seems to be a PySide bug as "event" sometimes contains a
            # QtGui.QListWidgetItem which is not a QEvent.
            # If we pass this up to "QWidget.eventFilter()" PySide will complain
            # about an unsupported parameter type so we just ignore this type
            # of "event" here.
            return False

        result = self._eventfilter(widget, event)
        if result == True:
            return True
        self.getting_touched.emit(event)
        return QtGui.QWidget.eventFilter(self, widget, event)


class MirrorLineEdit(QtGui.QLineEdit):
    '''
    How the mirror line edit works:

    The control's main function is
        mirror(widget)
    All settings of the 'widget' are read and assigned to 'self'.
    This makes the widget 'self' look exactly like widget 'widget'.

    Special tricks:
        self.setFocusProxy(widget)
    lets 'self' look like it has the focus, but being just a delegate.
        self.focusInEvent(QtGui.QFocusEvent(QtCore.QEvent.FocusIn))
    is a trick to leave a second cursor visible. Normally, focusIn and
    focusOut are balanced and allow just a single cursor.

    The redirection of mouse event was trickier to find out.
    Mouse events are created on a widget that really has the mouse spot.
    After lots of trying, the solution was very simple:
    We call the mouse event again on the target widget and clear our own event.
    '''
    def __init__(self, parent=None):
        self.mirrored_widget = None
        super(MirrorLineEdit, self).__init__(parent)

    def mirror(self, widget):
        if not isinstance(widget, QtGui.QLineEdit):
            return
        self.mirrored_widget = widget
        self.setReadOnly(False)
        # The mirrored edit widget should use its own size/font so we don't
        # copy these attributes from the original widget.
        self.setStyleSheet(widget.styleSheet())
        self.setToolTip(widget.toolTip())
        # this is _the_ trick to produce a second cursor!
        self.focusInEvent(QtGui.QFocusEvent(QtCore.QEvent.FocusIn))
        self.setText(widget.text())
        self.setCursorPosition(widget.cursorPosition())
        start = widget.selectionStart()
        if start >= 0:
            self.setSelection(start, len(widget.selectedText()))
        # this redirects the focus to the real control but keeps the highlight.
        self.setFocusProxy(widget)

    def mousePressEvent(self, event):
        self._handle_mirrored_mouse(event)
        return QtGui.QLineEdit.mousePressEvent(self, event)

    def mouseReleaseEvent(self, event):
        self._handle_mirrored_mouse(event)
        return QtGui.QLineEdit.mouseReleaseEvent(self, event)

    def mouseMoveEvent(self, event):
        self._handle_mirrored_mouse(event)
        return QtGui.QLineEdit.mouseMoveEvent(self, event)

    def mouseDoubleClickEvent(self, event):
        self._handle_mirrored_mouse(event)
        return QtGui.QLineEdit.mouseDoubleClickEvent(self, event)

    def _handle_mirrored_mouse(self, event):
        widget = self.mirrored_widget
        if widget:
            widget.event(event)
        return True
