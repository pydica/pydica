#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

"""
based on the PySide dialogs/findfiles example from Qt v4.x

For some notes about dialog problems, see dialog-notes.txt
"""

import os
import functools
import json  ## note: ujson seems to be a very fast alternative

from PySide import QtCore, QtGui

from ddc.client.val_app.gui.dialog.ui_batchdialog import Ui_BatchDialog
from ddc.lib.log_proxy import l_
from ddc.tool.datamanager.bunch_assembler import BunchAssembler
from ddc.tool.datamanager.discover_lib import find_data_files
from ddc.storage.paths import DataBunch
from ddc.storage.batch import Batch


AUTO_RUN = True

# how do we access the batches for getting info
# previously we used 'read'. Now we use 'write', because
# that hopefully gives locking information about the old
# software as well.

_ACCESS_INFO = 'write'

def make_num_str(txt):
    try:
        s = str(txt)
        i = int(s)
        txt = str(i)
    except:
        pass
    return txt

def count_errors_in_batch(batch):
    '''Look into a batch and check for errors'''
    # this is almost copied from viewer_class.py

    # filter by form_position
    tasks = list(t for t in batch.tasks())
    # filter by existence of 'field_name'
    attr = 'field_name'
    tasks = list(t for t in tasks if attr in t.data)

    return len(tasks)

def dumps_bunch(bunch):
    '''pack a complete bunch into a json pickle'''
    assert isinstance(bunch, DataBunch)
    return json.dumps(bunch)

def loads_bunch(jtext):
    data = json.loads(jtext)
    return DataBunch(*data)

def find_complete_bunches(root_path, recursive):
    data_files = find_data_files(root_path, recursive)
    bunch_results = BunchAssembler(data_files).process_filenames()
    complete_bunches = bunch_results.complete
    return complete_bunches

####################################################
###
##    from here, QT is involved

def make_str_cell(txt):
    '''create a read-only cell for a string with some default layout'''
    cell = QtGui.QTableWidgetItem(txt)
    cell.setTextAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
    cell.setFlags(cell.flags() & ~QtCore.Qt.ItemIsEditable)
    return cell

def make_num_cell(num):
    '''create a read-only cell for a number with some default layout'''
    if isinstance(num, Exception):
        icon = QtGui.QIcon.fromTheme("dialog-warning");
        cell = QtGui.QTableWidgetItem(icon,
                    'Locked')
        brush = QtGui.QBrush(QtGui.QColor(255, 0, 0))
        cell.setForeground(brush)
        cell.setToolTip(repr(num))
    else:
        txt = make_num_str(num)
        cell = QtGui.QTableWidgetItem(txt)
    cell.setTextAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)
    cell.setFlags(cell.flags() & ~QtCore.Qt.ItemIsEditable)
    return cell


class DlgWindow(QtGui.QDialog, Ui_BatchDialog):

    def update_controls(func):
        '''replace a method with a construct that ensures to call _update_controls
        '''
        def wrapper(self, *args, **kwargs):
            try:
                result = func(self, *args, **kwargs)
            finally:
                self._update_controls()
            return result
        return functools.wraps(func)(wrapper)

    def capture_exceptions(func):
        '''report any exception in the gui and pass it on, unchanged
        '''
        def wrapper(self, *args, **kwargs):
            try:
                result = func(self, *args, **kwargs)
            except Exception as e:
                mbox = QtGui.QMessageBox()
                mbox.setText("<p>An unexpected error occured:</p>"
                 "<p>{!r}</p>".format(e))
                mbox.exec_()
                raise
            return result
        return functools.wraps(func)(wrapper)

    @update_controls
    def __init__(self, parent=None, log=None):
        super(DlgWindow, self).__init__(parent)
        self.log = l_(log)

        # the variable that holds the selected bunch
        self.selected_bunch = None
        # the location where we start to search
        self.root_dir = None

        # let the automatic sourcery happen
        self.setupUi(self)

        # clear the demo data
        self.table_widget.setRowCount(0)

        # insert an invisible column for identification
        self.table_widget.insertColumn(0)
        self.table_widget.setColumnHidden(0, True)
        self.first_col = first_col = 1
        self.sort_col = first_col+4 # sort on path

        self.cancel_button.setDefault(False)
        self.cancel_button.clicked.connect(self.reject)

        self.ok_button.setDefault(True)
        self.ok_button.clicked.connect(self.accept)

        # overriding the button focus. See dialog-notes.txt
        self.ok_button.setFocus() # brute-force

        self.browse_button.clicked.connect(self.browse)
        self.find_button.clicked.connect(self.find)

        # the result of the selection
        self.table_widget.itemClicked.connect(self.bunch_selected)
        self.table_widget.itemDoubleClicked.connect(self.bunch_selected2)
        self.table_widget.itemSelectionChanged.connect(self.bunch_unselected)

        # the filter button
        self.filter_checkbox.setChecked(QtCore.Qt.CheckState.Checked)
        self.filter_checkbox.stateChanged.connect(self.filter_changed)

        self._reset_caches()

        # explicitly disable sorting, do it with sorted(), instead
        self.table_widget.setSortingEnabled(False)

        # a timer for lazy updating the computed columns
        self.timer = QtCore.QTimer(self)
        self.timer.setInterval(0)
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self._on_timeout)
        self.pending_row_list = []

    def _reset_caches(self):
        self.bunch_to_errcount = {}
        self.bunch_to_formcount = {}

    def set_root_dir(self, directory):
        if self.root_combo.findText(directory) == -1:
            self.root_combo.addItem(directory)
        self.root_dir = directory

    @update_controls
    def browse(self):
        if not self.root_dir:
            self.root_dir = os.getcwd()

        directory = QtGui.QFileDialog.getExistingDirectory(self, "Startverzeichnis",
                QtCore.QDir(self.root_dir).absolutePath())

        if directory:
            if self.root_combo.findText(directory) == -1:
                self.root_combo.addItem(directory)
                self.root_dir = directory

            self.root_combo.setCurrentIndex(self.root_combo.findText(directory))
            if AUTO_RUN:
                self.find()

    def exec_(self, *args):
        # clear out some left data
        self._reset_caches()
        self.table_widget.setRowCount(0)
        # make something visible, soon
        self.show()
        # maybe use a process control here?
        if AUTO_RUN:
            self.find()
        return super(DlgWindow, self).exec_()

    @update_controls
    def find(self):
        self.find_button.setEnabled(False)
        self.table_widget.setRowCount(0)

        root_path = self.root_combo.currentText()

        rec_flag = self.recursive_scan.isChecked()
        files = sorted(find_complete_bunches(root_path, recursive=rec_flag))

        self.show_files(files)
        self.initiate_incremental_update(0)

    def show_files(self, files):
        fc = self.first_col
        root_dir = self.root_combo.currentText()
        filtering = self.filter_checkbox.isChecked()

        for bunch in files:
            # this block is the dumped bunch
            bunch_item = make_str_cell(dumps_bunch(bunch))

            # the customer name
            cdb_name = bunch.cdb
            cdb_short = os.path.splitext(os.path.basename(cdb_name))[0]
            cus_split = 5
            customer_id = cdb_short[:cus_split]
            customer_batch = cdb_short[cus_split:]

            row = self.table_widget.rowCount()
            self.table_widget.insertRow(row)
            # add the hidden columns
            self.table_widget.setItem(row, 0, bunch_item)

            # these are the shown columns
            customer_id_item = make_num_cell(customer_id)
            customer_batch_item = make_num_cell(customer_batch)
            formcount_item = make_num_cell(self.bunch_to_formcount.get(bunch, '?'))
            errcount_item = make_num_cell(self.bunch_to_errcount.get(bunch, '?'))
            details_item = make_str_cell(os.path.relpath(bunch.cdb, root_dir))

            self.table_widget.setItem(row, fc+0, customer_id_item)
            self.table_widget.setItem(row, fc+1, customer_batch_item)
            self.table_widget.setItem(row, fc+2, formcount_item)
            self.table_widget.setItem(row, fc+3, errcount_item)
            self.table_widget.setItem(row, fc+4, details_item)

            if filtering:
                suppress = bunch.is_processed()
            else:
                suppress = False

            if suppress:
                self.table_widget.hideRow(row)
            else:
                self.table_widget.showRow(row)

    def _calculate_extra_data(self, bunch):
        '''calculate the missing data for a bunch if not in the helper dicts.'''

        # this could actually be extracted into a non-gui class
        if bunch in self.bunch_to_errcount:
            return False # this one is done
        try:
            batch = Batch.init_from_bunch(bunch, delay_load=False,
                                          access=_ACCESS_INFO)
        except OSError as e:
            # pass the exception instance as visible result
            self.bunch_to_errcount[bunch] = e
            return True
        try:
            errors_in_batch = count_errors_in_batch(batch)
            self.bunch_to_errcount[bunch] = errors_in_batch
            self.bunch_to_formcount[bunch] = batch.cdb.count()
        finally:
            batch.close()
            return True

    def _update_row(self, row_id):
        '''take a row and add current data to it'''
        # find the bunch of the row
        bunch = self.rowid_to_bunch(row_id)
        # fetch the updated data if available
        formcount_item = make_num_cell(self.bunch_to_formcount.get(bunch, '?'))
        errcount_item = make_num_cell(self.bunch_to_errcount.get(bunch, '?'))
        fc = self.first_col
        self.table_widget.setItem(row_id, fc+2, formcount_item)
        self.table_widget.setItem(row_id, fc+3, errcount_item)

    def rowid_to_bunch(self, row_id):
        jtext = self.table_widget.item(row_id, 0).data(0)
        return loads_bunch(jtext)

    @update_controls
    def bunch_selected(self, sel_item):
        if isinstance(sel_item, QtGui.QListWidgetItem):
            # There seems to be a PySide (using PySide 1.2.2, QT 4.8.7, Python 3.5.1
            # on Fedora 24). Sometimes the "sel_item" is a QListWidgetItem instead
            # of the expected QTableWidgetItem so the ".row() method below raises
            # an exception.
            # I think the best course of action is to ignore these "selections".
            self.log.debug('PySide bug in bunch_selected(): got QListWidgetItem instead of QTableWidgetItem')
            return

        row = self.table_widget.row(sel_item)
        data = loads_bunch(self.table_widget.item(row, 0).data(0))
        self.selected_bunch = data

    def bunch_selected2(self, sel_item):
        # the behavior is like a single click, followed by OK
        # selected will be called once, before selected2 is issued.
        self.timer.stop()
        self._reset_caches()
        self.accept()

    @update_controls
    def bunch_unselected(self):
        self.selected_bunch = None

    def filter_changed(self, state):
        filtering = state == QtCore.Qt.CheckState.Checked

        for row in range(self.table_widget.rowCount()):
            bunch = loads_bunch(self.table_widget.item(row, 0).data(0))
            if filtering:
                suppress = bunch.is_processed()
            else:
                suppress = False

            if suppress:
                self.table_widget.hideRow(row)
            else:
                self.table_widget.showRow(row)

    def _update_controls(self):
        # make things visible/selectable
        may_hit_ok = bool(self.selected_bunch)
        may_hit_find = True

        self.ok_button.setEnabled(may_hit_ok)
        self.find_button.setEnabled(may_hit_find)

    def initiate_incremental_update(self, start_row):
        '''
        The idea:
        We initiate a list of all items to be computed.
        Whenever the status of the table is modified, this
        list is re-created. This is efficient, because entries into
        the table are only computed once.
        '''
        row_count = self.table_widget.rowCount()
        self.pending_row_list = (list(range(start_row, row_count)) +
                                 list(range(0, start_row)))
        self.pending_row_list.reverse()
        self.current_anchor_row = start_row
        self.timer.start(0)

    @capture_exceptions
    def _on_timeout(self):
        '''operate on the things to do, trigger another call when not done.'''
        if self.pending_row_list:
            row_id = self.pending_row_list.pop()
            # calculate only visible rows
            if not self.table_widget.isRowHidden(row_id):
                # FIXME: sometimes Pydica crashes in "self.rowid_to_bunch()". This
                # is not 100% reproducible but I can trigger it when clicking on
                # the same row multiple times (involves opening the dialog several
                # times).
                bunch = self.rowid_to_bunch(row_id)
                # calculate only new rows
                if self._calculate_extra_data(bunch):
                    # update only new data
                    self._update_row(row_id)
            # now check if the viewport has changed and if so, start over.
            # this is quick, because we use the computed results.
            topleft_visible = self.table_widget.itemAt(0, 0)
            # for some reason, this seems not always to exist.
            if topleft_visible:
                # this line sometimes complains that 'row' does not exist.
                # That must be a wrong object, maybe serious PySide bug.
                top_visible_row = topleft_visible.row()
                if top_visible_row != self.current_anchor_row:
                    # create an updated todo-list
                    self.initiate_incremental_update(top_visible_row)
            self.timer.start(0)


if __name__ == '__main__':

    import sys

    app = QtGui.QApplication(sys.argv)
    window = DlgWindow()
    window.show()
    res = window.exec_()
    print(res)
    sys.exit(res)
