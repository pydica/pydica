#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from PySide import QtGui

# absolute import so we can run this module as a script to test the gui manually
from ddc.client.val_app.gui.dialog.ui_progressdialog import Ui_Dialog

DONE = 'done'

class ProgressWindow(QtGui.QDialog, Ui_Dialog):
    def __init__(self, parent=None, log=None):
        super(ProgressWindow, self).__init__(parent)
        self._ui_initialized = False
        self._max_value = None
        self.is_cancelled = False
        self._is_worker_finished = False

    def progress_reporter(self, qt_app):
        return QTProgress(self, qt_app)

    def setupUI(self):
        super().setupUi(self)
        self.cancel.clicked.connect(self.reject)
        self.confirm.setEnabled(False)
        self.confirm.clicked.connect(self.accept)
        self._ui_initialized = True

    def show(self, worker):
        if not self._ui_initialized:
            self.setupUI()
            self.set_max_value(self._max_value or 0)
        super().show()
        worker()
        self._is_worker_finished = True
        if self.is_cancelled:
            self.reject()

    # need to override reject so also closing the dialog with ESC/clicking the
    # close button does the right thing.
    def reject(self):
        self.is_cancelled = True
        if self._is_worker_finished:
            # Need to wait until the worker is done (retrieved the "cancel"
            # value). The UI code in ".show()" will check for self.is_cancelled
            # and call ".reject()" again.
            super(ProgressWindow, self).reject()

    def set_max_value(self, value):
        if not self._ui_initialized:
            self._max_value = value
            return

        self.progressBar.setRange(0, value)
        self.progressBar.setValue(0)
        self.progressBar.reset()

    def update_progress(self, current, *, details=None):
        if details is not None:
            self.task_details.setText(details)

        if current == DONE:
            # LATER: callback handler to dynamically update the "final action"
            # (e.g. button text)
            self.set_max_value(100)
            self.progressBar.setValue(self.progressBar.maximum())
            self.confirm.setEnabled(True)
            self.confirm.setFocus()
        elif current is not None:
            self.progressBar.setValue(current)
        return self.is_cancelled

# This class is helpful because it implements the general "progress" API also
# found in "progress_utils.py" so we can have also command-line progress
# reports if necessary.
class QTProgress(object):
    def __init__(self, ui, qt_app, *, max_value=0):
        self.ui = ui
        self.qt_app = qt_app
        self.result = None
        self.is_cancelled = False
        self.set_max_value(max_value)

    def update(self, i, details=None):
        cancel = self.ui.update_progress(i, details=details)
        if cancel == True:
            self.is_cancelled = True
        # trigger the QT event loop so the UI keeps responding
        # pydica is (at the moment) a single-process application. Distributing
        # the load (especially batch validation) on multiple processes would be
        # a nice improvement but unfortunately this won't be trivial as we can
        # not share SQLite instances/CDB file handles across thread/process
        # boundaries.
        # So for now we are doing a kind of "cooperative threading" where the
        # "worker" interrupts its work often enough so that the GUI appears to
        # be responsive.
        self.qt_app.processEvents()
        return (not self.is_cancelled)

    def finish(self, result=None):
        self.ui.update_progress(DONE)
        self.result = result

    def abort(self):
        assert (not self.is_cancelled)
        self.ui.reject()
        self.is_cancelled = True

    # --- custom API ----------------------------------------------------------
    def set_max_value(self, max_value):
        assert (not self.is_cancelled)
        self.ui.set_max_value(max_value)


# =============================================================================
# dummy code to run the progress dialog independently from the main pydica app
def worker_job(progress_factory):
    startup_seconds = 0.5
    progress_seconds = 1

    import time
    progress = progress_factory(max_value=0)
    progress.update(0, details='Initialisiere Datenbank (%ds)' % startup_seconds)

    start = time.time()
    progress_start = start + startup_seconds
    progress_done = progress_start + progress_seconds

    current_time = start
    progress_mode = False
    while current_time < progress_done:
        current_time = time.time()
        current = int(10 * (current_time - progress_start))

        is_active = True
        if not progress_mode:
            if current >= 0:
                progress.set_max_value(10 * progress_seconds)
                progress_mode = True
                is_active = progress.update(current, details='Suche Fehler in Belegdaten')
            else:
                is_active = progress.update(0)
        elif progress_mode:
            if SIMULATE_ABORT:
                progress.abort()
                return
            is_active = progress.update(current)
        if not is_active:
            # operation cancelled by user
            return
        time.sleep(0.1)

    progress.finish(result=progress_done)


SIMULATE_ABORT = False

if __name__ == "__main__":
    import sys
    qt_app = QtGui.QApplication(sys.argv)
    window = ProgressWindow()
    progress = window.progress_reporter(qt_app)
    def progress_callable(max_value):
        progress.set_max_value(max_value)
        return progress

    def worker_():
        return worker_job(progress_callable)
    window.show(worker_)
    if not progress.is_cancelled:
        qt_app.exec_()
    user_cancelled = window.is_cancelled
    print('user cancelled? %r' % user_cancelled)
    print('final worker result is: %r' % (progress.result))
