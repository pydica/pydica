# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'progressdialog.ui'
#
# Created: Fri Nov 11 14:51:52 2016
#      by: pyside-uic 0.2.13 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 155)
        self.task_details = QtGui.QLabel(Dialog)
        self.task_details.setGeometry(QtCore.QRect(30, 20, 331, 31))
        self.task_details.setObjectName("task_details")
        self.progressBar = QtGui.QProgressBar(Dialog)
        self.progressBar.setGeometry(QtCore.QRect(30, 60, 341, 26))
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName("progressBar")
        self.confirm = QtGui.QPushButton(Dialog)
        self.confirm.setEnabled(False)
        self.confirm.setGeometry(QtCore.QRect(200, 100, 171, 32))
        self.confirm.setDefault(False)
        self.confirm.setFlat(False)
        self.confirm.setObjectName("confirm")
        self.cancel = QtGui.QPushButton(Dialog)
        self.cancel.setGeometry(QtCore.QRect(30, 100, 111, 32))
        self.cancel.setDefault(True)
        self.cancel.setObjectName("cancel")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "validiere Stapel", None, QtGui.QApplication.UnicodeUTF8))
        self.task_details.setText(QtGui.QApplication.translate("Dialog", "Suche Fehler in Belegdaten", None, QtGui.QApplication.UnicodeUTF8))
        self.confirm.setText(QtGui.QApplication.translate("Dialog", "Fehler korrigieren", None, QtGui.QApplication.UnicodeUTF8))
        self.cancel.setText(QtGui.QApplication.translate("Dialog", "Abbrechen", None, QtGui.QApplication.UnicodeUTF8))

