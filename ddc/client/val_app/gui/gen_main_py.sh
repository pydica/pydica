#!/bin/bash

# this nice bash functionality enables us to call this script
# from any directory (previously it assumed the current working
# directory was the directory where this script is located).
THISDIR="$( dirname "${BASH_SOURCE[0]}" )"
cd "${THISDIR}"

pyside-uic --from-imports mainwindow.ui > ui_mainwindow.py
python fix_import.py ui_mainwindow.py
