# -*- coding: utf-8 -*-

from PySide import QtGui


__all__ = ['msgbox_ok_cancel']


cache = {}
def _q_message_box(gui, use_cache):
    global cache
    # in Windows 10 we saw frequent crashes with ediview when triggering
    # the "delete form dialog" repeatedly. e.g. by pressing F12/ESC a few
    # times per second.
    # Setup was Windows 10 (June 2016), Python 3.3/3.4 in 32 bit and 64 bit,
    # PySide 1.2.2 and 1.2.4. This problem did not occur on Fedora 23
    # (64 bit, Python 3.4, PySide 1.2.2).
    # The trigger is purely the frequent instantiation of a QMessageBox.
    #
    # Very likely there is some kind of race condition in PySide's Windows
    # code. As debugging PySide/QT (especially on Windows) is quite a pain
    # I decided to go with a simple workaround which is caching the message
    # box instance here.
    # As these message boxes are application-modal there are not issues
    # with concurrent access.
    #
    # I suspect the crashes might happen also in other places where a
    # QMessageBox is instantiated. However we don't use the caching
    # workaround there (so far) because I don't like to clutter unrelated
    # code with these workarounds without positively knowing that they are
    # actually necessary in these places as well (prevent cargo culting).
    if use_cache and (cache.get('qmbox') is not None):
        return cache['qmbox']
    mbox = QtGui.QMessageBox(gui)
    mbox.setIconPixmap(gui.windowIcon().pixmap(64, 64))
    if use_cache:
        cache['qmbox'] = mbox
    return mbox

def msgbox_ok_cancel(title, text, gui, default='CANCEL', use_cache=False):
    mbox = _q_message_box(gui, use_cache=use_cache)
    mbox.setStandardButtons(mbox.Ok | mbox.Cancel);
    default_button = mbox.Cancel if (default == 'CANCEL') else mbox.Ok
    mbox.setDefaultButton(default_button)
    mbox.setText(title)
    mbox.setInformativeText(text)
    return mbox
