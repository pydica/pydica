# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os
import shutil
from tempfile import mkdtemp

from pythonic_testcase import *
from srw.rdblib import guess_path
from srw.rdblib.cdb import create_cdb_with_dummy_data
from srw.rdblib.ibf import create_ibf

from ddc.client.config import ALL_FIELD_NAMES
from ddc.client.val_app.testutil import fake_gui
from ddc.client.val_app.gui.cdb_opener import open_batch_with_ui_progress
from ddc.lib.progress_utils import ProgressRecorder
from ddc.storage import Batch, TaskType, TaskStatus
from ddc.tool.datamanager import bunch_from_cdb_name
from ddc.validation.testutil import build_context


def cancelling_progressor(update_callable):
    class Progressor(ProgressRecorder):
        def update(self, i, details=None):
            super().update(i, details=details)
            result = update_callable(self, i, details)
            if result == False:
                self.is_cancelled = True
            return result
    return Progressor()

class CDBOpenerTest(PythonicTestCase):
    def setUp(self):
        super(CDBOpenerTest, self).setUp()
        self.temp_dir = mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.temp_dir)
        super(CDBOpenerTest, self).tearDown()

    def _create_rdb_and_ibf(self, *, cdb_path, nr_forms):
        cdb_fp = create_cdb_with_dummy_data(nr_forms=nr_forms, filename=cdb_path, field_names=ALL_FIELD_NAMES)
        cdb_fp.close()
        ibf_path = guess_path(cdb_path, type_='IBF')
        ibf_fp = create_ibf(nr_images=nr_forms, filename=ibf_path, create_directory=True)
        ibf_fp.close()

    def test_can_validate_simple_rdb_with_progress_update(self):
        cdb_path = os.path.join(self.temp_dir, 'foo.rdb')
        self._create_rdb_and_ibf(cdb_path=cdb_path, nr_forms=1)
        # add invalid field so we get a verification task (only GEBURTSDATUM is
        # being validated in the default configuration).
        batch = Batch.init_from_bunch(bunch_from_cdb_name(cdb_path))
        form = batch.form(0)
        form.fields['GEBURTSDATUM'].value = 'invalid'
        batch.close(commit=True)

        gui = fake_gui()
        batch = open_batch_with_ui_progress(cdb_path, gui, build_context())
        assert_not_none(batch)
        assert_length(1, batch.forms())
        assert_length(2, batch.tasks())
        assert_length(1, batch.tasks(type_=TaskType.FORM_VALIDATION, status=TaskStatus.CLOSED))
        assert_length(1, batch.tasks(type_=TaskType.VERIFICATION, status=TaskStatus.NEW))

    def test_can_cancel_before_assembling_bunch(self):
        invalid_path = os.path.join(self.temp_dir, 'does-not-exist.rdb')
        # otherwise we should see a message that the CDB is invalid
        progressor = cancelling_progressor(lambda *args: False)

        gui = fake_gui(progress_reporter=progressor)
        batch = open_batch_with_ui_progress(invalid_path, gui, build_context())
        assert_none(batch)
        assert_equals([0], progressor.updates)
        # LATER: assert no warnings

    def test_can_cancel_before_opening_the_cdb_file(self):
        cdb_path = os.path.join(self.temp_dir, 'foo.rdb')
        with open(cdb_path, 'wb') as cdb_fp:
            cdb_fp.write(b'\x00' * 100)
        cdb_fp = create_cdb_with_dummy_data(nr_forms=1, filename=cdb_path, field_names=('FOO', 'BAR'))
        cdb_fp.close()

        # otherwise we should see a message that the CDB is invalid
        def cancel_structure_validation(self, i, details):
            if len(self.updates) == 2:
                assert details == 'Strukturvalidierung'
                return False
        progressor = cancelling_progressor(cancel_structure_validation)
        gui = fake_gui(progress_reporter=progressor)
        batch = open_batch_with_ui_progress(cdb_path, gui, build_context())
        assert_none(batch)
        assert_equals([0, 0], progressor.updates)
        # LATER: assert no warnings

    def test_can_close_batch_if_validation_was_interrupted(self):
        cdb_path = os.path.join(self.temp_dir, 'interrupted.rdb')
        self._create_rdb_and_ibf(cdb_path=cdb_path, nr_forms=3)

        cancel = lambda self, i, details=None: False if (i == 2) else None
        progressor = cancelling_progressor(cancel)
        gui = fake_gui(progress_reporter=progressor)
        batch = open_batch_with_ui_progress(cdb_path, gui, build_context())

        assert_none(batch)
        assert_equals([0, 1, 2], progressor.updates[-3:])
        assert_false(progressor.is_finished)

        bunch = bunch_from_cdb_name(cdb_path)
        with assert_not_raises():
            batch = Batch.init_from_bunch(bunch, create_persistent_db=False)
        assert_length(0, batch.tasks())


