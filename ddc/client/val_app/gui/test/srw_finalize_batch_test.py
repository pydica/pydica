# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os
import shutil
from tempfile import mkdtemp

from pythonic_testcase import *
from srw.rdblib import guess_path, DataBunch
from srw.rdblib.cdb import create_cdb_with_dummy_data
from srw.rdblib.ibf import create_ibf

from ddc.client.config import ALL_FIELD_NAMES
from ddc.client.val_app.gui.srw_ascii import finalize_batch
from ddc.lib.attribute_dict import AttrDict
from ddc.storage import Batch, TaskType
from ddc.tool.cdb_collection import CDB_Collection


def fake_gui():
    return AttrDict()

def create_file(filepath, data=None):
    if data is None:
        data = b''
    with open(filepath, 'wb') as fp:
        fp.write(data)


class SRWAsciiExportTest(PythonicTestCase):
    def setUp(self):
        super(SRWAsciiExportTest, self).setUp()
        self.temp_dir = mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.temp_dir)
        super(SRWAsciiExportTest, self).tearDown()

    # --- tests ---------------------------------------------------------------

    def test_can_export_ascii_file_from_cdb(self):
        batch = self._create_batch_in_tempdir()
        asc_path = guess_path(batch.bunch.cdb, type_='asc')
        cdb_id = id(batch.cdb)
        assert_false(os.path.exists(asc_path),
            message='ASC file must not exist so the it can be created.')

        seq = CDB_Collection(batch)
        result = finalize_batch(batch, cdb_collection=seq)
        assert_equals(True, result)
        assert_equals(asc_path, batch.bunch.ask)
        assert_true(os.path.exists(asc_path))
        assert_equals(cdb_id, id(batch.cdb),
            message='should keep CDB instance the same as no rename was necessary')
        # close all open files - otherwise Windows won't be able to remove
        # the temp dir
        batch.close()

    def _create_batch_in_tempdir(self, rdb=False):
        nr_forms = 2
        extension = 'RDB' if rdb else 'CDB'
        xdb_path = os.path.join(self.temp_dir, '00042100.'+extension)
        ibf_path = guess_path(xdb_path, type_='ibf')
        create_cdb_with_dummy_data(nr_forms=nr_forms, filename=xdb_path, field_names=ALL_FIELD_NAMES)
        create_ibf(nr_images=nr_forms, filename=ibf_path, create_directory=True)
        bunch = DataBunch(cdb=xdb_path, ibf=ibf_path, db=None, ask=None)
        batch = Batch.init_from_bunch(bunch, create_persistent_db=True)
        return batch

    def test_can_rename_rdb_to_cdb_and_export_ascii(self):
        batch = self._create_batch_in_tempdir(rdb=True)
        rdb_path = batch.bunch.cdb
        asc_path = guess_path(batch.bunch.cdb, type_='asc')
        assert_false(os.path.exists(asc_path),
            message='ASC file must not exist so the it can be created.')

        seq = CDB_Collection(batch)
        result = finalize_batch(batch, cdb_collection=seq)
        assert_equals(True, result)
        assert_equals(asc_path, batch.bunch.ask)
        assert_true(os.path.exists(asc_path))
        cdb_filename = os.path.basename(batch.bunch.cdb)
        assert_true(cdb_filename.endswith('.CDB'))
        assert_false(os.path.exists(rdb_path),
            message='RDB should not exist (expected rename to CDB)')
        assert_true(os.path.exists(batch.bunch.cdb))
        # close all open files - otherwise Windows won't be able to remove
        # the temp dir
        batch.close()

    def test_rejects_export_if_asc_file_already_exists(self):
        batch = self._create_batch_in_tempdir()
        asc_path = guess_path(batch.bunch.cdb, type_='asc')
        create_file(asc_path, data=b'secret')
        assert_none(batch.bunch.ask,
            message='should check for file existence even if ASC is not known in bunch')
        assert_true(os.path.exists(asc_path),
            message='ASC file should exist to trigger the error.')

        seq = CDB_Collection(batch)
        result = finalize_batch(batch, cdb_collection=seq)
        assert_equals(False, result, message='no export if ASC file exists')
        assert_equals('asc_exists', result.key)
        assert_equals(b'secret', open(asc_path, 'rb').read())
        # close all open files - otherwise Windows won't be able to remove
        # the temp dir
        batch.close()

    def test_rejects_export_if_new_verification_tasks_exist(self):
        batch = self._create_batch_in_tempdir()
        asc_path = guess_path(batch.bunch.cdb, type_='asc')
        form1 = batch.db_form(0)
        form1.add_task(type_=TaskType.VERIFICATION)
        batch.commit()
        assert_length(1, batch.new_tasks())

        seq = CDB_Collection(batch)
        result = finalize_batch(batch, cdb_collection=seq)
        assert_equals(False, result, message='no export if CDB contains errors exists')
        assert_equals('data_contains_errors', result.key)
        assert_false(os.path.exists(asc_path))
        # close all open files - otherwise Windows won't be able to remove
        # the temp dir
        batch.close()

