# fix the wrong relative import in ui_mainwindow.py

import sys
fname = sys.argv[1]
txt = open(fname, 'r').read()
txt = txt.replace(' customwidgets', ' .customwidgets')
open(fname, 'w').write(txt)
