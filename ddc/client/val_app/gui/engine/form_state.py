# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from ddc.lib.log_proxy import l_

__all__ = ['Field', 'FormState', 'ValidationResult']

class ValidationResult(object):
    OK      = 'OK'
    WARNING = 'WARNING'
    ERROR   = 'ERROR'


class Field(object):
    def __init__(self, field_name, form_state):
        self.name = field_name
        self._form_state = form_state

    @property
    def validation_state(self):
        first_error = self.first_error()
        if first_error is None:
            return ValidationResult.OK
        elif first_error['is_warning']:
            return ValidationResult.WARNING
        return ValidationResult.ERROR

    def first_error(self):
        result = self._form_state.validation_result
        if result is None:
            return None
        field_errors = result.errors.get(self.name)
        if not field_errors:
            return None
        return field_errors[0]

    def is_valid(self):
        return (self.first_error() is None)


class FormState(object):
    """Transient state regarding a single form
    e.g. validation state
    """
    def __init__(self, batch_state, fields, *, form_index, log=None, widget_config=None):
        """
        fields is the list of fields present in the current form. The order of
        the iterable is also used to compute the next field the user should
        jump to (e.g. on Enter/Tab key press).
        """
        self._batch_state = batch_state
        self.index = form_index
        self._fields = fields
        self.validation_result = None
        self._widget_config = widget_config if (widget_config is not None) else {}
        self.log = l_(log)

    def field(self, field_name):
        return Field(field_name, self)

    # --- navigation ----------------------------------------------------------
    def first_invalid_field(self, ignore_readonly_fields=True):
        return self._next_invalid_field(current_field=None, ignore_readonly_fields=ignore_readonly_fields)

    def next_field(self, current_field=None, ignore_readonly_fields=True):
        """
        This function determines which field should be edited (i.e. focused)
        next while taking "validation mode" into account.

        "current_field" is the field currently focused or None if no field
        is focused (i.e. new form which is just being displayed and no focus
        has been set so far).

        If we have a validation result the code will return the next INVALID
        field (i.e. which has a validation error or warning). If all remaining
        fields are valid it will return None (the caller should probably take
        care to switch to another form).

        If there is no validation result the code will return the next field
        (as defined by the order of fields given in the constructor). If the
        focus is already in the last field it will return None.
        """
        if self.validation_result is not None:
            return self._next_invalid_field(current_field, ignore_readonly_fields=ignore_readonly_fields)

        if current_field is None:
            first_field = self._fields[0]
            if (ignore_readonly_fields == False) or (not self._is_field_readonly(first_field)):
                return first_field
            current_field = first_field

        field_for_index = lambda idx: self._fields[idx] if (idx < len(self._fields)) else None
        current_index = self._fields.index(current_field)
        if ignore_readonly_fields:
            next_index = current_index + 1
            next_field = field_for_index(next_index)
            while (next_field is not None) and self._is_field_readonly(next_field):
                next_index += 1
                next_field = field_for_index(next_index)
        else:
            next_index = current_index + 1
            next_field = field_for_index(next_index)
        return next_field


    def _next_invalid_field(self, current_field=None, ignore_readonly_fields=True):
        """
        Return the next invalid field given the current field (which is usually
        the current focus position) or None if there is no invalid
        field left (with a higher index, previous fields might contain errors).
        """
        if current_field is None:
            current_index = -1
        else:
            current_index = self._fields.index(current_field)

        next_fields = self._fields[current_index+1:]
        for field in next_fields:
            if ignore_readonly_fields and self._is_field_readonly(field):
                continue
            field_errors = self.validation_result.errors.get(field.name, ())
            if field_errors:
                return field
        return None

    def _is_field_readonly(self, field):
        field_config = self._widget_config.get(field.name, {})
        is_readonly = (field_config.get('read_only') == True)
        return is_readonly


    # --- validation ----------------------------------------------------------
    def validate_values(self):
        """Validate the current form values and update the validation result."""
        validator = self._batch_state.validator
        if validator is None:
            return
        current_index = self._batch_state.index_current_form
        self.log.debug('validating form #%d', current_index)
        current_form = self._batch_state.batch.form(current_index)
        result = validator.process_form(current_form, current_index)
        self.validation_result = result

        # add additional debug logging
        if not result.contains_errors():
            self.log.debug('no validation errors in form #%d', current_index)
            return
        self.log.debug('form #%d contains validation errors', current_index)
        for field_name, errors in result.errors.items():
            if len(errors) == 0:
                continue
            for error in errors:
                type_ = 'warning' if getattr(error, 'is_warning', False) else 'error'
                msg = '  %s "%s": %s (%s)' % (field_name, result.initial_value[field_name], error.key, type_)
                self.log.debug(msg)
