# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from ddc.storage import TaskType


__all__ = ['BatchState']

class BatchState(object):
    """Encapsulate all (transient) state regarding a certain batch.  e.g.
    Which forms have errors?
    Given a BatchState you can decide which form you need to jump to using Enter
    """
    def __init__(self, batch, *, current_form=None, validator=None):
        self.batch = batch
        self.validator = validator
        self._current_form = current_form
        self.editing_stack = []
        self._index_editing_stack = None

    @property
    def index_current_form(self):
        if self.current_form is None:
            return None
        return self.current_form.index

    @property
    def current_form(self):
        return self._current_form

    def set_current_form(self, form_state):
        self._current_form = form_state

    # --- convenience API -----------------------------------------------------
    # Later on we might want to split these methods into separate classes and
    # convert BatchState into a class which only stores data (without methods).
    # However as long as the code is short and simple, let's just put it here.
    # BUT: don't forget to do the refactoring once code size becomes an issue!
    def next_form_index(self, *, current_index=None, validation_mode=True):
        """
        Return the next form index given the current position (instance state).
        If validation_mode is True this will return the next form with errors
        which have to be resolved by the current user.
        If the current form is the last one of the given batch return None.
        """
        current_index = current_index if (current_index is not None) else -1
        index_last_form = len(self.batch.forms()) -1
        if not validation_mode:
            next_index = current_index + 1
            return next_index if (next_index <= index_last_form) else None
        new_tasks = self.batch.new_tasks(TaskType.VERIFICATION)
        # we could assume that all previous forms have no open verification
        # tasks (as the user would usually fix the errors associated with them
        # so they'd be marked as "closed" afterwards).
        # However the user should be able to jump to the next form in case there
        # are unresolvable tasks.
        potential_tasks = [t for t in new_tasks if (t.form_index > current_index)]
        # LATER: need to filter tasks in case the user is only allowed to work
        # on specific fields / remove hits for read-only fields
        # Currently pydica does not skip forms with errors even if all errors
        # refer to read-only fields. Ideally we should just skip these forms.
        if len(potential_tasks) == 0:
            return None
        next_index = min((task.form_index for task in potential_tasks))
        return next_index

    # --- edit stack ----------------------------------------------------------
    def record_field_edit(self, form_index, field_name):
        stack_item = (form_index, field_name)
        if self.editing_stack and (self.editing_stack[-1] == stack_item):
            # prevent duplicates
            return
        # when a new edit was made, just put it at the end of the
        # editing stack so we don't loose any "history"
        # Alternatively we could employ a model similar to common
        # implementations of undo/redo where the "redo" chain is deleted
        # once a new edit was made.
        # In the end I think it doesn't matter too much either way as
        # a user will likely jump to the next invalid field (using
        # TAB/ENTER) instead of using UP/DOWN again.
        self.editing_stack.append(stack_item)
        self._index_editing_stack = None

    def previous_field_from_edit_stack(self):
        if len(self.editing_stack) == 0:
            return None
        if self._index_editing_stack is None:
            self._index_editing_stack = len(self.editing_stack)
        elif self._index_editing_stack <= 0:
            return None
        self._index_editing_stack -= 1
        return self.editing_stack[self._index_editing_stack]

    def next_field_from_edit_stack(self):
        if (len(self.editing_stack) == 0) or (self._index_editing_stack is None):
            return None
        if self._index_editing_stack + 1 >= len(self.editing_stack):
            # setting the index to None helps to ease adding new items to the
            # stack
            self._index_editing_stack = None
            return None
        self._index_editing_stack += 1
        return self.editing_stack[self._index_editing_stack]
