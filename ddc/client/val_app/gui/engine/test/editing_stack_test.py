# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from pythonic_testcase import *

from ddc.client.val_app.gui.engine import batch_state_for_values, FormState
from ddc.lib.attribute_dict import AttrDict
from ddc.validation.testutil import fake_plugin


class EditingStackTest(PythonicTestCase):
    def _batch_state_with_two_fields(self):
        foo = AttrDict(name='ABGABEDATUM')
        bar = AttrDict(name='AUSSTELLUNGSDATUM')
        form_values = (dict({foo.name: 'baz', bar.name: 'invalid'}),)
        plugin = fake_plugin({})

        batch_state = batch_state_for_values(form_values, plugin)
        return (batch_state, (foo, bar))

    def test_can_record_editing_stack(self):
        batch_state, (foo, bar) = self._batch_state_with_two_fields()
        state_form0 = FormState(batch_state, fields=(foo, bar), form_index=0)
        state_form1 = FormState(batch_state, fields=(foo, bar), form_index=5)
        batch_state.set_current_form(state_form0)

        batch_state.record_field_edit(1, foo.name)
        # not sure if this could happen as we only use the "textEdited" event
        # but without digging too deep I think it might happen that we set a new
        # current form before the event is fired by QT.
        # so this tests we really use the right form index from parameters...
        batch_state.set_current_form(state_form1)
        batch_state.record_field_edit(1, bar.name)
        batch_state.record_field_edit(2, bar.name)
        assert_equals(
            [(1, foo.name), (1, bar.name), (2, bar.name)],
            batch_state.editing_stack
        )

    def test_does_not_record_duplicate_edits_for_the_same_field(self):
        batch_state, (foo, bar) = self._batch_state_with_two_fields()
        state_form0 = FormState(batch_state, fields=(foo, bar), form_index=0)
        batch_state.set_current_form(state_form0)

        batch_state.record_field_edit(1, foo.name)
        batch_state.record_field_edit(1, foo.name)
        assert_equals([(1, foo.name)], batch_state.editing_stack)

        batch_state.record_field_edit(2, foo.name)
        assert_equals([(1, foo.name), (2, foo.name)], batch_state.editing_stack,
            message='however record edits for the same field in different forms')

    def test_can_navigate_through_editing_stack(self):
        batch_state, (foo, bar) = self._batch_state_with_two_fields()
        assert_none(batch_state.previous_field_from_edit_stack(),
            message='no editing stack recorded')
        assert_none(batch_state.next_field_from_edit_stack(),
            message='no editing stack recorded')

        batch_state.record_field_edit(1, foo.name)
        batch_state.record_field_edit(2, bar.name)
        assert_equals([(1, foo.name), (2, bar.name)], batch_state.editing_stack)

        assert_equals((2, bar.name), batch_state.previous_field_from_edit_stack())
        assert_equals((1, foo.name), batch_state.previous_field_from_edit_stack())
        assert_none(batch_state.previous_field_from_edit_stack(),
            message='reached start of batch')

        assert_equals((2, bar.name), batch_state.next_field_from_edit_stack())
        assert_none(batch_state.next_field_from_edit_stack(),
            message='reached end of batch')

        assert_equals((2, bar.name), batch_state.previous_field_from_edit_stack())
        batch_state.record_field_edit(2, foo.name)
        assert_none(batch_state.next_field_from_edit_stack(),
            message='index reset to none')
        assert_equals((2, foo.name), batch_state.previous_field_from_edit_stack())
