# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from pythonic_testcase import *

from pycerberus.api import Validator
from pycerberus.lib.form_data import FormData
from pycerberus.validators import IntegerValidator

from ddc.client.val_app.gui.engine import batch_state_for_values, FormState, ValidationResult
from ddc.lib.attribute_dict import AttrDict
from ddc.validation.testutil import fake_plugin


class FormStateTest(PythonicTestCase):
    def test_can_tell_if_validation_complained_about_field_contents(self):
        state = FormState(None, (), form_index=-1)
        for field_name in ('foo', 'bar', 'baz'):
            assert_equals(
                ValidationResult.OK,
                state.field(field_name).validation_state
            )

        form_data = FormData(child_names=('foo', 'bar', 'baz'))
        form_data.set(errors={
            'bar': (dict(key='too_short', message='bar', is_warning=False, meta=None),),
            'baz': (
                dict(key='too_long', message='baz', is_warning=True, meta=None),
                dict(key='too_old', message='baz', is_warning=False, meta=None),
            ),
        })
        state.validation_result = form_data
        assert_equals(ValidationResult.OK, state.field('foo').validation_state)
        assert_equals(ValidationResult.ERROR, state.field('bar').validation_state)
        assert_equals(ValidationResult.WARNING, state.field('baz').validation_state,
            message='should return state for first error and ignore other errors')

    def test_can_tell_if_field_contains_invalid_contents(self):
        state = FormState(None, (), form_index=-1)
        form_data = FormData(child_names=('foo', 'bar', 'baz'))
        form_data.set(errors={
            'bar': (dict(key='too_short', message='bar', is_warning=False, meta=None),),
            'baz': (dict(key='too_long', message='baz', is_warning=True, meta=None),),
        })
        state.validation_result = form_data
        assert_true(state.field('foo').is_valid())
        assert_false(state.field('bar').is_valid())
        assert_false(state.field('baz').is_valid())

    def test_can_trigger_validation(self):
        # we need to use real field names because cdb_tool check for unknown
        # fields (to detect bad CDB files). However for the purposes of this
        # test the field names are completely arbitrary.
        foo = AttrDict(name='ABGABEDATUM')
        bar = AttrDict(name='AUSSTELLUNGSDATUM')
        form_values = (dict({foo.name: 'baz', bar.name: 'invalid'}),)
        plugin = fake_plugin({foo.name: Validator(), bar.name: IntegerValidator()})

        batch_state = batch_state_for_values(form_values, plugin)
        state = FormState(batch_state, fields=(foo, bar), form_index=0)
        batch_state.set_current_form(state)
        assert_none(state.validation_result)

        state.validate_values()
        assert_not_none(state.validation_result)
        assert_true(state.field(foo.name).is_valid())
        assert_false(state.field(bar.name).is_valid())

    def test_can_find_next_field_without_validation_result(self):
        foo = AttrDict(name='foo')
        bar = AttrDict(name='bar')
        state = FormState(None, fields=(foo, bar), form_index=-1)
        assert_none(state.validation_result)

        assert_equals(foo, state.next_field())
        assert_equals(bar, state.next_field(current_field=foo))
        assert_none(state.next_field(current_field=bar))

    def test_can_ignore_readonly_fields_without_validation_result(self):
        foo = AttrDict(name='foo')
        bar = AttrDict(name='bar')
        widget_config = {'foo': {'read_only': True}}
        state = FormState(None, fields=(foo, bar), form_index=-1, widget_config=widget_config)
        assert_none(state.validation_result)

        assert_equals(foo, state.next_field(current_field=None, ignore_readonly_fields=False))
        assert_equals(bar, state.next_field(current_field=None, ignore_readonly_fields=True),
            message='should ignore first field "foo" which is read-only')

        state._widget_config = {'bar': {'read_only': True}}
        assert_equals(bar, state.next_field(current_field=foo, ignore_readonly_fields=False))
        assert_none(state.next_field(current_field=foo, ignore_readonly_fields=True),
            message='bar is read only so there is no other field')

    def test_can_find_next_field_with_validation_result(self):
        foo = AttrDict(name='foo')
        bar = AttrDict(name='bar')
        baz = AttrDict(name='baz')
        fields = (foo, bar, baz)
        field_names = list(map(lambda f: f.name, fields))
        state = FormState(None, fields, form_index=-1)
        form_data = FormData(child_names=field_names)
        form_data.set(errors={
            'bar': (dict(key='too_short', message='bar', is_warning=False, meta=None),),
            'baz': (dict(key='too_long', message='baz', is_warning=True, meta=None),),
        })
        state.validation_result = form_data

        assert_equals(bar, state.next_field(current_field=None),
            message='should be able to find first invalid field in form')
        assert_equals(bar, state.next_field(current_field=foo))
        assert_equals(baz, state.next_field(current_field=bar))
        assert_none(state.next_field(current_field=baz))

        state._widget_config = {bar.name: {'read_only': True}}
        assert_equals(bar, state.next_field(current_field=foo, ignore_readonly_fields=False))
        assert_equals(baz, state.next_field(current_field=foo, ignore_readonly_fields=True))

        state._widget_config = {baz.name: {'read_only': True}}
        assert_equals(baz, state.next_field(current_field=bar, ignore_readonly_fields=False))
        assert_none(state.next_field(current_field=bar, ignore_readonly_fields=True))

    # --- internal helpers ----------------------------------------------------

