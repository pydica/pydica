# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from pythonic_testcase import *

from ddc.client.val_app.gui.engine.batch_state import BatchState
from ddc.storage import db_schema, get_model, Batch, TaskStatus, TaskType
from ddc.storage.testhelpers import create_bunch
from ddc.lib.attribute_dict import AttrDict


class BatchStateTest(PythonicTestCase):
    def test_can_return_next_form_index_in_non_validation_mode(self):
        bunch = create_bunch(nr_forms=10)
        batch = Batch.init_from_bunch(bunch, create_persistent_db=False)
        state = BatchState(batch)

        assert_equals(0,
            state.next_form_index(current_index=None, validation_mode=False))
        assert_equals(6,
            state.next_form_index(current_index=5, validation_mode=False))

        index_last_form = len(batch.forms()) - 1
        index_before_last = index_last_form - 1
        assert_equals(index_last_form,
            state.next_form_index(current_index=index_before_last, validation_mode=False),
            message='should be able to go to the last form - guard against off-by-one')
        assert_none(state.next_form_index(current_index=index_last_form, validation_mode=False),
            message='should return None if there is no "next" form')

    def test_can_return_next_form_index_in_validation_mode(self):
        # TODO: shuffle form numbers, multiple tasks for a form
        model = get_model(db_schema.LATEST)
        task_meta = AttrDict(errors=({},))
        tasks = (
            # should ignore non-verification tasks
            model.Task(1, 'dummytype', field_name='BRUTTO', data=task_meta),
            model.Task(2, TaskType.VERIFICATION, field_name='BRUTTO', data=task_meta),
            model.Task(6, TaskType.VERIFICATION, field_name='BRUTTO', data=task_meta),
            # should ignore closed tasks
            model.Task(8, TaskType.VERIFICATION, status=TaskStatus.CLOSED, data=task_meta),
        )
        # We might have to ensure that the form data is actually invalid for
        # the fields referenced in task_meta above. Currently this is no
        # concern so let's keep the test simple.
        bunch = create_bunch(nr_forms=10, tasks=tasks, model=model)
        batch = Batch.init_from_bunch(bunch, create_persistent_db=False)
        state = BatchState(batch)

        assert_equals(2, state.next_form_index(current_index=0, validation_mode=True),
            message='should consider only verification tasks')

        assert_equals(6, state.next_form_index(current_index=2, validation_mode=True))

        assert_none(state.next_form_index(current_index=6, validation_mode=True),
            message='should return None if there are no errors left (and should ignore closed tasks)')

    def test_can_return_first_form_in_next_form_index_in_validation_mode(self):
        task_meta = AttrDict(field_name='BRUTTO', errors=({},))
        model = get_model(db_schema.LATEST)
        tasks = (
            model.Task(0, TaskType.VERIFICATION, status=TaskStatus.NEW, data=task_meta),
        )
        # We might have to ensure that the form data is actually invalid for
        # the fields referenced in task_meta above. Currently this is no
        # concern so let's keep the test simple.
        bunch = create_bunch(nr_forms=10, tasks=tasks, model=model)
        batch = Batch.init_from_bunch(bunch, create_persistent_db=False)
        state = BatchState(batch)

        assert_equals(0, state.next_form_index(current_index=None, validation_mode=True),
            message='should be able to return also the first form as "next" for new batches')

    # --- custom assertions ---------------------------------------------------

    # --- internal helpers ----------------------------------------------------

