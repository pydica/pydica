# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from srw.rdblib import DataBunch
from srw.rdblib.cdb import create_cdb_with_form_values
from srw.rdblib.ibf import create_ibf

from ddc.client.val_app.gui.engine import BatchState
from ddc.storage import create_sqlite_db, Batch
from ddc.validation import validation_module_for_batch
from ddc.validation.testutil import build_context


__all__ = ['batch_state_for_values']

def batch_state_for_values(form_values, plugin):
    bunch = DataBunch(
        cdb=create_cdb_with_form_values(form_values),
        ibf=create_ibf(nr_images=len(form_values)),
        db=create_sqlite_db(),
        ask=None,
    )
    batch = Batch.init_from_bunch(bunch)
    context = build_context(plugin=plugin)

    validator = validation_module_for_batch(batch, bunch, context)
    return BatchState(batch, validator=validator)

