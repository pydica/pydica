# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

'''
Grid Building
-------------

GridBuilder installs a grid layout and adds QLineEdit and QCheckbox
objects, based upon some heuristics.

Currently, it uses a standard grid layout at pretty high resolution.
This may be replaced by a custom grid control in the future.
'''

import ddc.config.pyside_fix
from PySide import QtCore, QtGui

from ddc.client.tool.field_cleaner import remove_overlaps, kill_duplicates
from ddc.platf.platform_quirks import map_fontsize
from .customwidgets import PydicaLineEdit


class settings_base(object):
    '''
    this base class acts like a template for common settings classes.
    The derived classes should carefully avoid anything special, so
    they are simple placeholders to put some properties into them.
    '''
    def __init__(self, **kwds):
        # handling static values:
        # turning class attributes into instance values
        for key, val in self.__class__.__dict__.items():
            if not key.startswith('_'):
                setattr(self, key, val)

        for kw, val in kwds.items():
            if hasattr(self, kw) and not kw.startswith('_'):
                setattr(self, kw, val)
            else:
                raise AttributeError('{} has no definition for {}'
                                     .format(self.__class__, kw))

class GridDefaultCfg(settings_base):
    '''
    this simple class is a starter for more sophisticated configs which
    are to be defined (allowed members, persistence, etc.).
    For now, a simple instance with some entries is sufficient.
    '''
    default_font_scale = 1.0
    default_font_size = 16.0
    default_line_height = 24.0
    char_scale = 2.0       # string_width / char_scale gives field_width
    font_scale = default_font_scale
    font_min_scale = 0.75  # some good size for small displays
    font_max_scale = 3.0   # enough size for large displays
    points_range_limit = (1, 72)  # for the main text font.
    # XXX what to do if there is more than one font?


class GridBuilder(object):
    '''
    A class for building a flexible grid of text controls.

    A grid is using a field list as defined in config_base.py .
    '''

    def __init__(self, field_list, target, eventfilter_textfields=None):
        '''
        Take a field list and create a grid with text controls.
        The text controls are arranged according to the position,
        size and order of the field list.

        All fields get an according widget with some default font and layout.
        '''
        self.field_list = field_list
        self.target = target
        self.config = GridDefaultCfg()

        self._build_controls(eventfilter_textfields)
        self.set_font_scaling(self.config.default_font_scale)
        # set the builder now to avoid early changes
        target.builder = self

    def set_font_scaling(self, scale):
        '''
        modify the current font to a given scaling.
        '''
        c = self.config
        scale = max(min(c.font_max_scale, scale), c.font_min_scale)
        self.config.font_scale = scale
        points = int(self.config.default_font_size * scale)

        widgets = (field.widget for field in self.field_list)
        for widget in widgets:
            if isinstance(widget, QtGui.QLineEdit):
                font = widget.font()
                font.setPointSize(map_fontsize(points))

                widget.setFont(font)
                w = widget.width_in_pixels
                widget.setFixedWidth(w / self.config.char_scale *
                                     self.config.font_scale)
                widget.setFixedHeight(self.config.default_line_height *
                                      self.config.font_scale)
        self.font_index = points

    def set_font_points(self, points):
        '''
        calculate and set the scale via points (by the largest font used).
        Returns the actually accepted points.
        '''
        assert points == int(points), 'no fractions allowed for points()'
        mi, ma = self.config.points_range_limit
        assert points in range(mi, ma+1), 'points only defined from 1..72'
        ref_points = self.config.default_font_size
        scale = points / ref_points
        self.set_font_scaling(scale)
        return self.font_index

    def _fits_into_area(self, target_width):
        # our own width must be <= the layout width
        own_width = self.target.layout().totalMinimumSize().width()
        return own_width <= target_width

    def set_best_fitting_scale(self, maxw):
        '''
        Iterate over font sizes to maximize the font without scroll bars.
        This has initially some visually effect.
        '''
        points = self.font_index
        mi, ma = self.config.points_range_limit
        while self._fits_into_area(maxw) and points < ma:
            points, p2 = self.set_font_points(points + 1), points
            if not self._fits_into_area(maxw) or points == p2:
                break
        while not self._fits_into_area(maxw) and points > mi:
            points, p2 = self.set_font_points(points - 1), points
            if self._fits_into_area(maxw) or points == p2:
                break
        ref_points = self.config.default_font_size
        scale = points / ref_points
        return scale


    def _build_controls(self, eventfilter_textfields):
        # We need retrieve the field configuration directly from the SRW plugin
        # anyway so we can just as well also get the checkbox names here...
        from srw.pydica.fields import CHECKBOXES

        field_list = self.field_list
        kill_duplicates(field_list)
        remove_overlaps(field_list)

        page = self.target
        grid = QtGui.QGridLayout(page)
        page.setLayout(grid)
        grid.setHorizontalSpacing(0)
        grid.setVerticalSpacing(0)
        width, height = field_list.size
        maxrow, maxrowspan = 0, 0
        maxcol, maxcolspan = 0, 0

        # quick hack for checkbox align
        check_col = None

        for field in field_list:
            x, y, w, h = list(map(float, field.rect))
            # assume nl lines and nc columns
            nl = 500
            nc = 200
            row = (nl * y) / height
            col = (nc * x) / width
            rowspan = max(nl * h / height, 1)
            colspan = max(nc * w / width, 1)
            row, col, rowspan, colspan = list(map(int, (row, col, rowspan, colspan)))

            is_lineedit = False
            if field.name in CHECKBOXES:
                checkbox_field = QtGui.QCheckBox(page)
                txt = field.field_name
                # hack, we need a mapper
                txt = txt.replace('arbeits', 'arbeits-_')
                txt = txt.replace('_', '\n')
                # removed the text to save space.
                # we need to add a stylesheet to set the real dimensions
                checkbox_field.setText(txt and ' \n\n')
                font = checkbox_field.font()
                font.setPointSize(map_fontsize(9))
                checkbox_field.setFont(font)
                #checkbox_field.setFixedSize(40, 40)
                #XXX checkboxes need CSS formatting. Later
                if check_col is None:
                    check_col = col
                col = check_col
                gui_field = checkbox_field
                is_lineedit = False
            else:
                edit_field = PydicaLineEdit(page, eventfilter=eventfilter_textfields)
                font = edit_field.font()
                font.setPointSize(map_fontsize(self.config.default_font_size * self.config.default_font_scale))
                edit_field.setFont(font)

                # XXX here is quite some tweaking necessary:
                # adjust font size and scaling by good defaults of the screen size.
                edit_field.width_in_pixels = w
                edit_field.setFixedWidth(w / self.config.char_scale * self.config.default_font_scale)
                edit_field.setFixedHeight(self.config.default_line_height * self.config.default_font_scale)
                gui_field = edit_field
                is_lineedit = True

            if field.coord_down:
                txt = 'apo_fsrh.ini'
            else:
                txt = 'fields.ini'
            gui_field.setToolTip('\n'.join([
                field.field_name,
                repr((row, col, rowspan, colspan)),
                'rect %s' % repr(field.rect),
                'bbox %s from %s' % (field.bbox, txt), ]))

            #gui_field.setWhatsThis('\n'.join([
            # this is for later
            if not field.correction:
                gui_field.setEnabled(False)
            grid.addWidget(gui_field, row, col, rowspan, colspan)
            maxrow, maxrowspan = max(row, maxrow), max(rowspan, maxrowspan)
            maxcol, maxcolspan = max(col, maxcol), max(colspan, maxcolspan)

            # remove the focus shadow on the Mac:
            gui_field.setAttribute(QtCore.Qt.WA_MacShowFocusRect, 0)

            # add the widget also to the field
            field.widget = gui_field
            field.is_lineedit = is_lineedit
            field.is_checkbox = not is_lineedit
            gui_field.field = field

        # text area centered as well
        # XXX make this configurable, evaluate Alignment

        ################# vertical alignment
        # this shifts to the top, because the bottom right spacer is at Y maximum
        ## width, height, wpolicy, hpolicy
        spacer = QtGui.QSpacerItem(0, 0, QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Expanding)
        ## row, column, rowspan colspan
        grid.addItem(spacer, maxrow+maxrowspan, maxcol+maxcolspan, 1, 1)

        # this shifts to the bottom, because the top left spacer is at Y minimum
        ## width, height, wpolicy, hpolicy
        spacer = QtGui.QSpacerItem(0, 0, QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Expanding)
        ## row, column, rowspan colspan
        if False:
            grid.addItem(spacer, 0, 0, 1, 1)
        # together they center vertically

        ################# horizontal alignment
        # this shifts to the right, because the bottom left spacer is at X minimum
        ## width, height, wpolicy, hpolicy
        spacer = QtGui.QSpacerItem(0, 0, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        ## row, column, rowspan colspan
        grid.addItem(spacer, maxrow+maxrowspan, 1, 1)

        # this shifts to the left, because the top right spacer is at X maximum
        ## width, height, wpolicy, hpolicy
        spacer = QtGui.QSpacerItem(0, 0, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        ## row, column, rowspan colspan
        grid.addItem(spacer, 0, maxcol+maxcolspan, 1, 1)
        # together they center horizontally
