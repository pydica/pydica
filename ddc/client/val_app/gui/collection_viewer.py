# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import functools
from functools import partial
import os

from PySide import QtCore, QtGui

import ddc
from ddc.client.config.config_base import FieldList
from .engine import BatchState, FormState, ValidationResult
from .cdb_opener import open_batch_with_ui_progress
from .css_module import LineEdit_Style as Style
from .fieldpainting import FieldPainter
from .prescription_viewer import PrescriptionViewer
from .srw_ascii import start_ascii_export
from .srw_reklafax import display_reklafax_dialog
from ddc.lib.attribute_dict import AttrDict
from ddc.storage import TaskType
from ddc.tool.cdb_collection import CDB_Collection
from ddc.validation import batch_validation, validation_module_for_batch


__all__ = ['CollectionViewer']


def has_correction(field_data):
    if field_data.value is None:
        return False
    return (field_data.value != field_data.initial_value)


class KeypressFilter(object):
    def __init__(self, controller):
        super(KeypressFilter, self).__init__()
        self.controller = controller

    def filter_keypress(self, widget, event):
        event_type = event.type()
        if event_type == QtCore.QEvent.KeyPress:
            key = event.key()
            if key in (QtCore.Qt.Key_Return, QtCore.Qt.Key_Enter, QtCore.Qt.Key_Tab):
                self.controller.switch_focus_to_next_field(widget)
                return True
            elif key == QtCore.Qt.Key_Up:
                self.controller.switch_focus_to_previously_edited_field(widget)
                return True
            elif key == QtCore.Qt.Key_Down:
                self.controller.switch_focus_to_next_edited_field(widget)
                return True
        elif event_type == QtCore.QEvent.FocusIn:
            self.controller._set_focus_for_field(widget.field)
        return False

def update_view(func):
    'replace a method with a construct that ensures to call _update_view'
    def wrapper(self, *args, **kwargs):
        try:
            result = func(self, *args, **kwargs)
        finally:
            self._update_view()
        return result
    return functools.wraps(func)(wrapper)


class CollectionViewer(object):
    """This is intended to be the main controller. This class contains
    references to views and "models" (e.g. data abstraction classes).

    Right now this is a bit convoluted but the class will be split over time.
    Mostly all GUI-related setup needs to be moved out of this class.
    I'm going with an evolutionary approach instead of radically cutting things
    because this class is also used by the remote control.
    """
    def __init__(self, context, *, gui=None):
        self.context = context
        # redirector: make closeEvent available in pyobject
        self.gui = PrescriptionViewer(redirector=self) if (gui is None) else gui
        self.gui.image_label.painter = None
        self.gui.zoom_label.painter = None

        self.create_actions()
        self.create_menus()
        self.create_connections()
        self._batch_state = None
        self.seq = None
        self.first_show = True
        self.grid_built = False
        self.highlight_fields = set()
        # default fix-up
        self.gui.stackedWidget.setCurrentIndex(0) # override whatever the GUI
                                                  # was saved with

        self.log = self.context.get_logger('pydica.collectionviewer')
        self.field_list = []
        self.textfield_list = []

        # validation support: a validation module is necessary
        self.val_module = None
        self.gui.set_display_validation(True)
        self.gui.set_validation_interaction(False)
        self._timer = self._setup_focus_timer()

    def _setup_focus_timer(self):
        # QT has a very strange behavior (which is considered a bug by many users
        # though I did not find any official QT developer answer to that).
        # If you just call ".setFocus()" on a QLineEdit the cursor won't be visible
        # (but the widget gets the "focus" from QT's point of view).
        # Another symptom is that the zoomed image and the zoomed input are not
        # updated when starting the application (they will be updated after the
        # user focussed a different line edit window).
        # Using the timer also has the side effect to hide several other problems
        # like green boxes+labels visible for all fields in the image. The timer
        # "solves" these problems from a user point of view but I suspect there are
        # some bugs/missing checks in the initial setup.
        #
        # For development purposes the actual timer can be removed by just calling
        # "._set_focus_for_field_delayed()" directly from "_set_focus_for_field()"
        # Ideally I'd like to get rid of the timer but part of the problem seems to
        # the implementation of QLineEdit and QT's event flow.
        #
        # a timer for inverting the computation order of focusIn
        timer = QtCore.QTimer(self.gui)
        timer.setInterval(0)
        timer.setSingleShot(True)
        # dummy, not used but for disconnect:
        timer.timeout.connect(self._set_focus_for_field_delayed)
        return timer

    def _initialize_field_model(self, size=None):
        # "self.field_list" contains something like a model for the displayed
        # fields. However some parts of the GUI depend on the "rec" attribute
        # which is only present in the CDB data abstraction.
        field_list = self.field_list if (size is None) else FieldList(size)
        for field in field_list:
            field.rec = self.seq.current_form.fields[field.link_name].rec
        return field_list

    # --- legacy (internal) API -----------------------------------------------
    # keep internal API stable so we can finish the batch/formstate refactoring
    @property
    def validation_result(self):
        return (self._form_state is not None) and self._form_state.validation_result

    @validation_result.setter
    def validation_result(self, value):
        assert self._form_state is not None
        self._form_state.validation_result = value

    @property
    def pos(self):
        return self._batch_state.current_form.index

    # -------------------------------------------------------------------------
    # support to report unhandled exceptions
    def context_for_exception_report(self):
        context_string = ''
        if self._batch_state:
            batch = self._batch_state.batch
            cdb_path = batch.bunch.cdb
            if self._batch_state.current_form:
                form_index = self._batch_state.current_form.index
                pic = batch.pic_for_form(form_index)
            else:
                form_index = '[not set]'
                pic = '[not set]'
            lines = [
               'CDB:  %s' % cdb_path,
               'form: %s' % form_index,
               'PIC:  %s' % pic
            ]
            context_string = '\n'.join(lines)
        return AttrDict(
            env_path=self.context.get('env_dir'),
            extra_info=context_string,
        )

    def notify_about_unhandled_exception(self, report_path):
        self.gui.msgbox_critical(
            'Schwerer Programmfehler',
            'Leider ist ein völlig unerwarteter Fehler im Programm aufgetreten.'
        )

    # -------------------------------------------------------------------------

    @property
    def _form_state(self):
        if self._batch_state is None:
            # prevent exception on Windows 10 (Python 3.3 / 32 bit) when closing
            # the application without storing changed data
            return None
        return self._batch_state.current_form

    def display_validation_results(self):
        return (self.val_module is not None) and self.gui.display_validation

    def use_validation_for_navigation(self):
        return self.display_validation_results() and self.gui.validation_interaction

    def open_sequence(self, imgseq, form_index=None):
        if self._batch_state:
            # LATER: decide what to do with modified data...
            self.close_current_batch(store_modified_data=True)
        batch = imgseq.batch
        bunch = batch.bunch
        cdb_basename = os.path.basename(bunch.cdb)
        self.val_module = validation_module_for_batch(batch, bunch, self.context)
        self.log = self.context.get_logger('pydica.gui', context=cdb_basename)
        self.seq = imgseq
        self._batch_state = BatchState(batch, validator=self.val_module)
        if form_index is None:
            form_index = self._batch_state.next_form_index(
                current_index=None,
                validation_mode=self.use_validation_for_navigation()
            )
            if form_index is None:
                # This can happen if there are no verification tasks for the new batch.
                # TODO: We should handle "no open tasks" better (e.g. notify the user
                # with a message box) but this at least avoids a crash.
                form_index = 0
        self.seq.seek(form_index)
        self.count = self.seq.form_count
        self.gui.num_recipies.setText(str(self.count))
        fname = bunch.cdb
        data_dir = self.context.settings.get('datadir', ddc.rootpath)
        try:
            fname = os.path.relpath(fname, data_dir)
        except ValueError:
            pass
        self.gui.set_form_info(pic=None, form_index=form_index, filename=fname)
        self.gui.setWindowTitle('%s - %s' % (
            self.gui.title_text, os.path.basename(fname)))
        self._page_action(form_index, force=True)

        if self.first_show:
            self.gui.ensure_text_visible()
            self.first_show = False

        # side effect: we have the edit list now

    def open_sequence_dlg(self):
        data_dir = self.context.settings.get('datadir', QtCore.QDir.currentPath())
        default_type = '.RDB'
        cdb_path = self.seq.batch.bunch.cdb if self.seq else ''
        is_default_data = cdb_path.startswith(ddc.rootpath)
        if not is_default_data:
            data_dir = os.path.dirname(cdb_path)
            extension = os.path.splitext(cdb_path)[1]
            default_type = extension.upper()
        name_filters = (
            'RDB-Dateien (*.rdb *.RDB)',
            'CDB-Dateien (*.cdb *.CDB)',
        )
        if default_type == '.CDB':
            name_filters = tuple(reversed(name_filters))
        filename, _ = QtGui.QFileDialog.getOpenFileName(
            self.gui,
            "Stapel öffnen",
            data_dir,
            ';;'.join(name_filters)
        )
        qt_app = QtGui.QApplication.instance()
        qt_app.processEvents()
        if filename:
            # note: with the standard dialog we cannot change the current
            # working dir, without having a file selected for opening!
            cdb_basename = os.path.basename(filename)
            cdb_log = self.context.get_logger('pydica.storage.cdb', context=cdb_basename)
            batch = open_batch_with_ui_progress(filename, self.gui, self.context, log=cdb_log)
            if batch is None:
                return
            seq = CDB_Collection(batch, log=cdb_log)
            self.open_sequence(seq)
        # PySide: file dialog seems to loose focus
        self.gui.activateWindow()

    def goto_form(self, pos):
        self._page_action(pos)

    def goto_form_dlg(self):
        gui = self.gui
        pos = self.pos + 1
        pos, ok = QtGui.QInputDialog.getInt(gui,
            'Belegnummer',
            'bitte geben Sie die Nummer des Belegs an',
            pos)
        # we can add range constraints here, but I think this is inconvenient

        if ok and (1 <= pos <= self.count):
            self.update_displayed_form_number(pos)
            self._page_action(pos - 1)

    def _find_field_by_name(self, field_name):
        # can be optimized by dicts if necessary
        for field in self.field_list:
            if field.field_name.lower() == field_name.lower():
                return field
        # not found, maybe this name is just a link name?
        for field in self.field_list:
            if field.link_name.lower() == field_name.lower():
                return field
        return None

    def setfocus_field(self, field_name):
        field = self._find_field_by_name(field_name)
        if field is None:
            self.log.error('Unable to set focus to field %r (field not found)', field_name)
            return
        self._set_focus_for_field(field)

    def field_highlight(self, field_name):
        field = self._find_field_by_name(field_name)
        if field:
            self.highlight_fields.add(field_name.upper())

    def clear_highlight_fields(self):
        self.highlight_fields = set()

    @update_view
    def delete_form(self, pic_nr):
        # This method is used by ediview
        self.log.info('delete current form if PIC=%s', pic_nr)
        current_form = self.seq.current_form
        if current_form.pic_nr != pic_nr:
            logmsg = 'form deletion requested for PIC %s but PIC %s is currently displayed '
            self.log.warning(logmsg, pic_nr, current_form.pic_nr)
            self.gui.msgbox_critical(
                'Eingabe-Fehler',
                'Der zu löschende Beleg wird nicht angezeigt (PIC=%s).' % pic_nr
            )
            return

        form_nr = self._batch_state.current_form.index + 1
        self.log.info('deleting form #%s (PIC=%s)', form_nr, current_form.pic_nr)
        current_form.delete()

    @update_view
    def delete_current_form(self):
        self.log.info('delete form %s', self.pos)
        self.seq.current_form.delete()

    @update_view
    def undelete_current_form(self):
        self.log.info('undelete form %s', self.pos)
        batch = self._batch_state.batch
        # Prevent duplicate PIC numbers after undeletion - this would be a very
        # serious inconsistency which will cause a lot of extra work.
        #
        # The idea is to run the batch validators with the new state (after
        # undeletion) and abort the operation if any errors are detected.
        # However our data model (Batch et al) do not provide sufficient
        # rollback mechanisms. Therefore the validators only get the current
        # state (form still deleted) and we need a workaround (special key in
        # context) to tell them about the undeletion.
        context = {'srw.assume_undeleted': self.pos}
        errors = batch_validation.process_batch(batch, context)
        # LATER: if we have more validators we should check if the found errors
        # actually refer to the current form.
        if len(errors) == 0:
            self.seq.current_form.undelete()
            return
        error = errors[0]
        self.gui.msgbox_critical(
            'FEHLER',
            error.message
        )

    def delete_current_form_dlg(self, delete_requested):
        form_is_deleted = self.seq.current_form.is_deleted()
        undelete_requested = not delete_requested
        if form_is_deleted and delete_requested:
            return
        elif (not form_is_deleted) and undelete_requested:
            return

        if delete_requested:
            action = self.delete_current_form
            title = "Beleg löschen"
            text = "wollen Sie den Beleg wirklich löschen?"
        else:
            action = self.undelete_current_form
            title = "Beleg entlöschen"
            text = "wollen Sie den Beleg wirklich wiederherstellen?"
        mbox = self.gui.msgbox_ok_cancel(
            title=title, text=text,
            default='CANCEL', use_cache=True
        )
        if mbox.exec_() == mbox.Ok:
            action()

    # -------------------------------------------------------------------------
    # SRW-specific functionality, should be moved into a plugin (but needs some
    # additional infrastructure).
    def reklafax_dlg(self, intern=False):
        display_reklafax_dialog(self.gui,
            self.seq, self.pos,
            log=self.log, context=self.context,
            intern=intern
        )

    def ascii_export_dlg(self):
        if (self.val_module is not None) and (self._form_state is not None):
            # Ensure that verification tasks exist for all unresolved errors
            # (so the safety checks during the ASCII export can do their job)
            current_form_index = self._batch_state.current_form.index
            validation_result = self._form_state.validation_result
            self.val_module.store_validation_state_as_tasks(current_form_index, validation_result)
        start_ascii_export(self.gui, self._batch_state.batch, log=self.log, cdb_collection=self.seq)

        # Update GUI after possible RDB->CDB rename
        batch = self._batch_state.batch
        form_index = self._batch_state.current_form.index
        pic = batch.pic_for_form(form_index)
        xdb_name = os.path.basename(batch.bunch.cdb)
        self.gui.set_form_info(pic=pic, form_index=form_index, filename=xdb_name)

    # -------------------------------------------------------------------------

    def show(self):
        # XXX Note that this raise method is crucial to force the app to be
        # in the foreground, while activateWindow() seems to do little about this.
        # This fact is poorly to not documented and caused a long search in
        # stackoverflow.
        self.gui.raise_()
        self.gui.show()

    def create_actions(self):
        gui = self.gui
        # these actions seem only to work if they are added to some menu

        self.prev_page_act = QtGui.QAction("Prev Page", gui,
                shortcut="PgUp", triggered=self.prev_page)
        self.next_page_act = QtGui.QAction("Next Page", gui,
                shortcut="PgDown", triggered=self.next_page)

        ###
        # new actions of the voucher menu
        gui.gotoFormAct = QtGui.QAction("Gehezu Beleg", gui,
                shortcut="F3", triggered=self.goto_form_dlg)

        gui.reklafaxInternAct = QtGui.QAction("Reklafax (Intern)", gui,
                shortcut="F10", triggered=lambda: self.reklafax_dlg(intern=True))

        gui.reklafaxAct = QtGui.QAction("Reklafax", gui,
                shortcut="F11", triggered=self.reklafax_dlg)

        gui.deleteFormAct = QtGui.QAction("Beleg löschen", gui,
                shortcut="F12", triggered=lambda: self.delete_current_form_dlg(delete_requested=True))

        gui.undeleteFormAct = QtGui.QAction("Beleg entlöschen", gui,
                triggered=lambda: self.delete_current_form_dlg(delete_requested=False))

        self.open_sequenceAct = QtGui.QAction("Stapel öffnen", gui,
                shortcut="Ctrl+O", triggered=self.open_sequence_dlg)

        self.asciiAct = QtGui.QAction("Ascii Export", gui,
            shortcut="Alt+A", triggered=self.ascii_export_dlg)

    def create_menus(self):
        gui = self.gui
        gui.viewMenu.addAction(self.prev_page_act)
        gui.viewMenu.addAction(self.next_page_act)

        gui.fileMenu.addAction(self.open_sequenceAct)

        gui.voucherMenu = QtGui.QMenu("Beleg", gui)
        gui.voucherMenu.addAction(gui.gotoFormAct)
        gui.voucherMenu.addAction(gui.reklafaxAct)
        gui.voucherMenu.addAction(gui.reklafaxInternAct)
        gui.voucherMenu.addAction(gui.deleteFormAct)
        gui.voucherMenu.addAction(gui.undeleteFormAct)

        gui.menuBar.addMenu(gui.voucherMenu)

        gui.exportMenu = QtGui.QMenu("Export", gui)
        gui.menuBar.addMenu(gui.exportMenu)
        gui.exportMenu.addAction(self.asciiAct)

    def create_connections(self):
        gui = self.gui
        gui.recipe_no_edit.returnPressed.connect(self.handle_entered_form_number)
        btn = gui.prev_button
        btn.clicked.connect(self.prev_page)
        btn.setToolTip('prev')
        btn = gui.next_button
        btn.clicked.connect(self.next_page)
        btn.setToolTip('next')
        gui.text_page.font_scaled.connect(self.on_font_scaled)

    def _update_view(self):
        # this is now called via a decorator!
        # refreshing the picture
        self.gui.repaint()
        # updating status indicators
        self._page_action(self.pos, force=True)

    def _is_form_dirty(self):
        is_cdb_dirty = self.seq and self.seq.current_form.is_dirty()
        is_sqlite_dirty = self._batch_state and self._batch_state.batch.db.is_dirty()
        return is_cdb_dirty or is_sqlite_dirty

    def _write_data_back(self):
        self.log.debug('writing data back to form')
        self.seq.commit()
        if not self._is_form_dirty():
            return
        form = self.seq.current_form
        form.set_dirty(False)
        for field in self.textfield_list:
            fld = form.fields[field.name]
            if fld.value != field.widget.text():
                self.log.debug('changed value for field %s: %r -> %r', field.name, fld.value, field.widget.text())
                fld.value = field.widget.text()
        form.write_back()

    def _page_action(self, new_form_index, force=False):
        if new_form_index is None:
            self.handle_end_of_batch()
            return
        if self._batch_state.current_form is None:
            current_form_index = None
            self.log.debug('switching to form #%s (no current form)', new_form_index)
        else:
            current_form_index = self._batch_state.current_form.index
            self.log.debug('switching to form #%s (currently viewing #%s, force=%s)', new_form_index, current_form_index, force)

        # even if validation results are not displayed we should still store
        # validation tasks so there is no inconsistent information about remaining
        # errors (SQLite vs. CDB).
        # Completely disabling the validation should be an "expert" option (maybe
        # only via command line/config file?) to minize the chance of screwups.
        if (self.val_module is not None) and (self._form_state is not None):
            self.val_module.store_validation_state_as_tasks(current_form_index, self._form_state.validation_result)

        if current_form_index == new_form_index and not force:
            self.log.debug('already at requested form #%s, nothing to do.', current_form_index)
            return
        if not self.seq:
            self.log.info('no seq opened, aborting form switch')
            return

        form_index = new_form_index
        self.seq.seek(form_index)
        # we need "self.textfield_list" to build a FormState
        # LATER: "_build_grid()" is pretty much static so it could be done at
        # startup (only slight refactorings needed as the pixmap is referenced).
        prescription_image = self.seq.load_image()
        self.gui.open(prescription_image)
        self._build_grid()

        # this also updates self.pos
        form_state = FormState(
            self._batch_state,
            self.textfield_list,
            form_index=form_index,
            log=self.log
        )
        self._batch_state.set_current_form(form_state)

        pic = self._batch_state.batch.pic_for_form(form_index)
        self.gui.set_form_info(pic, self._batch_state.current_form.index)

        self.field_list = self._initialize_field_model()
        widget_config = self._configure_widgets_for_current_form()
        self._form_state._widget_config = widget_config
        self._update_displayed_values()

        self._form_state.validate_values()
        self._focus_first_field_in_form()
        self._update_field_values_from_validation_result()
        self._update_error_states_for_fields()
        self.clear_highlight_fields()
        is_deleted = self.seq.current_form.is_deleted()
        self.gui.deleteFormAct.setDisabled(is_deleted)
        self.gui.undeleteFormAct.setDisabled(not is_deleted)

    def _configure_widgets_for_current_form(self):
        current_index = self._batch_state.current_form.index

        batch = self._batch_state.batch
        bunch = batch.bunch
        form = batch.form(current_index)
        form_values = dict((name, form.fields[name].value) for name in form.field_names)
        plugin_mgr = self.context['plugin_manager']
        kwargs = dict(form_values=form_values, bunch=bunch, context=self.context)
        widget_config = plugin_mgr.query_plugins('widget_configuration', **kwargs)

        for field in self.field_list:
            field_config = widget_config.get(field.name, {})
            is_readonly = (field_config.get('read_only') == True)
            if field.is_lineedit:
                field.widget.setReadOnly(is_readonly)
        return widget_config

    def _update_displayed_values(self):
        for field in self.field_list:
            txt = field.value
            if field.is_lineedit:
                field.widget.setText(txt)
            else:
                check_state = QtCore.Qt.Checked if txt else QtCore.Qt.Unchecked
                field.widget.setCheckState(check_state)

    # --- initialization ------------------------------------------------------
    def _initialize_form_fields(self):
        textfield_list = []
        for field in self.field_list:
            if field.is_lineedit:
                if field.widget.isEnabled():
                    textfield_list.append(field)
        return textfield_list

    def _build_grid(self):
        if self.grid_built:
            return
        # XXX refactor this code block and break into parts!
        #
        # build image fields (left)
        pixmap = self.gui.image_label.pixmap()
        self.gui.zoom_label.setPixmap(pixmap)
        size = pixmap.width(), pixmap.height()
        # the list of fields as used in the gui
        self.field_list = self._initialize_field_model(size)

        keypress_filter = KeypressFilter(self).filter_keypress
        self.gui.initialize_grid(self.field_list, keypress_filter)
        self.grid_built = True

        self._initialize_field_model()
        del_check = (lambda self=self: self.seq and
                     self.seq.current_form.is_deleted())
        for label in self.gui.image_label, self.gui.zoom_label:
            label.painter = FieldPainter(self.field_list, del_check)
            label.painter.set_aspect_ratio(self.gui.aspect_factor)
        self.textfield_list = self._initialize_form_fields()
        self._connect_handlers_for_text_fields()
        self.log.debug('grid building finished')

    def _connect_handlers_for_text_fields(self):
        for editfield in self.textfield_list:
            self.set_field_style(editfield)
            widget = editfield.widget
            # using "textEdited" signal instead of "textChanged" as the former
            # is only triggered by user actions while "textChanged" is more
            # generic and also called if the text is changed programmatically
            # (e.g. as a result of a validation heuristic, setting text after
            # switching to a new form).
            widget.textEdited.connect(partial(self.on_text_change, editfield))
            widget.getting_touched.connect(partial(self.on_lineedit_touched, widget))

    # --- END: initialization ------------------------------------------------------

    def on_text_change(self, field, new_text):
        form = self.seq.current_form
        form_index = self.seq.pos
        cdb_field = form.fields[field.name]
        cdb_field.value = field.widget.text()
        # we record a field edit as soon as the user changes a single character
        # even if he immediately reverts it e.g. by deleting the added
        # character.
        # (I don't think the different approaches matter too much in practice
        # as real users will likely use the editing stack in a very simple way.
        # However I spent some time on implementing this correctly and again
        # learnt about possible problems with QT's order of emitted events so
        # I'd like to document this.)
        #
        # Previously I tried to record a field edit only when leaving a field
        # (and if the field contents were actually changed) using the "focus in"
        # and "focus out" events. However this was problematic because sometimes
        # we got the "focus in" event before "focus out". Also in some
        # situations (using the mouse to change to a different form) our model
        # (BatchState) was already updated but the QT gui was not (so we needed
        # to store a lot of internal state).
        # The main problem was that I need to combine two events so I decided
        # to use the "textEdited" event which was way simpler to implement.
        self._batch_state.record_field_edit(form_index, field.name)
        self.seq.current_form.set_dirty(True)
        # we now also call the validation after each keypress,
        # but without writing changes inside a field (because that might
        # interfere badly with the users intention). What matters is if a field
        # would be valid afterwards
        self._form_state.validate_values()
        self._update_error_states_for_fields()
        self.set_status_field(field)
        # also update the graphics
        self.gui.image_label.update()
        self.gui.zoom_label.update()

    def set_field_style(self, field):
        if self._form_state is None:
            field_state = ValidationResult.OK
        else:
            field_state = self._form_state.field(field.name).validation_state
        if field.name in self.highlight_fields:
            style = Style.highlight
        elif not field.widget.isEnabled():
            style = Style.readonly
        elif not self.display_validation_results():
            style = Style.normal
        elif field_state == ValidationResult.OK:
            data_was_changed = (field.rec.recognizer_result != field.value)
            style = Style.normal if (not data_was_changed) else Style.valid
        elif field_state == ValidationResult.WARNING:
            style = Style.warning
        elif field_state == ValidationResult.ERROR:
            style = Style.error
        field.widget.setStyleSheet(style.stylesheet)
        field.widget.style_name = style.name

    def set_status_field(self, field):
        """display the current error message in the status widget"""
        first_error = ''
        if self.validation_result and self.display_validation_results():
            field_errors = self.validation_result.errors.get(field.name, ())
            if len(field_errors) > 0:
                first_error = field_errors[0].message
        self.gui.set_field_info(field.i18n, first_error)

    def _handle_field_warning(self, msg):
        # use a yes/no field with defauld "no"
        gui = self.gui
        mbox = QtGui.QMessageBox(gui)
        mbox.setStandardButtons(mbox.Yes | mbox.No);
        mbox.setDefaultButton(mbox.No)
        mbox.setText(msg)
        mbox.setInformativeText("Möchten Sie die Warnung ignorieren?")
        mbox.setIconPixmap(gui.windowIcon().pixmap(64, 64))
        ret = mbox.exec_()
        selected_button = QtGui.QMessageBox.StandardButton(ret)
        return selected_button == mbox.Yes

    def _handle_field_error(self, msg):
        gui = self.gui
        mbox = QtGui.QMessageBox(gui)
        mbox.setStandardButtons(mbox.Ok);
        mbox.setDefaultButton(mbox.Ok)
        mbox.setText(msg)
        mbox.setInformativeText("Dieser Fehler muss bearbeitet werden.")
        mbox.setIconPixmap(gui.windowIcon().pixmap(64, 64))
        ret = mbox.exec_()
        selected_button = QtGui.QMessageBox.StandardButton(ret)
        return selected_button == mbox.Ok

    def switch_focus_to_next_field(self, widget):
        current_field = widget.field
        field_state = self._form_state.field(current_field.name)
        val_state = field_state.validation_state
        current_field_has_warning = (val_state == ValidationResult.WARNING)
        current_field_has_error = (val_state == ValidationResult.ERROR)
        first_error = field_state.first_error()
        # LATER: we query the QT widget here to retrieve configuration data
        # ("read only"). I think we should store the more abstract widget
        # configuration (which was provided by a plugin) somewhere and use that.
        # I think the "model"/"state" should be separated from the actual GUI
        # items as much as possible (might avoid QT problems, less mixing of
        # GUI code and more complicated logic).
        field_is_read_only = widget.isReadOnly()
        # read only fields should never trigger warnings
        if field_is_read_only or (not self.use_validation_for_navigation()):
            pass
        elif current_field_has_error:
            self.log.debug('about to switch to next field but current field %s contains error', current_field.name)
            msg = '%s: %s' % (current_field.name, first_error.message)
            self._handle_field_error(msg)
            return
        elif current_field_has_warning:
            self.log.debug('about to switch to next field but current field %s contains warning', current_field.name)
            msg = '%s: %s' % (current_field.name, first_error.message)
            ignore_warning = self._handle_field_warning(msg)
            if not ignore_warning:
                return
            batch = self._batch_state.batch
            form_index = self._batch_state.current_form.index
            error_key = first_error.key
            field_value = batch.form(form_index)[current_field.name].value
            self.log.debug('%s (#%d): ignore warning "%s" (%r)', current_field.name, form_index, error_key, field_value)
            db_form = batch.db_form(form_index)
            db_form.add_ignored_warning(current_field.name, error_key, field_value)

        next_field = self._form_state.next_field(current_field)
        if next_field is None:
            next_form_index = self._batch_state.next_form_index(
                current_index=self._batch_state.current_form.index,
                validation_mode=self.use_validation_for_navigation()
            )
            if next_form_index is not None:
                self.log.debug('switching to form #%d (from %s / form #%d)', next_form_index, current_field.name, self._batch_state.current_form.index)
                self._page_action(next_form_index)
            else:
                # LATER: should prompt for batch change...
                # Might have to check for "skipped" errors
                self.handle_end_of_batch()
            return
        self.log.debug('switch focus from %s to %s (form #%d)', current_field.name, next_field.name, self._batch_state.current_form.index)
        self._set_focus_for_field(next_field)

    def switch_focus_to_previously_edited_field(self, widget):
        """Switch focus to the previously edited field (as recorded in
        BatchState's editing stack)"""
        prev_item = self._batch_state.previous_field_from_edit_stack()
        if prev_item is None:
            return
        prev_form_index, prev_field_name = prev_item
        name_of_focussed_field = widget.field.name
        if prev_form_index != self._batch_state.current_form.index:
            self._page_action(prev_form_index)
        elif name_of_focussed_field == prev_field_name:
            # common case: a user modified a field and wants to switch to the
            # field edited before. However the editing stack has the current
            # first so the user would have to press "cursor up" twice to focus
            # the previously edited field.
            # However if he did not change anything in the new field a single
            # "cursor up" would be enough. To provide a consistent behavior we
            # just request the "previous edit" if the edit stack returns the
            # currently focussed field.
            self.switch_focus_to_previously_edited_field(widget)
            return
        self.log.debug('previous edited field: %r (form #%d)', prev_field_name, prev_form_index)
        prev_field = self._find_field_by_name(prev_field_name)
        self._set_focus_for_field(prev_field)

    def switch_focus_to_next_edited_field(self, widget):
        """Switch focus to the field editing after the current one.
        This only has an effect if the user used the editing stack before to
        focus a previously edited field."""
        next_item = self._batch_state.next_field_from_edit_stack()
        if next_item is None:
            return
        next_form_index, next_field_name = next_item
        self.log.debug('next edited field: %r (form #%d)', next_field_name, next_form_index)
        if next_form_index != self._batch_state.current_form.index:
            self._page_action(next_form_index)
        next_field = self._find_field_by_name(next_field_name)
        self._set_focus_for_field(next_field)

    def _focus_first_field_in_form(self):
        self.log.debug('set focus for first field in form %d', self._batch_state.current_form.index)
        next_field = self._form_state.next_field(current_field=None)
        if next_field is None:
            # we don't want to switch forms so just use the first field
            next_field = self.textfield_list[0]
        self._set_focus_for_field(next_field)

    def _set_focus_for_field(self, field):
        if field is None:
            self.log.error('tried to set focus to field=None in form %r', self._batch_state.current_form.index)
            return
        # comments from ctismer:
        #   we want to capture a possible mouse event first, before triggering
        #   a focusIn. But the focusIn comes before the mouse event is triggered.
        #   Therefore, we start an idle timer that inverts the logic flow.
        # see other comments in self._setup_focus_timer()
        self._timer.timeout.disconnect()
        self._timer.timeout.connect(partial(self._set_focus_for_field_delayed, field))
        self._timer.start()

    def _set_focus_for_field_delayed(self, field):
        for field_ in self.field_list:
            # This avoid multiple "selected" fields when using SETFOCUS in the
            # remote control.
            # When multiple SETFOCUS commands were issues by the remote control
            # the previously selected fields were still "selected" (text with
            # blue background) even though the field did not have the focus
            # anymore from QT's point of view. Sending an explicit FocusOut
            # event avoids that.
            # Somehow setting the focus on a widget does not lead to the old
            # field being unselected as far as QT is concerned.
            # The remote control runs in the background so no field had the
            # "focus" even though text was selected. Hence we just send FocusOut
            # to all fields instead of testing ".hasFocus()".
            field_.widget.focusOutEvent(QtGui.QFocusEvent(QtCore.QEvent.FocusOut))
        field.widget.setFocus()
        field.widget.selectAll()
        # The following line can be used to avoid the timer/"_set_focus_for_field_delayed"
        # completely. The all previously observed problems (see comment at
        # timer setup) were gone. The only glitch was a missing rectangle around
        # the image area containing the field text.
        # As the timer works so far I'm not spending time right now I think the
        # "FocusIn" event is an important clue and I spent too much time already
        # on that topic so I'm leaving the line below as a starting point.
        #field.widget.focusInEvent(QtGui.QFocusEvent(QtCore.QEvent.FocusIn))
        self._update_field_display(field)

    def handle_end_of_batch(self):
        self.log.info('end of batch reached')
        # also close tasks for current form...
        current_form_index = self._batch_state.current_form.index
        validation_result = self._form_state.validation_result
        self.val_module.store_validation_state_as_tasks(current_form_index, validation_result)
        batch = self._batch_state.batch
        batch.commit()

        contains_errors = len(batch.new_tasks(type_=TaskType.VERIFICATION)) > 0
        title = 'Ende des Stapels erreicht'
        if not contains_errors:
            text = 'Sie haben alle Fehler behoben und können jetzt eine ASC-Datei erstellen.'
        else:
            text = 'Es gibt allerdings noch unbearbeitete Fehler vor dem aktuellen Formular..'
        self.gui.msgbox_info(title, text)

    def _update_field_display(self, field, event=None):
        self.gui.mirror_edit.mirror(field.widget)
        self.gui.text_scrollarea.ensureWidgetVisible(field.widget)
        self.gui.image_label.update()
        self.gui.image_label.painter.ensure_focused_field_visible(
            self.gui.image_scrollarea)
        self.gui.zoom_label.update()
        self.gui.zoom_label.painter.ensure_focused_field_visible(
            self.gui.zoom_scrollarea)
        # set the list view status
        self.set_status_field(field)

    def on_font_scaled(self):
        '''
        event handler (slot) for changed font size of the grid.
        We simply use the same font in the status view for now.
        '''
        # get the list widget
        lw = self.gui.messagelist_widget
        # get the font of a text field.
        # they are currently all the same
        for field in self.field_list:
            if field.widget and field.is_lineedit:
                widget = field.widget
                break
        else:
            # no text field, yet
            return
        for i in range(lw.count()):
            fld = lw.item(i)
            if isinstance(fld, QtGui.QListWidgetItem):
                fld.setFont(widget.font())
        # also adjust the font of the mirror line edit
        for field in self.field_list:
            if field.widget.hasFocus():
                self.gui.mirror_edit.mirror(widget)

    def on_lineedit_touched(self, widget, event):
        if not widget.hasFocus():
            return
        self.gui.mirror_edit.mirror(widget)

    def prev_page(self):
        if self.pos > 0:
            self._page_action(self.pos - 1)

    def next_page(self):
        if self.pos < self.count - 1:
            self._page_action(self.pos + 1)

    def update_displayed_form_number(self, form_index):
        form_position = form_index + 1
        self.gui.recipe_no_edit.setText(str(form_position))

    def handle_entered_form_number(self):
        user_input = self.gui.recipe_no_edit.text()
        try:
            form_index = int(user_input) - 1
        except ValueError:
            form_index = self.pos
        if 0 <= form_index <= (self.count - 1):
            self._page_action(form_index)

    def _update_field_values_from_validation_result(self):
        if not self.validation_result:
            return
        self.log.debug('updating field values with results from validation')
        form = self.seq.current_form
        for field in self.textfield_list:
            fld = form.fields[field.name]
            field_result = self.validation_result.children[field.name]
            if has_correction(field_result):
                self.log.debug('validation provided new value %r for field %s', field_result.value, field.name)
                # copy updated values into the data structure
                fld.value = field_result.value
                # update the text field with the changed value
                field.widget.setText(fld.value)
                # note: the true write happens later in form.write_back()

    def _update_error_states_for_fields(self):
        self.log.debug('updating colors for text fields')
        for field in self.textfield_list:
            self.set_field_style(field)

    def close_current_batch(self, store_modified_data):
        self.log.debug('closing batch')
        batch = self._batch_state.batch

        if (self.val_module is not None) and (self._form_state is not None):
            current_form_index = self._batch_state.current_form.index
            self.val_module.store_validation_state_as_tasks(current_form_index, self._form_state.validation_result)

        if self._is_form_dirty() and store_modified_data:
            key = 'last_form_index'
            form_index = self._batch_state.current_form.index
            self.log.debug('storing last form (#%d) before close', form_index)
            batch.store_setting(key, form_index)
            self._write_data_back()
            batch.commit()

        batch.close()
        self._batch_state = None

    def closeEvent(self, event):
        # this event is artificially redirected, see __init__
        if not self._is_form_dirty():
            event.accept()
            return

        mb = QtGui.QMessageBox()
        flags = QtGui.QMessageBox.Abort
        flags |= QtGui.QMessageBox.Discard
        flags |= QtGui.QMessageBox.Save

        mb.setText("The form has been modified.")
        mb.setInformativeText("Do you want to save your changes?")
        mb.setStandardButtons(flags)
        mb.setDefaultButton(QtGui.QMessageBox.Save)

        reply = mb.exec_();
        if reply == QtGui.QMessageBox.Save:
            self.close_current_batch(store_modified_data=True)
            event.accept()
        elif reply == QtGui.QMessageBox.Discard:
            self.close_current_batch(store_modified_data=False)
            event.accept()
        else:
            event.ignore()
