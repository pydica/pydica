# -*- coding: utf-8 -*-
# This module implements SRW-specific behavior. Ideally the functionality
# should be present only in plugin functionality.
from __future__ import division, absolute_import, print_function, unicode_literals

import os

from PySide import QtGui
from srw.rdblib import guess_path, DataBunch
from srw.walther.ascii_export import export_ascii_to_path

from ddc.lib.log_proxy import l_
from ddc.lib.result import Result
from ddc.storage import FIELDS_INI


__all__ = ['start_ascii_export']

def ascii_export(batch):
    # ensure that all data is persisted before exporting ASC file.
    batch.commit()
    bunch = batch.bunch
    cdb_path = bunch.cdb
    ascii_path = guess_path(cdb_path, 'asc')
    if os.path.exists(ascii_path):
        return Result(False, reason='Es existiert bereits eine ASC-Datei.', key='asc_exists')
    # 'x' mode so an existing ASC file is never overwritten (requires Python 3.3+)
    with open(ascii_path, 'xb') as ascii_fp:
        export_ascii_to_path(batch.cdb, ascii_fp, FIELDS_INI)
    asc_bunch = DataBunch(cdb=bunch.cdb, ibf=bunch.ibf, db=bunch.db, ask=ascii_path)
    batch.bunch = asc_bunch
    return Result(True)

def finalize_batch(batch, *, cdb_collection, log=None):
    # LATER: just checking for any new tasks - maybe we could be more specific?
    if len(batch.new_tasks()) > 0:
        return Result(False, reason='Es sind noch nicht bearbeitete Fehler vorhanden.', key='data_contains_errors')
    # LATER: check if we have verification tasks?
    export_result = ascii_export(batch)
    if export_result != True:
        return export_result
    bunch = batch.bunch
    xdb_path = bunch.cdb
    xdb_filename = os.path.basename(xdb_path)
    if not xdb_filename.upper().endswith('.RDB'):
        return Result(True)
    batch.rename_xdb(to='CDB', log=log)

    # CDB_Collection also has a "cache" for forms which are backed by mmap'ed
    # files. We need to ensure that these forms are reloaded otherwise Python
    # will throw "ValueError: mmap closed or invalid" once the CDB_Collection
    # tries to save anything.
    # The best solution (probably not short term) is to remove CDB_Collection
    # entirely and probably rethink the form abstraction
    # (cdb_collection.CDB_Form, cdb_tool.Form).
    #
    # reproducer:
    #    - export ASC (including RDB->CDB rename)
    #    - change a field
    #    - trigger a write (e.g. closing the app or pressing enter)
    #    => exception
    cdb_collection._load_forms()
    return Result(True)

def start_ascii_export(gui, batch, *, cdb_collection, log=None):
    result = finalize_batch(batch, cdb_collection=cdb_collection, log=log)
    if result == True:
        title = 'ASC-Export erfolgreich.'
        text = 'Herzlichen Glückwusch: Dieser Stapel ist geschafft!'
        gui.msgbox_info(title, text)
        log.info(title)
        return
    title = 'ASC-Export nicht möglich'
    text = result.reason
    gui.msgbox_warning(title, text)
    log.warning('%s: %s', title, text)
    return
