# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import socket

from PySide import QtGui

from ddc.lib.log_proxy import l_


__all__ = ['copy_text_to_clipboard']

def copy_text_to_clipboard(text, clipboard_provider=None, log=None):
    log = l_(log)
    provider = clipboard_provider or 'udp'
    log.debug('using "%s" clipboard implementation', provider)
    if provider == 'qt':
        app = QtGui.QApplication.instance()
        app.clipboard().setText(text)
    elif provider == 'win32':
        import win32clipboard
        import win32con
        win32clipboard.OpenClipboard()
        win32clipboard.SetClipboardData(win32con.CF_UNICODETEXT, text)
        win32clipboard.CloseClipboard()
    elif provider == 'pyperclip':
        import pyperclip
        pyperclip.copy(text)
    elif provider == 'udp':
        UDP_PORT = 5005
        msg = text.encode('cp1252')
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto(msg, ('127.0.0.1', UDP_PORT))
    else:
        log.error('unknown clipboard provider "%s"', provider)
        raise ValueError('unknown clipboard provider "%s"' % (clipboard_provider,))

