# -*- coding: utf-8 -*-
'''
This module comtains a few CSS definitions to be used
in the Gui. It provides a basic mapping from widget
state to visual appearence.
'''
from __future__ import division, absolute_import, print_function, unicode_literals


lineedit_readonly = '''
QLineEdit {
     border: 1px solid lightgrey ;
}
'''

lineedit_normal = '''
QLineEdit {
     border: none;
}
QLineEdit:focus {
     border: 1px solid black ;
}
'''

lineedit_valid = '''
QLineEdit {
     border: 1px solid lightgreen ;
}
QLineEdit:focus {
     border: 2px solid green ;
}
'''

lineedit_warning = '''
QLineEdit {
     border: 2px solid orange ;
}
QLineEdit:focus {
     border: 2px solid yellow ;
}
'''

lineedit_error = '''
QLineEdit {
     border: 2px solid darkred ;
}
QLineEdit:focus {
     border: 2px solid red ;
}
'''

# this is for ediview, only
lineedit_highlight = '''
QLineEdit {
     border: 1px solid red ;
}
QLineEdit:focus {
     border: 2px solid red ;
}
'''

class PaintProps(object):
    def __init__(self, name, stylesheet):
        self.name = name
        self.stylesheet = stylesheet

class LineEdit_Style(object):
    readonly = PaintProps('readonly', lineedit_readonly)
    normal = PaintProps('normal', lineedit_normal)
    valid = PaintProps('valid', lineedit_valid)
    warning = PaintProps('warning', lineedit_warning)
    error = PaintProps('error', lineedit_error)
    highlight = PaintProps('highlight', lineedit_highlight)

# XXX add a body that shows the different styles in a Gui
