#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os

from srw.rdblib import DataBunch
from srw.rdblib.cdb import open_cdb

from ddc.client.config import ALL_FIELD_NAMES
from ddc.lib.log_proxy import l_
from ddc.tool.datamanager import bunch_from_cdb_name
from ddc.tool.datamanager.introducer import create_and_validate_batch_from_bunch


__all__ = ['open_batch_with_ui_progress']

def open_batch_with_ui_progress(path, gui, pydica_context, log=None):
    """
    Open a batch given a RDB/CDB path. The code performs sanity checks on
    the CDB contents before parsing and validates all forms while providing a
    graphical progress dialog (including the ability to cancel).

    Returns a batch instance or None if the user cancelled the operation.
    """
    log = l_(log)
    qt_app = gui.qt_app()
    window = gui.progress_window()
    progress = window.progress_reporter(qt_app)

    def worker():
        is_active = progress.update(0, details=u'IBF-Datei und Datenbank suchen')
        if is_active == False:
            return
        bunch = bunch_from_cdb_name(path, log=log)

        is_active = progress.update(0, details=u'Strukturvalidierung')
        if is_active == False:
            return
        result = open_cdb(bunch.cdb, field_names=ALL_FIELD_NAMES)
        if result == False:
            log.info(result.message)
            progress.abort()
            filename = os.path.basename(path)
            title = '%s konnte nicht geöffnet werden' % filename
            gui.msgbox_warning(title, result.message)
            return

        bunch = DataBunch.merge(bunch, cdb=result.cdb_fp)
        batch = create_and_validate_batch_from_bunch(
            bunch,
            pydica_context,
            log=log,
            progress=progress,
        )
        if not progress.is_cancelled:
            progress.finish(result=batch)
        else:
            batch.close()
            progress.result = None

    window.show(worker)
    if not progress.is_cancelled:
        window.exec_()
    processing_aborted = window.is_cancelled
    if processing_aborted:
        return None
    batch = progress.result
    return batch
