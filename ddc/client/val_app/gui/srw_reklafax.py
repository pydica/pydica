# -*- coding: utf-8 -*-
# This module implements SRW-specific behavior. Ideally the functionality
# should be present only in plugin functionality.
from __future__ import division, absolute_import, print_function, unicode_literals

from PySide import QtGui

from .clipboard import copy_text_to_clipboard


__all__ = ['display_reklafax_dialog']

def reklafax(seq, fname, nr, codnr, message, *, log, context):
    log.info('creating Reklafax for PIC %s, form #%d: %s', codnr, nr, message)
    beleg_nr = nr + 1
    bunch = seq.batch.bunch
    im_fname = bunch.ibf
    txt = 'REKLAFAX REQUEST: CODNR: {} BELEGNR: {:03d} FILE: {} MESSAGE: {}'
    txt = txt.format(codnr, beleg_nr, im_fname, message)
    copy_text_to_clipboard(txt,
        clipboard_provider=context['settings'].get('clipboard'),
        log=log
    )

def display_reklafax_dialog(gui, seq, form_index, *, log, context, intern=False):
    parent = gui
    title = 'Texteingabe für Reklafax'
    label = 'Was soll gedruckt werden?'
    default = '' if (not intern) else 'interner Fehler !!! INTERN !!!'
    text, ok = QtGui.QInputDialog.getText(parent, title, label, text=default)
    if not ok:
        return
    message = text
    codnr = seq.current_form.pic_nr
    fname = seq.cdb_filename
    reklafax(seq, fname, form_index, codnr, message, log=log, context=context)
