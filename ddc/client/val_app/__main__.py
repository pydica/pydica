#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os
import sys

# quick fix that makes it work. But I don't like this very much...
_path = os.path.realpath(os.path.join(os.path.dirname(__file__), '../../..'))
if _path not in sys.path:
    sys.path.insert(0, _path)

import ddc.config.pyside_fix
from PySide import QtGui

from ddc.config import init_pydica, parse_stringbool
from ddc.client.val_app import CollectionViewer
from ddc.client.val_app.gui.fieldpainting import paint_settings
from ddc.lib.exception_reporter import install_except_hook
from ddc.tool.cdb_collection import default_cdb_filename, CDB_Collection
from ddc.tool.datamanager import bunch_from_cdb_name
from ddc.storage import Batch


class ValidationViewer(CollectionViewer):
    def __init__(self, context, *, gui=None):
        super(ValidationViewer, self).__init__(context, gui=gui)
        self.gui.set_display_validation(True)
        self.gui.set_validation_interaction(True)
        self.resetActions()
        paint_settings.set_painting(True)

    def open_sequence(self, seq, form_index=None):
        '''
        This function is supposed to open a cdb/ibf sequence in connection
        with a SQLite db of the same name.
        '''
        super(ValidationViewer, self).open_sequence(seq, form_index=form_index)
        if (form_index is None) and seq.batch:
            key = 'last_form_index'
            last_index = self._batch_state.batch.get_setting(key)
            if last_index is not None:
                self.goto_form(int(last_index))
        self.resetActions()

    def create_actions(self):
        super(ValidationViewer, self).create_actions()
        pass
        # The batch selection dialog triggers several PySide bugs - including a
        # crash bug. The dialog is not useful in the current processing flow so
        # let's disable it for now. However the code is mostly ok so let's keep it
        # for later.
        # LATER: enable the dialog once some users only work on specific errors.
        #self.select_batchAct = QtGui.QAction("Select Batch", self.gui,
        #                                     triggered=self.select_batch_dlg)


    def create_menus(self):
        super(ValidationViewer, self).create_menus()
        validation_menu = self.gui.menus.validation
        # see explanation in "create_actions" why this is disabled
        #validation_menu.addAction(self.select_batchAct)


    def resetActions(self):
        pass


def main(argv=sys.argv):
    usage = '''
usage: %s <config> [CDB]
Config may be a config file ending in '.ini' or '.cfg', or a
symbolic link ending in '.lnk'.
Without parameter, a file 'config.lnk' is looked up in the project root.''' % argv[0]
    pydica_context = init_pydica(argv, usage)

    app = QtGui.QApplication(argv)
    myapp = ValidationViewer(pydica_context)
    pydica_settings = pydica_context['config'].get('pydica', {})
    abort_on_error = parse_stringbool(pydica_settings.get('abort_on_error'))
    install_except_hook(myapp, store_reports=True, abort_on_error=abort_on_error)
    myapp.show()
    # for now, disable maximized in the wing debugger
    if "WINGDB_ACTIVE" not in os.environ:
        myapp.gui.showMaximized()
    dflt = None
    if len(argv) > 1:
        # If config is passed explicitely, the CDB file might be the second
        # command line parameter. I assume the "default cdb" is just a dev
        # feature so let's just use the last cli parameter as a workaround for
        # now.
        cdb_filename = argv[-1]
        if os.path.exists(cdb_filename) and cdb_filename[-4:].lower() in ('.cdb', '.rdb'):
            dflt = os.path.abspath(cdb_filename)
    if dflt is None:
        dflt = default_cdb_filename()

    cdb_basename = os.path.basename(dflt)
    cdb_log = pydica_context.get_logger('pydica.storage.cdb', context=cdb_basename)
    bunch = bunch_from_cdb_name(dflt, log=cdb_log)
    batch = Batch.init_from_bunch(bunch, create_persistent_db=(bunch.db is None), log=cdb_log)
    seq = CDB_Collection(batch, log=cdb_log)
    myapp.open_sequence(seq)

    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
