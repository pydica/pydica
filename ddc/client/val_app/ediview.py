#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

"""
This is the original client_val application.
It is used for testing and reference if needed.
"""


import os
import socket
import signal
import sys

# quick fix that makes it work. But I don't like this very much...
_path = os.path.realpath(os.path.join(os.path.dirname(__file__), '../../..'))
if _path not in sys.path:
    sys.path.insert(0, _path)

import ddc.config.pyside_fix
from PySide import QtGui

# XXX this needs to be split up and moved to the app.__main__ file
# just here to see if it works
from ddc.client.val_app import CollectionViewer
from ddc.client.val_app.gui.srw_ascii import ascii_export
from ddc.client.val_app.gui.srw_reklafax import reklafax
from ddc.config import parse_stringbool, init_pydica
from ddc.lib.exception_reporter import install_except_hook
from ddc.tool.cdb_collection import CDB_Collection, default_cdb_filename
from ddc.tool.remote_control import CDB_Reader, RemoteControl


def exit_on_ctrl_c(*args):
    QtGui.QApplication.quit()
    sys.stdout.write('\n')

def ipv4_address_for_host():
    ipaddrlist = []
    for ip in socket.gethostbyname_ex(socket.gethostname())[2]:
        if not ip.startswith('127.'):
            ipaddrlist.append(ip)
    if len(ipaddrlist) == 0:
        return None
    # very crude heuristic to choose "the" IP address.
    return ipaddrlist[0]

def initialize_ediview(pydica_context, *, controller=None):
    headless = (controller is not None)
    CONTROL_PATH = os.path.abspath(pydica_context.settings.get('control_path', '.'))
    udp_port_str = pydica_context.settings.get('udp_port', None)
    udp_port = int(udp_port_str) if udp_port_str else None

    cdb_filename = default_cdb_filename()
    cdb_basename = os.path.basename(cdb_filename)
    cdb_log = pydica_context.get_logger('pydica.storage.cdb', context=cdb_basename)

    seq = CDB_Collection.from_cdb_filename(cdb_filename, access='dontcare', log=cdb_log)

    app = None
    myapp = controller
    if not headless:
        app = QtGui.QApplication([])
        myapp = CollectionViewer(pydica_context)
    myapp.gui.stackedWidget.setCurrentIndex(0) # override whatever the GUI was saved with
    if not headless:
        myapp.show()
    myapp.open_sequence(seq)

    reader = CDB_Reader(pydica_context)
    reader.signal_goto.connect(myapp.goto_form)
    reader.signal_open.connect(myapp.open_sequence)
    reader.signal_setfocus.connect(myapp.setfocus_field)
    reader.signal_highlight.connect(myapp.field_highlight)
    reader.signal_delete.connect(myapp.delete_form)
    reader.signal_delete_current.connect(myapp.delete_current_form)
    reader.signal_undelete_current.connect(myapp.undelete_current_form)
    reader.signal_reklafax.connect(reklafax)
    reader.signal_ascii.connect(ascii_export)

    rem = RemoteControl(pydica_context, debug=True, watch_path=CONTROL_PATH, udp_port=udp_port)
    rem.got_opendel_cmd.connect(reader.opendel_cmd)
    rem.got_openasc_cmd.connect(reader.openasc_cmd)
    rem.got_setfocus_cmd.connect(reader.setfocus_cmd)
    rem.got_highlight_cmd.connect(reader.highlight_cmd)
    rem.got_delete_cmd.connect(reader.delete_cmd)
    rem.got_delete_current_cmd.connect(reader.delete_current_cmd)
    rem.got_undelete_current_cmd.connect(reader.undelete_current_cmd)
    rem.got_reklafax_cmd.connect(reader.reklafax_cmd)
    rem.got_ascii_cmd.connect(reader.ascii_cmd)

    if udp_port:
        ip_address = ipv4_address_for_host()
        if ip_address is None:
            log.warning('unable to detect local IP address.')
        else:
            ip_port_path = os.path.join(CONTROL_PATH, 'listen.udp.txt')
            with open(ip_port_path, 'w') as fp:
                fp.write('%s:%s\n' % (ip_address, udp_port))
    # we must return "reader" to keep the instance alive. Otherwise Python's
    # reference counting will remove the instance. This causes a bug which is
    # easy too overlook as the remote control will continue processing commands
    # but the UI won't be updated.
    # It seems there are some interesting interactions in the QT Python
    # bindings. Previously I assumed that connecting a method (e.g. "reader.opendel_cmd")
    # to the RemoteControl instance would prevent the reading from being garbage
    # collected. However that is obviously not the case.
    return (app, myapp, rem, reader)


def main():
    argv = sys.argv
    usage = '''
usage: %s <config>
Config may be a config file ending in '.ini' or '.cfg', or a
symbolic link ending in '.lnk'.
Without parameter, a file 'config.lnk' is looked up in the project root.''' % argv[0]
    pydica_context = init_pydica(argv, usage)
    signal.signal(signal.SIGINT, exit_on_ctrl_c)

    # we need to keep a reference to "reader" - see comment at end of function
    app, myapp, rem, reader = initialize_ediview(pydica_context)

    pydica_settings = pydica_context['config'].get('pydica', {})
    abort_on_error = parse_stringbool(pydica_settings.get('abort_on_error'))
    install_except_hook(myapp, store_reports=True, abort_on_error=abort_on_error)

    # for now, disable maximized in the wing debugger
    if "WINGDB_ACTIVE" not in os.environ:
        myapp.gui.showMaximized()

    rem.start()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
