# -*- coding: utf-8 -*-

from .batchvalidators import DuplicatePIC


__all__ = ['process_batch']

def process_batch(batch, context=None):
    errors = []
    if context is None:
        context = {}
    validators = (
        DuplicatePIC(),
    )
    for validator in validators:
        validator_errors = validator.process_batch(batch, context)
        errors.extend(validator_errors)
    return tuple(errors)
