# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from pythonic_testcase import *

from ddc.storage.testhelpers import batch_with_pic_forms
from ddc.validation.batchvalidators import DuplicatePIC


class DuplicatePICTest(PythonicTestCase):
    def test_accepts_batch_without_duplicated_pics(self):
        pic1 = '12345600100024'
        pic2 = '12345600114024'
        batch = batch_with_pic_forms([pic1, pic2])

        errors = DuplicatePIC().process_batch(batch, context={})
        assert_length(0, errors)

    def test_accepts_batch_if_all_duplicate_pics_are_deleted(self):
        pic1 = '12345600100024'
        pic2 = ('DELETED', pic1)
        pic3 = ('DELETED', pic1)
        batch = batch_with_pic_forms([pic1, pic2, pic3])

        errors = DuplicatePIC().process_batch(batch, context={})
        assert_length(0, errors)

    def test_rejects_batch_with_duplicate_nondeleted_pic(self):
        pic1 = '12345600100024'
        pic2 = '12345600114024'
        batch = batch_with_pic_forms([pic1, pic2, pic1, pic1])

        errors = DuplicatePIC().process_batch(batch, context={})
        assert_length(1, errors)
        error = errors[0]
        assert_equals('duplicate_pic', error.key)
        assert_equals((0, 2, 3), error.forms)

    def test_rejects_batch_if_duplicate_pic_form_is_marked_as_being_undeleted(self):
        # This tests a workaround due to our insufficient transaction semantics
        # in Batch (see explanation in CollectionViewer.undelete_current_form())
        pic1 = '12345600100024'
        pic2 = ('DELETED', pic1)
        batch = batch_with_pic_forms([pic1, pic2])

        assert_length(0, DuplicatePIC().process_batch(batch, context={}))
        assert_length(1, DuplicatePIC().process_batch(batch, context={'srw.assume_undeleted': 1}))

