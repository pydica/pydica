# -*- coding: utf-8 -*-

from ddc.lib.attribute_dict import AttrDict


__all__ = ['DuplicatePIC']

class DuplicatePIC(object):
    def process_batch(self, batch, context):
        errors = []
        seen_pics = {}
        for i, form in enumerate(batch.forms()):
            if form.is_deleted() and (context.get('srw.assume_undeleted') != i):
                continue
            pic = batch.pic_for_form(i)
            form_indexes = seen_pics.setdefault(pic, [])
            form_indexes.append(i)

        for pic, indexes in seen_pics.items():
            if len(indexes) > 1:
                positions = map(lambda i: str(i+1), indexes)
                e = AttrDict(
                    key='duplicate_pic',
                    message='PIC %s kommt mehrfach vor (Belege %s)' % (pic, ', '.join(positions)),
                    forms=tuple(indexes)
                )
                errors.append(e)
        return tuple(errors)
