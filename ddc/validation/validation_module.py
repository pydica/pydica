# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os
from pprint import pformat
import sys

from pythonic_testcase import assert_equals

from ddc.config import init_pydica
from ddc.lib.attribute_dict import AttrDict
from ddc.lib.dict_merger import merge_dicts
from ddc.lib.progress_utils import SuppressProgress
from ddc.lib.request_logger import store_exception_report
from ddc.tool.datamanager import bunch_from_cdb_name
from ddc.storage import Batch, TaskStatus, TaskType


__all__ = ['validation_module_for_batch', 'ValidationModule']

def validation_module_for_batch(batch, bunch, context):
    plugin_mgr = context['plugin_manager']
    batch_meta = plugin_mgr.query_plugins('batch_metadata', bunch=bunch, batch=batch)
    batch.meta.update(batch_meta)
    batch_context = AttrDict(merge_dicts(context, {'batch_meta': batch_meta}))
    return ValidationModule(batch, batch_context)

keys = lambda errors: [error['key'] for error in errors]

# TODO: log all actions
class ValidationModule(object):
    """
    This class should handle all validation-related activity including
    heuristic fixes, customer-specific data/error handling), tasks and logging.
    It knows batches and tasks is generally the "go-to" place when you want to
    perform (automatic) validation.

    Please note that all operations which work on the "plain" form data are
    implemented in the FormValidation class while this class should be about
    integrating the actual validation into the pydica system.

    Please note that the ValidationModule is currently work in progress, some
    features (such as tasks and logging) are missing/not implemented completely.
    Also the API may change later on depending on the actual GUI needs.
    """
    def __init__(self, batch, context):
        self.batch = batch
        self.context = context
        self.log = context.get_logger('pydica.validation_module')
        self.plugin = self.load_validation_plugin()

    def load_validation_plugin(self):
        activated_plugins = self.context['plugin_manager'].activated_plugins
        plugins = tuple(activated_plugins.values())
        if len(plugins) == 0:
            raise ValueError('No plugin contributed a validation schema')
        assert len(plugins) == 1
        return plugins[0]

    def process_tasks(self, progress_factory=None):
        tasks = self.batch.new_tasks(TaskType.FORM_VALIDATION)
        if not tasks:
            return
        if progress_factory is None:
            progress_factory = SuppressProgress.as_factory()
        progress = progress_factory(max_value=len(tasks))
        for i, task in enumerate(tasks):
            self.process_task(task)
            is_active = progress.update(i)
            if is_active == False:
                return
        progress.finish()

    def process_task(self, task):
        """Validate all fields of a form (specified by the given form
        validation task).
        Afterwards the initial task will be closed. The method creates
        new tasks in case of validation errors."""
        # LATER: Somehow we need to deal with the edge case that there are
        # other validation errors regarding the same prescription.
        # (need to handle duplication)
        assert_equals(TaskType.FORM_VALIDATION, task.type_)
        assert_equals(TaskStatus.NEW, task.status)
        form_index = task.form_index
        form = self.batch.form(form_index)
        form_result = self.process_form(form, form_index)

        for field_name, field_errors in form_result.errors.items():
            db_form = self.batch.db_form(form_index)
            if not field_errors or self._new_verification_task_exists_already(db_form, field_name, field_errors):
                continue
            self._create_verification_task_for_error(db_form, field_name, field_errors)
        task.close()

    def process_values(self, form_values, extra_context=None):
        """
        This method is really just an alias for doing validation plainly on the
        provided values without any reference to forms.
        Callers should have a single API for doing verification.
        """
        context = self.context
        if extra_context is not None:
            context = self.context.copy()
            context.update(extra_context)
        formtype = self.plugin.detect_formtype(form_values)
        validator = self.plugin.form_validator(formtype=formtype, context=context)
        form_result = validator.run(form_values, context=context)
        if self._contains_unexpected_exceptions(form_result):
            self._log_validation_exception(form_result, context)
        return form_result

    def process_form(self, form, form_position):
        """Validate all form fields and return the form result."""
        form_values = dict((name, form.fields[name].value) for name in form.field_names)
        plugin_mgr = self.context['plugin_manager']
        form_meta = plugin_mgr.query_plugins('form_metadata', form=form, form_position=form_position, context=self.context)
        extra_context = {
            'form_meta': form_meta,
        }
        result =  self.process_values(form_values, extra_context=extra_context)
        return self._strip_ignored_warnings(result, form_position)

    def _strip_ignored_warnings(self, result, form_index):
        db_form = self.batch.db_form(form_index)
        ignored_warnings = set(db_form.ignored_warnings(as_ignore_key=True))
        for field_name, errors in result.errors.items():
            if len(errors) == 0:
                continue
            field_data = getattr(result, field_name)
            value = result.initial_value[field_name]
            filtered_errors = []
            for error in errors:
                ignore_key = (field_name, error.key, value)
                if ignore_key not in ignored_warnings:
                    filtered_errors.append(error)
                else:
                    self.log.debug('#%d/%s: ignoring error %s (%r)', form_index, field_name, error.key, value)
            field_data.set(errors=filtered_errors)
        return result

    def store_validation_state_as_tasks(self, form_index, validation_result):
        """
        Compare the "new" VERIFICATION tasks for the given form with the current
        validation result and update the tasks accordingly (close tasks for errors
        not present in the current validation result and create new tasks for new
        validation errors).
        """
        db_form = self.batch.db_form(form_index)
        filtered_result = self._strip_ignored_warnings(validation_result, form_index)
        key_set = lambda errors: set(keys(errors))
        for task in db_form.new_tasks(TaskType.VERIFICATION):
            field_name = task.field_name
            field_result = filtered_result.get(field_name)
            field_errors = field_result.errors if field_result else ()
            task_errors = task.data.errors
            all_errors_already_known = (not key_set(task_errors).difference(key_set(field_errors)))
            if not all_errors_already_known:
                msg = 'verification task for form #%d / field "%s" (%s) no longer relevant - closing.'
                self.log.debug(msg, form_index, field_name, ', '.join(keys(task_errors)))
                task.close()

        for field_name, field_errors in validation_result.errors.items():
            if not field_errors:
                continue
            if self._new_verification_task_exists_already(db_form, field_name, field_errors):
                continue
            self._create_verification_task_for_error(db_form, field_name, field_errors)

    # --- internal helpers ----------------------------------------------------

    def _new_verification_task_exists_already(self, db_form, field_name, field_errors):
        new_tasks = db_form.tasks(
            field_name=field_name,
            type_=TaskType.VERIFICATION,
            status=TaskStatus.NEW,
        )
        return self._has_task_for_errors(new_tasks, field_errors)

    def _has_task_for_errors(self, tasks_for_field, field_errors):
        # If all error keys are already present in verification tasks there is
        # no need to create yet another task. However if there is a new key, we
        # should also create a new verification task because there might be
        # users who are allowed to fix the new error but not the old.
        # But let's not spend too much brain cells about this: This situation
        # is pretty much theoretical because the tasks should reflect errors in
        # CDB data and each validation run should cause the same errors.
        # However I thought it is a good idea to account for potential problems
        # so we have a really robust implementation which does not break when
        # something else is buggy.
        existing_keys = set()
        for task in tasks_for_field:
            error_keys = [error['key'] for error in task.data['errors']]
            existing_keys.update(error_keys)
        new_keys = set([error['key'] for error in field_errors])
        all_errors_already_known = new_keys.difference(existing_keys)
        return (not all_errors_already_known)

    def _create_verification_task_for_error(self, db_form, field_name, errors):
        task_meta = AttrDict(
            errors = tuple(errors),
        )
        task = db_form.add_task(
            type_=TaskType.VERIFICATION,
            status=TaskStatus.NEW,
            field_name=field_name,
            data = task_meta
        )
        msg = 'created new verification task for form #%d / field "%s" (%s)'
        self.log.debug(msg, (db_form.form_index), field_name, ', '.join(keys(errors)))
        return task

    # --- logging of unexpected exceptions (bugs in validators) ---------------
    def _contains_unexpected_exceptions(self, form_result):
        """return True if form_result contains an error with the
        'unexpected_exception' key"""
        contains_errors = form_result
        if contains_errors and (self._find_first_unexpected_exception(form_result) is not None):
            return True
        return False

    def _find_first_unexpected_exception(self, form_result):
        for field, errors in form_result.errors.items():
            for error in errors:
                if error.key == 'unexpected_exception':
                    return (field, error)
        return None

    def _log_validation_exception(self, form_result, context):
        "create an error report file if context contains an environment dir."
        env_path = context.get('env_dir')
        if not env_path:
            return
        field_name, validation_error = self._find_first_unexpected_exception(form_result)
        summary = 'Error when validating field %s\n\n' % field_name
        # LATER: get PIC nr/cdb batch file name
        kv_to_string = lambda item: '%s: %s' % (item[0], pformat(item[1]))
        context_string = '\n'.join(map(kv_to_string, sorted(context.items())))
        store_exception_report(env_path, summary,
            exc_info=validation_error.meta,
            headers=form_result.initial_value,
            context_string=context_string,
        )


def main(argv=sys.argv):
    usage = 'usage: %s CONFIGFILE CDBFILENAME' % argv[0]
    pydica_context = init_pydica(argv, usage)
    if len(sys.argv) < 3:
        print(usage)
        sys.exit(1)
    cdb_filename = argv[2]
    if not os.path.exists(cdb_filename):
        print(usage)
        print('cdb file not found: %s' % cdb_filename)
        sys.exit(2)
    cdb_basename = os.path.basename(cdb_filename)
    log = pydica_context.get_logger('pydica.validation', context=cdb_basename)

    bunch = bunch_from_cdb_name(cdb_filename, log=log)
    assert bunch.ibf is not None
    create_db = (bunch.db is None)
    batch = Batch.init_from_bunch(bunch, create_persistent_db=create_db, log=log)
    module = ValidationModule(batch, pydica_context)
    module.process_tasks()
    batch.commit()

if __name__ == '__main__':
    main()
