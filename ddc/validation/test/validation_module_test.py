# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from operator import attrgetter
import os

from pycerberus.api import Validator
from pycerberus.lib.form_data import FormData
from pythonic_testcase import *
from srw.formvalidation.generic.validators import DateValidator

from ddc.lib.attribute_dict import AttrDict
from ddc.lib.progress_utils import ProgressRecorder
from ddc.storage import db_schema, get_model, TaskStatus, TaskType
from ddc.storage.testhelpers import use_tempdir
from ddc.validation import ValidationModule
from ddc.validation.testutil import (batch_with_form_data, build_context,
    fake_plugin)



class ValidationModuleTest(PythonicTestCase):
    def test_can_validate_form_values(self):
        batch = batch_with_form_data([{'GEBURTSDATUM': 'anything'}])
        vm = ValidationModule(batch, build_context())
        assert_length(0, batch.tasks())

        result = vm.process_values({'GEBURTSDATUM': '28.10.61'})
        assert_false(result.contains_errors())
        assert_length(0, batch.tasks())

        result = vm.process_values({'GEBURTSDATUM': '98.76.54'})
        assert_length(0, batch.tasks())
        assert_true(result.contains_errors())
        errors = result.errors
        assert_equals(('GEBURTSDATUM', ), tuple(errors))
        date_errors = errors['GEBURTSDATUM']
        assert_length(1, date_errors)
        assert_equals('bad_date', date_errors[0].key)

    def test_validators_can_access_context(self):
        class FakeValidator(Validator):
            def validate(self, value, context):
                assert_contains('foobar', context)

        plugin = fake_plugin({'GEBURTSDATUM': FakeValidator()})
        context = build_context(plugin=plugin)
        context['foobar'] = 'some value'
        batch = batch_with_form_data([{'GEBURTSDATUM': 'anything'}])
        vm = ValidationModule(batch, context)
        result = vm.process_values({'GEBURTSDATUM': '28.10.61'})
        assert_false(result.contains_errors())

    def test_can_pass_extra_context_items_per_form(self):
        state = AttrDict(calls=0)
        class FakeValidator(Validator):
            def validate(self, value, context):
                state['calls'] += 1
                assert_contains('foobar', context)

        plugin = fake_plugin({'GEBURTSDATUM': FakeValidator()})
        batch = batch_with_form_data([{'GEBURTSDATUM': 'anything'}])
        vm = ValidationModule(batch, build_context(plugin=plugin))
        values = {'GEBURTSDATUM': '28.10.61'}
        result = vm.process_values(values, extra_context={'foobar': 'baz'})
        assert_equals(1, state.calls)
        assert_false(result.contains_errors())

        # check that extra_context is actually what made it pass above
        result = vm.process_values(values)
        assert_equals(2, state.calls)
        assert_true(result.contains_errors(), message='"foobar" should not be present in validation context')

    def test_can_create_error_reports_for_unexpected_exceptions(self):
        class FakeValidator(Validator):
            def validate(self, value, context):
                raise ValueError('You shall not pass!')

        plugin = fake_plugin({'GEBURTSDATUM': FakeValidator()})
        batch = batch_with_form_data([{'GEBURTSDATUM': 'anything'}])

        with use_tempdir() as env_dir:
            context = build_context(plugin=plugin, env_dir=env_dir)
            vm = ValidationModule(batch, context)
            result = vm.process_values({'GEBURTSDATUM': '28.10.61'})
            assert_true(result.contains_errors(), message='Fakevalidator did not raise its error message')

            exception_dir = os.path.join(env_dir, 'exceptions')
            assert_true(os.path.exists(exception_dir),
                message='expected exception dir %s not found' % exception_dir)
            self.assert_exception_report_exists(exception_dir)

    def test_can_process_validation_task(self):
        plugin = fake_plugin(validators={
            'ABGABEDATUM': DateValidator(),
            # adding a second validator to ensure the created validation task
            # references the right field.
            'BRUTTO': Validator(),
        })
        context = build_context(plugin=plugin)
        model = get_model(db_schema.LATEST)
        task = model.Task(0, TaskType.FORM_VALIDATION)
        forms_data = [{'ABGABEDATUM': 'invalid', 'BRUTTO': '100'}]
        batch = batch_with_form_data(forms_data, tasks=(task,), model=model)
        ValidationModule(batch, context).process_task(task)

        assert_equals(TaskStatus.CLOSED, task.status,
            message='form validation task should be marked as CLOSED')
        assert_length(2, batch.tasks(),
            message='automatic validation is expected to create one new verification task.')

        db_form = batch.db_form(form_index=0)
        new_tasks = batch.new_tasks(type_=TaskType.VERIFICATION)
        assert_length(1, new_tasks)
        verification_task = new_tasks[0]
        assert_equals(0, verification_task.form_index)
        assert_equals('ABGABEDATUM', verification_task.field_name)
        errors = verification_task.data['errors']
        assert_length(1, errors)
        assert_equals('bad_pattern', errors[0]['key'])

    def test_does_not_create_duplicate_verification_tasks_when_processing_validation_task(self):
        plugin = fake_plugin(validators={'ABGABEDATUM': DateValidator()})
        context = build_context(plugin=plugin)
        # using two forms to ensure that the code only prevents duplicate tasks
        # FOR THE SAME FORM but not two identical errors for different forms.
        forms_data = [{'ABGABEDATUM': 'invalid'}, {'ABGABEDATUM': 'invalid'}]
        model = get_model(db_schema.LATEST)
        validation_task1 = model.Task(0, TaskType.FORM_VALIDATION)
        validation_task2 = model.Task(1, TaskType.FORM_VALIDATION)
        batch = batch_with_form_data(forms_data, tasks=(validation_task1, validation_task2), model=model)
        vm = ValidationModule(batch, context)

        vm.process_task(validation_task1)
        vm.process_task(validation_task2)
        new_tasks = batch.new_tasks(TaskType.VERIFICATION)
        assert_length(2, new_tasks,
            message='expected a verification task for each form')
        tasks = tuple(sorted(new_tasks, key=attrgetter('form_index')))
        assert_equals(0, tasks[0].form_index)
        verification_task_first_form = tasks[0]
        assert_equals(1, tasks[1].form_index)

        db_form0 = batch.db_form(0)
        validation_task3 = db_form0.add_task(type_=TaskType.FORM_VALIDATION, status=TaskStatus.NEW)
        vm.process_task(validation_task3)
        assert_equals(TaskStatus.CLOSED, validation_task3.status)
        assert_length(2, batch.tasks(type_=TaskType.VERIFICATION, status=TaskStatus.NEW),
            message='should check for existing verification tasks instead of creating new ones too eagerly')

        verification_task_first_form.status = TaskStatus.CLOSED
        validation_task4 = model.Task(0, TaskType.FORM_VALIDATION, status=TaskStatus.NEW)
        batch.tasks().append(validation_task4)
        vm.process_task(validation_task4)
        assert_length(2, batch.new_tasks(TaskType.VERIFICATION),
            message='should create only consider new tasks for duplicates')

    def test_can_close_task_if_field_is_valid_now(self):
        form_index = 0
        errors = (AttrDict(key='bad_date', is_warning=False),)
        model = get_model(db_schema.LATEST)
        task = model.Task(form_index, TaskType.VERIFICATION,
            field_name='ABGABEDATUM', data=AttrDict(errors=errors))
        existing_tasks = [task]
        errors = {}
        batch = self._store_result_as_tasks(existing_tasks, errors, model=model)

        assert_length(0, batch.new_tasks(type_=TaskType.VERIFICATION))
        previous_tasks = batch.tasks(type_=TaskType.VERIFICATION, status=TaskStatus.CLOSED)
        assert_length(1, previous_tasks)
        assert_equals('ABGABEDATUM', previous_tasks[0].field_name)

    def test_can_close_task_if_warning_is_ignored(self):
        form_index = 0
        # actually "bad_date" should not be a warning - but on the other hand
        # ValidationModule should not care about specific keys so it's fine for
        # this test.
        errors = (AttrDict(key='bad_date', is_warning=True),)
        model = get_model(db_schema.LATEST)
        task = model.Task(form_index, TaskType.VERIFICATION,
            field_name='ABGABEDATUM', data=AttrDict(errors=errors))
        batch = self._batch_with_tasks([task], model=model)
        db_form0 = batch.db_form(0)
        db_form0.add_ignored_warning('ABGABEDATUM', 'bad_date', None)
        self._run_validation(batch, form_errors={'ABGABEDATUM': errors})

        assert_length(0, batch.new_tasks(type_=TaskType.VERIFICATION))
        previous_tasks = batch.tasks(type_=TaskType.VERIFICATION, status=TaskStatus.CLOSED)
        assert_length(1, previous_tasks)
        assert_equals('ABGABEDATUM', previous_tasks[0].field_name)

    def test_can_keep_task_if_field_is_still_invalid(self):
        form_index = 0
        date_error = AttrDict(key='bad_date', is_warning=False)
        errors = (date_error,)
        model = get_model(db_schema.LATEST)
        task = model.Task(form_index, TaskType.VERIFICATION,
            field_name='ABGABEDATUM', data=AttrDict(errors=errors))
        existing_tasks = [task]
        errors = {
            'ABGABEDATUM': errors,
        }
        batch = self._store_result_as_tasks(existing_tasks, errors, model=model)

        assert_length(1, batch.new_tasks(type_=TaskType.VERIFICATION))
        assert_length(0, batch.tasks(type_=TaskType.VERIFICATION, status=TaskStatus.CLOSED))

    def test_can_create_new_task_if_field_became_invalid(self):
        existing_tasks = []
        errors = {
            'ABGABEDATUM': (AttrDict(key='bad_date', is_warning=False), )
        }
        batch = self._store_result_as_tasks(existing_tasks, errors)

        assert_length(1, batch.new_tasks(type_=TaskType.VERIFICATION))
        assert_length(0, batch.tasks(type_=TaskType.VERIFICATION, status=TaskStatus.CLOSED))

    def test_can_create_a_new_task_for_invalid_field_if_previous_error_was_fixed(self):
        # This tests the situation where the original error was fixed but we
        # have a different error for the same field.
        form_index = 0
        date_error = AttrDict(key='bad_date', is_warning=False)
        model = get_model(db_schema.LATEST)
        task = model.Task(
            form_index,
            TaskType.VERIFICATION,
            field_name='ABGABEDATUM',
            data=AttrDict(errors=(date_error,))
        )
        existing_tasks = [task]

        fake_error = AttrDict(key='something_else', is_warning=False)
        errors = {
            'AUSSTELLUNGSDATUM': (date_error, fake_error)
        }
        batch = self._store_result_as_tasks(existing_tasks, errors, model=model)

        assert_length(1, batch.new_tasks(type_=TaskType.VERIFICATION))
        new_task = batch.new_tasks(type_=TaskType.VERIFICATION)[0]
        assert_equals('AUSSTELLUNGSDATUM', new_task.field_name)
        new_errors = new_task.data.errors
        error_keys = lambda errors: set([e['key'] for e in errors])
        assert_equals({'bad_date', 'something_else'}, error_keys(new_errors))

        previous_tasks = batch.tasks(type_=TaskType.VERIFICATION, status=TaskStatus.CLOSED)
        assert_length(1, previous_tasks)
        previous_errors = previous_tasks[0].data.errors
        assert_equals({'bad_date'}, error_keys(previous_errors))

    def test_can_ignore_warnings(self):
        plugin = fake_plugin(validators={'ABGABEDATUM': DateValidator()})
        context = build_context(plugin=plugin)
        batch = batch_with_form_data([{'ABGABEDATUM': '31.13.2015'}])
        vm = ValidationModule(batch, context)

        result = vm.process_form(batch.form(0), form_position=0)
        assert_true(result.contains_errors())

        db_form = batch.db_form(0)
        db_form.add_ignored_warning('ABGABEDATUM', 'bad_date', '31.13.2015')
        filtered_result = vm.process_form(batch.form(0), form_position=0)
        assert_false(filtered_result.contains_errors())

    def test_can_process_tasks(self):
        plugin = fake_plugin(validators={'ABGABEDATUM': DateValidator()})
        context = build_context(plugin=plugin)
        forms_data = [{'ABGABEDATUM': 'invalid'}, {'ABGABEDATUM': 'invalid'}]
        model = get_model(db_schema.LATEST)
        tasks = (
            model.Task(0, TaskType.FORM_VALIDATION),
            model.Task(1, TaskType.FORM_VALIDATION),
        )
        batch = batch_with_form_data(forms_data, tasks=tasks, model=model)
        vm = ValidationModule(batch, context)
        vm.process_tasks()

        assert_length(0, batch.new_tasks(type_=TaskType.FORM_VALIDATION))
        assert_length(2, batch.new_tasks(type_=TaskType.VERIFICATION))

    def test_can_report_progress_while_processing_tasks(self):
        plugin = fake_plugin(validators={'ABGABEDATUM': DateValidator()})
        context = build_context(plugin=plugin)
        forms_data = [{'ABGABEDATUM': 'invalid'}, {'ABGABEDATUM': 'invalid'}]
        model = get_model(db_schema.LATEST)
        tasks = (
            model.Task(0, TaskType.FORM_VALIDATION),
            model.Task(1, TaskType.FORM_VALIDATION),
        )
        batch = batch_with_form_data(forms_data, tasks=tasks, model=model)
        vm = ValidationModule(batch, context)
        progress = ProgressRecorder()
        vm.process_tasks(progress_factory=progress)

        assert_length(0, batch.new_tasks(type_=TaskType.FORM_VALIDATION))
        assert_length(2, batch.new_tasks(type_=TaskType.VERIFICATION))
        assert_true(progress.is_finished)
        assert_equals([0, 1], progress.updates)

    # --- helpers -------------------------------------------------------------

    def _batch_with_tasks(self, existing_tasks, model):
        forms_data = [{'ABGABEDATUM': 'invalid', 'AUSSTELLUNGSDATUM': '01.01.2016'}]
        return batch_with_form_data(forms_data, tasks=existing_tasks, model=model)

    def _run_validation(self, batch, form_errors):
        schema = {
            'ABGABEDATUM': DateValidator(),
            'AUSSTELLUNGSDATUM': DateValidator(),
        }
        plugin = fake_plugin(validators=schema)
        context = build_context(plugin=plugin)

        errors = {}
        for field_name in schema:
            errors[field_name] = form_errors.get(field_name, ())
        validation_result = FormData(child_names=tuple(schema))
        validation_result.set(errors=errors)
        if form_errors:
            assert_true(validation_result.contains_errors())

        validator = ValidationModule(batch, context)
        form_index = 0
        validator.store_validation_state_as_tasks(form_index, validation_result)
        return batch

    def _store_result_as_tasks(self, existing_tasks, form_errors, *, model=None):
        batch = self._batch_with_tasks(existing_tasks, model=model)
        return self._run_validation(batch, form_errors)

    def assert_exception_report_exists(self, exception_dir):
        for root_dir, directories, files in os.walk(exception_dir):
            for filename in files:
                path = os.path.join(root_dir, filename)
                if not path.lower().endswith('.txt'):
                    continue
                return path
        self.fail('no failure report found below "%s"' % exception_dir)
