# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from datetime import date as Date
from collections import OrderedDict

from pycerberus.api import Validator
from pythonic_testcase import *

from srw.rdblib import DataBunch
from srw.rdblib.cdb import create_cdb_with_form_values
from srw.rdblib.ibf import create_ibf
from srw.formvalidation.generic import FormValidation, PydicaSchema
from srw.formvalidation.generic.validators import DateValidator

from ddc.lib.attribute_dict import AttrDict
from ddc.lib.log_proxy import get_logger as get_logger_proxy
from ddc.plugin import PluginLoader
from ddc.storage import create_sqlite_db, Batch
from ddc.storage.ask import create_ask


__all__ = [
    'batch_with_form_data',
    'build_context',
    'fake_plugin',
    'generate_pic',
    'PydicaValidationTestCase'
]


def batch_with_form_data(form_data, tasks=(), *, model=None):
    nr_forms = len(form_data)
    db = create_sqlite_db(tasks=tasks, model=model)
    databunch = DataBunch(
        cdb=create_cdb_with_form_values(form_data),
        ibf=create_ibf(nr_images=nr_forms),
        ask=create_ask(),
        db=db,
    )
    db.session.commit()
    return Batch.init_from_bunch(databunch, create_persistent_db=False)

def fake_plugin(validators):
    schema = PydicaSchema()
    for key, validator in validators.items():
        schema.add(key, validator)

    plugin = AttrDict({
        'detect_formtype': lambda form_values: None,
        'form_validator': lambda formtype=None, *, context: FormValidation(schema=schema)
    })
    return plugin

class FakePluginManager(PluginLoader):
    def __init__(self, plugins):
        self.activated_plugins = plugins
        self.enabled_plugins = self.activated_plugins


def build_context(plugin=None, env_dir=None, get_logger=None):
    if plugin is None:
        plugin = fake_plugin({'GEBURTSDATUM': DateValidator(reject_future_dates=True)})
    if get_logger is None:
        _get_logger = lambda name, **kwargs: get_logger_proxy(name, log=False, **kwargs)
    else:
        _get_logger = get_logger
    config = {'pydica': {}}
    context = AttrDict(
        plugin_manager=FakePluginManager({'fake': plugin}),
        get_logger=_get_logger,
        config=config,
        settings=config['pydica'],
    )
    if env_dir:
        context['env_dir'] = env_dir
    return context

def generate_pic(scan_nr=1):
    today = Date.today()
    year_digit_str = str(today.year)[-1]
    month_str = '%02d' % today.month
    date_prefix = year_digit_str + month_str
    customer_str = '123'
    nr_str = '%05d' % scan_nr
    return date_prefix + customer_str + nr_str + '024'

def valid_prescription_values(*, with_pic=False, **values):
    # The idea is to return prescription values which should be considered as
    # "valid" in the test setup (as the field configuration is semi-hardcoded
    # at the moment we need to use the actual field names).
    from ddc.client.config import ALL_FIELD_NAMES
    valid_values = OrderedDict()
    for field_name in ALL_FIELD_NAMES:
        valid_values[field_name] = ''
    today = Date.today()
    valid_values.update({
        'LANR': '240000601',
        'BSNR': '179999900',
        'GEBURTSDATUM': '30.08.1950',
        'AUSSTELLUNGSDATUM': '%02d.%02d.%02d' % (today.day, today.month, today.year),
        'ABGABEDATUM': '%02d%02d%02d' % (today.day, today.month, today.year),
    })
    valid_values.update(values)
    if with_pic:
        # with_pic='...' will ensure we use a specific PIC
        pic_str = '10501200042024' if (with_pic == True) else with_pic
        valid_values['pic'] = pic_str
    return valid_values


def failing_form_validator(fieldname, critical_error=False):
    class FailingFormValidator(Validator):
        exception_if_invalid = False

        def __init__(self, first):
            self._first = first
            super(FailingFormValidator, self).__init__()

        def requires(self, context):
            return (self._first, )

        def messages(self):
            return {'too_close': 'too close'}

        def validate(self, fields, context):
            fieldname = self._first
            value_first = fields[fieldname]
            error = self._error('too_close', value_first, context, is_critical=critical_error)
            result = context['result']
            result.children[fieldname].add_error(error)
            return fields
    return FailingFormValidator(fieldname)

