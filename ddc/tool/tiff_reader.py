# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from io import BytesIO

from PIL import Image

def read_tiff_page(filename, file_or_buffer=None, pageno=1):
    """
    Read a file or buffer and create a new tiff buffer positioned
    to the page wanted.

    Returns a tuple (filename, buffer)

    On Error, returns None
    """
    f = file_or_buffer
    if f and not hasattr(f, 'seek'):
        f = BytesIO(f)
    else:
        f = open(filename, 'rb')

    im = Image.open(f)
    if im.format == 'TIFF':
        # all IBF files produced by the Walther software contain TIFF images
        # but some scripts create synthetic IBF files which contain JPG images
        # (so the IBF size stays manageable).
        im.seek(pageno)
    newimg = BytesIO()
    im.save(newimg, format='tiff')
    return filename, newimg.getvalue()
