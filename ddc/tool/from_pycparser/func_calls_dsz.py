#-----------------------------------------------------------------
# pycparser: func_defs.py
#
# Using pycparser for printing out all the calls of some function
# in a C file.
#
# Copyright (C) 2008-2011, Eli Bendersky
# License: BSD
#-----------------------------------------------------------------
from __future__ import print_function
import sys

# This is not required if you've installed pycparser into
# your site-packages/ with setup.py
#
sys.path.extend(['.', '..'])

from pycparser import c_parser, c_ast, parse_file


# A visitor with some state information (the funcname it's 
# looking for)
#
class FuncCallVisitor(c_ast.NodeVisitor):
    def __init__(self, *funcnames):
        self.funcnames = funcnames

    def visit_FuncCall(self, node):
        if node.name.name in self.funcnames:
            print('%s called at %s' % (
                    node.name.name, node.name.coord))
            try:
                sect = node.args.exprs[0].value
                name = node.args.exprs[1].value
                valu = node.args.exprs[2].value
                print(sect, name, valu)
            except AttributeError:
                pass # for testing

def show_func_calls(filename, funcnames, use_cache=True):
    import os, cPickle as pickle
    pickle_filename = os.path.basename(filename + '.ast')
    if use_cache and os.path.exists(pickle_filename):
        ast = pickle.load(file(pickle_filename))
    else:
        if sys.platform == 'darwin':
            cpp_path, cpp_args = 'gcc', ['-E']
        else:
            cpp_path, cpp_args = 'cpp', []
        ast = parse_file(filename, use_cpp=True, cpp_path=cpp_path,
                         cpp_args=cpp_args+ [r'-I../util/fake_libc_include']
                         )
        if use_cache:
            pickle.dump(ast, file(pickle_filename, 'wb'), -1)
        
    v = FuncCallVisitor(*funcnames)
    v.visit(ast)


if __name__ == "__main__":
    if len(sys.argv) > 2:
        filename = sys.argv[1]
        func = sys.argv[2:]
    else:
        #filename = 'c_files/hash.c'
        #func = 'malloc',
        import os
        os.chdir(os.path.dirname(__file__))
        filename = '../walther/fields_ini/APO_FSRH.C'
        func = 'GetPrivateProfileInt', 'GetPrivateProfileString'
    show_func_calls(filename, func, False)



