#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

"""

Generate an INI file from an AST file.
"""

import sys, os, cPickle as pickle

from pycparser import c_ast
from configobj import ConfigObj

class FuncCallVisitor(c_ast.NodeVisitor):
    def __init__(self, *funcnames):
        self.funcnames = funcnames
        self.nodes = []

    def visit_FuncCall(self, node):
        if node.name.name in self.funcnames:
            self.nodes.append(node)

def ast2ini(filename, ini_filename):
    ast = pickle.load(file(filename))
    funcnames = 'GetPrivateProfileInt', 'GetPrivateProfileString'
    v = FuncCallVisitor(*funcnames)
    v.visit(ast)
    conf = ConfigObj()
    conf.filename = ini_filename
    def clean(v):
        return v if v == repr(v) else eval(v)
    conf['KassenName'] = {}
    conf['KassenName']['Name'] = 'KASSENNAME'
    for node in v.nodes:
        sect = clean(node.args.exprs[0].value)
        name = clean(node.args.exprs[1].value)
        valu = clean(node.args.exprs[2].value)
        if sect not in conf:
            conf[sect] = {}
        conf[sect][name] = valu
    conf.write()
    text = file(ini_filename).read()
    text = text.replace('\n[', '\n\n[') # de-uglify
    file(ini_filename, 'w').write(text)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("""
ast2ini.py  <ast-pickle> [<out-inifile>]

Example:
    python ast2ini.py generated/APO_FSRH.C.ast
    should generate generated/APO_FSRH.ini
""")
        sys.exit(1)
        
    filename = sys.argv[1]
    if sys.argv[2:]:
        ini_filename = sys.argv[2]
    else:
        script_dir = os.path.dirname(__file__)
        outdir = os.path.join(script_dir, 'generated')
        tmp_name = os.path.basename(filename).split('.')[0]
        ini_filename = os.path.join(outdir, tmp_name + '.ini')
    ast2ini(filename, ini_filename)
    print('+++ %s successfully created from %s' % (ini_filename, filename))

