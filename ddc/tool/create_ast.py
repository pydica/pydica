#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

"""

Generate an AST pickle file from a C source.
These files are pickled as <filename.c>.ast

After that step, a windows-like .INI file can be generated.

This is possible with the great packages pycparser and ply.

"""

import sys, os, cPickle as pickle

from pycparser import c_parser, c_ast, parse_file


def create_ast(cfilename, pickle_filename):
    if sys.platform == 'darwin':
        cpp_path, cpp_args = 'gcc', ['-E']
    else:
        cpp_path, cpp_args = 'cpp', []
    ast = parse_file(filename, use_cpp=True, cpp_path=cpp_path,
                     cpp_args= cpp_args + [r'-I./util/fake_libc_include']
                     )

    pickle.dump(ast, file(pickle_filename, 'wb'), -1)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("""
create_ast.py <c-filename> [<ast filename, default=./generated/<c-filename>.ast]

Example:
    python create_ast.py walther/fields_ini/APO_FSRH.C
    should generate generated/APO_FSRH.C.ast
""")
        sys.exit(1)

    filename = sys.argv[1]
    if sys.argv[2:]:
        pickle_filename = sys.argv[2]
    else:
        script_dir = os.path.dirname(__file__)
        outdir = os.path.join(script_dir, 'generated')
        pickle_filename = os.path.join(outdir, os.path.basename(filename + '.ast'))
    create_ast(filename, pickle_filename)
    print('+++ %s successfully created from %s' % (pickle_filename, filename))

