#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

"""
Remote Control
==============

This module allows to inject limited commands into an application
by polling for file changes.

Polling is used instead of more efficient tools like
QFileSystemWatcher from PySide or the sophisticated watchdog
( http://packages.python.org/watchdog/installation.html )
with many dependencies, pyinotify, etc.

Reason: it should work on any platform and on network drives.
"""

'''
Protocol:
The remote controller writes and closes the command file.
Then it creates the handshake file.

The client(server) waits for the handshake file to exist.
It then executes the command file.

After that, it removes the command and the handshake file (in this order).
'''

from collections import namedtuple
from io import BytesIO
import os
import sys

import ddc.config.pyside_fix
from PySide import QtCore, QtGui, QtNetwork
from srw.rdblib.cdb import open_cdb

from ddc.client.config import ALL_FIELD_NAMES
from ddc.storage import form_index_for_pic
from ddc.tool.cdb_collection import CDB_Collection

# fixed file names
COMMAND_FN = 'PYD_REM.TXT'
HANDSHAKE_FN = 'PYD_REM.OK'
INTERVAL_MS = 100


class ConfigError(Exception):
    pass

class InvalidCmdError(Exception):
    pass

class ProtocolError(Exception):
    pass


_ArgDef = namedtuple('_ArgDef', ('name', 'type', 'description'))

class _ArgTypes(object):
    _argdefs = (
        ('cdb_filename', str,
         '''
         Der Name (inkl. Pfad) einer CDB- oder RDB-Datei, die zu öffnen ist.
         '''),
        ('ibf_filename', str,
         '''
         Der Name (inkl. Pfad) zur IBF-Datei des gerade geöffneten Stapels.
         '''),
        ('beleg_nr',     int,
         '''
         Die Nummer des Belegs innerhalb der Datei, die angesprungen wird.
         '''),
        ('pic_nr',       str,
         '''
         Die PIC-Nr. (= Beleg-Nr. = kodierte Nummer = CODNR), die der
         angesprungene Beleg haben muß.
         '''),
        ('message',      str,
         '''
         Ein einzeilger Text (Encoding noch zu klären). Der Text enthält
         kein Semikolon!
         '''),
        ('field_name',   str,
         '''
         Ein Bezeichner der sich auf das aktuelle Formular bezieht. Schreibweise
         irrelevant, der Name wird im Formulardatensatz gesucht. Keine Fehlermeldung
         wenn nicht gefunden, da nur zur Visualisierung (momentan).
         '''),

        ('DEFAULT',      str,
         '''
         Bisher unspezifiziert, ignoriert
         '''),
    )

    def __init__(self):
        for name, tp, desc in self._argdefs:
            desc = ' '.join(desc.split())
            setattr(self, name, _ArgDef._make((name, tp, desc)))

argtypes = _ArgTypes()


class _CommandDef(object):
    '''
    Definition of a command, its syntax, parameter names and types
    '''

    def __init__(self, prototype, description):
        self.__doc__ = description
        #cmd, *optarg = prototype.split(None, 1)
        # python2.7 does not support this, therefore:
        cmd, optarg = (prototype.split(None, 1) + [''])[:2]
        self.name = cmd.lower()
        args = optarg and optarg.split(';') or []
        self.arguments = tuple(getattr(argtypes, argname)
                               for argname in args)

    def __repr__(self):
        return '<{} {}>'.format(self.name.upper(),
                                ';'.join(arg.name for arg in self.arguments))
    @property
    def signature(self):
        return tuple(arg.type for arg in self.arguments)


class CommandDefs(object):
    '''
    This class defines the commands in an abstract way. The commands are
    intentionally not instantiated by some nifty decorative class, in order to
    keep the definition as magic-free as possible. (compare also the dbdefs
    module which is similar.)
    CommandDefs will probably be moved out of this module at some time.

    Some magic is involved later in class "Command", the real use-case.
    '''

    cmd_opendel = (
       'OPENDEL cdb_filename;beleg_nr;pic_nr',
       '''
       Öffnet die angegebene Datei mit dem angegebenen Beleg.
       Der Beleg kann gelöscht sein.
       Abbruch der Befehlssequenz wenn weder beleg_nr noch pic_nr
       gefunden werden. Setzt die Wirkung von HIGHLIGHT zurueck.
       ''')

    cmd_openasc = (
       'OPENASC cdb_filename;beleg_nr;pic_nr',
       '''
       Gleiche Arbeitsweise wie OPENDEL, arbeitet nur auf nicht gelöschten
       Belegen. Nur existierende Belege sind vom Ascii-Interface erreichbar.
       ''')

    cmd_highlight = (
        'HIGHLIGHT field_name;DEFAULT',
        '''
        Zeigt das angegebene Feld markiert an. Zur Zeit gibt es nur die Auszeichnung
        DEFAULT, sie wird noch nicht ausgewertet. Es können beliebig viele HIGHLIGHT Befehle in einer
        Befehlssequenz angegeben werden.
        (Befehl ist erweiterbar)
        ''')

    cmd_setfocus = (
        'SETFOCUS field_name;DEFAULT',
        '''
        Setzt den Eingabefocus auf das angegebene Feld. Parameter sind undefiniert.
        Parameter ist DEFAULT, wird noch nicht ausgewertet. Der jeweils letzte
        SETFOCUS Befehl ist wirksam, da nur ein Feld fokussiert werden kann.
        (Befehl ist erweiterbar)
        ''')

    cmd_delete = (
       'DELETE cdb_filename;beleg_nr;pic_nr',
       '''
       Setzt Löschmarkierung für den angegebenen Beleg, falls dieser bereits
       angezeigt wird.
       Keine Aktion, falls ein anderes Rezept angezeigt wird.
       ''')

    cmd_delete_current = (
        'DELETE_CURRENT',
        '''
        Setzt Löschmarkierung fuer aktuellen Beleg.
        Keine Aktion wenn bereits markiert.
        ''')

    cmd_undelete_current = (
        'UNDELETE_CURRENT',
        '''
        Entfernt Löschmarkierung fuer aktuellen Beleg.
        Keine Aktion wenn Beleg nicht markiert war.
        ''')

    cmd_reklafax = (
        'REKLAFAX cdb_filename;beleg_nr;pic_nr;message',
        '''
        Kopiert einen String in die Zwischenablage. Der String hat das folgende
        Format:

            REKLAFAX REQUEST: CODNR: pic_nr BELEGNR: beleg_nr FILE: ibf_filename MESSAGE: message

        Die Zwischenablage wird von einem anderen Programm überwacht und beim
        Auftauchen eines solchen Strings erfolgt eine entsprechende Reaktion.
        Pydica braucht sich nach dem Kopieren in die Zwischenablage darum nicht
        zu kümmern.

        **Achtung: Die ``beleg_nr`` muß an dieser Stelle dreistellig vorgenullt übergeben werden!**
        ''')

    cmd_ascii = (
        'ASCII cdb_filename',
        '''Erstellt für die angegebene Datei eine ASCII-Datei.''')

    cmd_close = (
        'CLOSE',
        '''Schliesst die aktuelle Datei. Momentan notwendig fuer pytest''')


class Command(object):
    '''
    Command definition class, based upon CommandDefs.
    Instantiation of this class results in a parsed command or an error.
    '''
    def def_commands(namespace):
        for cmd in dir(CommandDefs):
            if cmd.startswith('cmd_'):
                namespace[cmd] = cd = _CommandDef(*getattr(CommandDefs, cmd))
                argnames = list(arg.name for arg in cd.arguments)
                namespace['arg_' + cd.name] = namedtuple(cd.name, argnames)

    def_commands(locals())
    del def_commands

    def __init__(self, command_str):
        parts = command_str.split(None, 1)
        if not parts:
            raise SyntaxError('no command given')
        name = parts[0]
        args = parts[1:] and parts[1] or ''
        self.name = name.lower()
        try:
            cmd = getattr(self.__class__, 'cmd_' + self.name)
            argtup = getattr(self.__class__, 'arg_' + self.name)
        except AttributeError as e:
            raise InvalidCmdError(e)
        args = args.split(';') if args else []
        if len(cmd.arguments) < len(args):
            raise SyntaxError('too few arguments for {}: {}'.format(name, args))
        elif len(cmd.arguments) > len(args):
            raise SyntaxError('too many arguments for {}: {}'.format(name, args))
        arglis = []
        for i, arg in enumerate(cmd.arguments):
            v = args[i]
            argtype = cmd.arguments[i]
            tp = argtype.type
            if tp in (int, str):
                try:
                    v = tp(v)
                except ValueError as e:
                    raise TypeError(e)
            if argtype.name == 'beleg_nr':
                v = v - 1 # counted from zero, internally
                if v < 0:
                    raise ValueError('invalid {}: {}'.format(argtype.name, arg))
            arglis.append(v)

        self.argvalues = argtup._make(arglis)

    def __repr__(self):
        return '<{}>'.format(self.argvalues)


class RemoteControl(QtCore.QObject):
    '''
    A timer driven remote control class that polls files.
    It emits signals when a new command is received.
    By using a signal instead of a direct function call, it is
    easy to change to a thread-based implementation.
    '''
    c = Command
    got_opendel_cmd          = QtCore.Signal(*c.cmd_opendel.signature)
    got_openasc_cmd          = QtCore.Signal(*c.cmd_openasc.signature)
    got_delete_cmd           = QtCore.Signal(*c.cmd_delete.signature)
    got_delete_current_cmd   = QtCore.Signal(*c.cmd_delete_current.signature)
    got_undelete_current_cmd = QtCore.Signal(*c.cmd_undelete_current.signature)
    got_highlight_cmd        = QtCore.Signal(*c.cmd_highlight.signature)
    got_setfocus_cmd         = QtCore.Signal(*c.cmd_setfocus.signature)
    got_reklafax_cmd         = QtCore.Signal(*c.cmd_reklafax.signature)
    got_ascii_cmd            = QtCore.Signal(*c.cmd_ascii.signature)
    got_close_cmd            = QtCore.Signal(*c.cmd_close.signature)

    got_an_error             = QtCore.Signal(Exception)
    del c

    def __init__(self, context, debug=False, *, watch_path=None, udp_port=None):
        super(RemoteControl, self).__init__()
        self.context = context
        self.log = self.context.get_logger('pydica.remote_control')
        self.watch_path = watch_path
        self.udp_port = udp_port

        if self.udp_port:
            self.use_udp = True
        else:
            if not os.path.isdir(watch_path):
                raise ConfigError('directory {} not found'.format(watch_path))
            self.use_udp = False
        self.udp_socket = None
        self.hsh_fp = os.path.join(watch_path, HANDSHAKE_FN)
        self.cmd_fn = os.path.join(watch_path, COMMAND_FN)
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self._on_timeout)
        self.debug = debug
        self.idle_count = 0
        self.action_count = 0

    def start(self, interval=INTERVAL_MS):
        if self.use_udp:
            self.udp_socket = QtNetwork.QUdpSocket(self)
            result = self.udp_socket.bind(self.udp_port)
            if result != True:
                raise AssertionError('Unable to bind to socket for port %d (UDP)' % self.udp_port)
            self.log.debug('listening on port %d (UDP)', self.udp_port)
            self.connect(
                self.udp_socket,
                QtCore.SIGNAL('readyRead()'),
                self, QtCore.SLOT('read_received_datagrams()')
            )
        else:
            self.log.info('using control path "%s"', self.watch_path)
            self.timer.stop()
            self.timer.setSingleShot(True)
            self.interval = interval
            self._restart()

    def _restart(self):
        self.timer.start(self.interval)

    def stop(self):
        self.timer.stop()

    def _progress(self, msg):
        if not self.debug:
            return
        sys.stdout.write(msg)
        sys.stdout.flush()

    def _on_timeout(self):
        if self._check_condition():
            self._progress('X')
            self.action_count += 1
            self.do_action()
            self._remove_protocol_files()
        else:
            self._progress('.')
            self.idle_count += 1
        self._restart()

    def _check_condition(self):
        # Jan 2017: sporadic delays (up to 3 seconds) on Windows 10 (feature
        # update 1607) when using os.path.isfile() - even though the file was
        # already visible in the Windows Explorer.
        # Somehow using os.listdir() is a workaround for the problem - as long
        # as an Explorer window is present...
        #ok_file_exists = os.path.isfile(self.hsh_fp)
        ok_file_exists = (HANDSHAKE_FN in os.listdir(self.watch_path))
        return ok_file_exists

    def do_action(self):
        if not os.path.isfile(self.cmd_fn):
            msg = '"%s" not found!' % self.cmd_fn
            self.log.warning(msg)
            self.got_an_error.emit(ConfigError(msg))
            return
        with open(self.cmd_fn, 'rb') as fp:
            cmd_fp = BytesIO(fp.read())
        self.process_command_data(cmd_fp)

    def read_received_datagrams(self):
        while self.udp_socket.hasPendingDatagrams():
            datagram = QtCore.QByteArray()
            datagram.resize(self.udp_socket.pendingDatagramSize())

            result = self.udp_socket.readDatagram(datagram.size())
            request_data = result[0].data()
            self.process_command_data(BytesIO(request_data))

    def process_command_data(self, cmd_fp):
        cmdlines = []
        for line in cmd_fp.readlines():
            try:
                line = line.decode('utf8').strip()
            except Exception as e:
                self.log.exception('unable to decode received request %r' % line)
                self.got_an_error.emit(e)
                return
            if line and not line.startswith('#'):
                cmdlines.append(line)
        commands = []
        for line in cmdlines:
            try:
                self.log.debug('parsing: %r', line)
                parsed = Command(line)
                commands.append(parsed)
            except Exception as e:
                self.log.exception(e)
                self.got_an_error.emit(e)
                return
        # the rest will be done in the application
        for cmd in commands:
            signal_name = 'got_%s_cmd' % cmd.name
            signal = getattr(self, signal_name)
            self.log.debug('emitting %s', cmd)
            signal.emit(*cmd.argvalues)

    def _remove_protocol_files(self):
        for filename in (self.cmd_fn, self.hsh_fp):
            if os.path.exists(filename):
                os.remove(filename)


class CDB_Reader(QtCore.QObject):
    signal_open      = QtCore.Signal(CDB_Collection, int)
    signal_goto      = QtCore.Signal(int)
    signal_setfocus  = QtCore.Signal(str)
    signal_highlight = QtCore.Signal(str)
    signal_reklafax  = QtCore.Signal(str, int, str, str)
    signal_ascii     = QtCore.Signal(str)
    signal_delete    = QtCore.Signal(str)
    signal_delete_current    = QtCore.Signal()
    signal_undelete_current  = QtCore.Signal()
    signal_close     = QtCore.Signal()
    signal_error     = QtCore.Signal(Exception)

    def __init__(self, context, *args):
        super(CDB_Reader, self).__init__(*args)
        self.context = context
        self.seq = None
        self.log = self.context.get_logger('pydica.remote_control.cdbreader')

        self._connect_strings = {}
        # save the registered signal names
        def dummy(self, *args, **kw):
            pass
        for signame in self.__class__.__dict__.keys():
            if signame.startswith('signal_'):
                self._this_sig = getattr(self, signame)
                self._this_sig.connect(dummy)
                self._this_sig.disconnect(dummy)

    def msgbox_critical(self, title, text):
        msgbox = QtGui.QMessageBox()
        msgbox.setText(title)
        msgbox.setInformativeText(text)
        msgbox.setIcon(QtGui.QMessageBox.Critical)
        msgbox.exec_()

    def connectNotify(self, signalstr):
        # just to find out what the names of the signals are ;-)
        self._connect_strings[self._this_sig] = signalstr
        super(CDB_Reader, self).connectNotify(signalstr)

    def opendel_cmd(self, fname, nr, codnr):
        return self._open_cmd(fname, nr, codnr, ignore_deleted_forms=False)

    def openasc_cmd(self, fname, nr, codnr):
        return self._open_cmd(fname, nr, codnr, ignore_deleted_forms=True)

    def _display_critical_error(self, msg, extra_text=None):
        if self.receivers(self._connect_strings[self.signal_error]) > 0:
            # there is a receiver for the message
            self.signal_error.emit(msg)
            return True

        qt_app = QtGui.QApplication.instance()
        # The tests use a QCoreApplication and we must not create
        # message boxes there.
        is_gui_app = isinstance(qt_app, QtGui.QApplication)
        if is_gui_app:
            # we have a running app and can show a message
            self.msgbox_critical(msg, extra_text or '')
        return False

    def _open_cmd(self, fname, nr, codnr, ignore_deleted_forms):
        fname = os.path.normcase(os.path.realpath(fname.replace('\\', os.path.sep)))
        seq = self.seq
        cdb_basename = os.path.basename(fname)
        log = self.context.get_logger('pydica.storage.cdb', context=cdb_basename)
        is_cdb_already_open = (self.seq and (self.seq.cdb_filename == fname))
        if not is_cdb_already_open:
            result = open_cdb(fname, field_names=ALL_FIELD_NAMES)
            if result == False:
                log.info(result.message)
                self._display_critical_error(result.message, extra_text=fname)
                return None
            # actually we should be able to reuse the fp but now the simplest
            # thing is to just open it again...
            result.cdb_fp.close()
            # LATER: Most of the time the remote control is only used to view
            # data so access='read' would be the correct thing to do.
            # There is only exception to this which is deletion of prescriptions
            # (hence we used to acquire an exclusive lock). As we do so when
            # opening the CDB/IBF file no other program is allowed to access
            # the data which is a problem in our workflow (ASCII search tool
            # with Reklafax).
            # The "correct" solution is to request exclusive locks only when
            # actually deleting something (and dropping back to shared locks
            # when this is done). However as a short-term fix we completely
            # remove any locking with
            #    access='dontcare'
            # as this mode worked in production without problems for many months
            # already.
            seq = CDB_Collection.from_cdb_filename(fname=fname, access='dontcare', log=log)
        form_index = form_index_for_pic(
            seq.batch,
            pic=codnr,
            index_hint=nr,\
            ignore_deleted_forms=ignore_deleted_forms
        )
        log.debug('PIC %s entspricht form index %r (hint: %r)', codnr, form_index, nr)
        if form_index is None:
            msg = 'PIC %s nicht in %s gefunden!' % (codnr, fname)
            log.warn(msg)
            if not is_cdb_already_open:
                # ensure that all locks are released for this newly opened sequence...
                seq.close()
            notified_receiver = self._display_critical_error(msg, extra_text=fname)
            if not notified_receiver:
                # Pydica has undefined behavior after this message - the next
                # request image sometimes is wrong; that's why we abort the
                # program at this point until we find a solution for this
                raise ValueError(msg)
            return None

        seq.seek(form_index)
        self.seq = seq
        if not is_cdb_already_open:
            self.signal_open.emit(seq, form_index)
        else:
            self.signal_goto.emit(form_index)
        return form_index

    def setfocus_cmd(self, field_name):
        self.signal_setfocus.emit(field_name)

    def highlight_cmd(self, field_name):
        self.signal_highlight.emit(field_name)

    def delete_cmd(self, fname, nr, codnr):
        fname = os.path.normcase(os.path.realpath(fname.replace('\\', os.path.sep)))
        cdb_basename = os.path.basename(fname)
        log = self.context.get_logger('pydica.storage.cdb', context=cdb_basename)
        current_cdb_path = self.seq.cdb_filename if self.seq else None
        # each PIC number should appear only once (unless in deleted forms) but
        # an extra check here does no harm I guess.
        is_cdb_already_open = (current_cdb_path == fname)
        if not is_cdb_already_open:
            message = 'DELETE-Befehl für derzeit nicht geöffnete CDB empfangen.'
            log.warning(message + ' (%s)' % fname)
            log.info('aktuell angezeigte CDB: %s.', current_cdb_path)
            self._display_critical_error(message, extra_text=fname)
            return None
        # The remote control does not have access to the UI's BatchState so we
        # need to pass the PIC number in our signal and let the UI check it.
        self.signal_delete.emit(codnr)

    def delete_current_cmd(self):
        self.signal_delete_current.emit()

    def undelete_current_cmd(self):
        self.signal_undelete_current.emit()

    def reklafax_cmd(self,  fname, nr, codnr, message):
        self.signal_reklafax.emit(fname, nr, codnr, message)

    def ascii_cmd(self, fname):
        self.signal_ascii.emit(fname)

    def close_cmd(self):
        if self.seq:
            self.seq.close()
            self.seq = None
