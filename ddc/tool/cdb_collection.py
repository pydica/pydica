# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

"""

    CDB collection

This interface is kept very simple, as a starter.
"""
import os
import sys
from timeit import default_timer as timer

from srw.rdblib import DataBunch
from srw.rdblib.ibf import TiffHandler

import ddc
from ddc.tool.datamanager import bunch_from_cdb_name
from ddc.lib.log_proxy import l_
from ddc.storage import create_sqlite_db, Batch
from ddc.tool.tiff_reader import read_tiff_page


def default_cdb_filename():
    cdb_filename = os.path.join(ddc.rootpath, 'ddc/tool/test/data/form_batch/00035100.CDB')
    if len(sys.argv) > 1:
        arg1 = sys.argv[1]
        # extra check if argument is likely a cdb file to get tests to work
        # just in case the command line was something like like
        #    nosetests <pyfile>
        if os.path.isfile(arg1) and arg1.lower().endswith('.cdb'):
            return arg1
    return cdb_filename


class CDB_Collection(object):
    def __init__(self, batch, log=None):
        self.batch = batch
        self.log = l_(log)
        self.forms = None
        self.form_count = None
        self.pos = 0
        self._load_forms()

    @classmethod
    def from_cdb_filename(cls, fname, access='write', log=None):
        cdb_bunch = bunch_from_cdb_name(fname, log=l_(log))
        # ensure that we always have a SQLite DB (even if it is only in-memory)
        db = cdb_bunch.db or create_sqlite_db()
        bunch = DataBunch.merge(cdb_bunch, db=db)
        return cls.from_bunch(bunch, access=access, log=log)

    @classmethod
    def from_bunch(cls, bunch, access='write', log=None):
        batch = Batch.init_from_bunch(bunch, delay_load=False, access=access, log=log)
        return CDB_Collection(batch=batch, log=log)

    def seek(self, num):
        if num < 0 or num >= self.cdb.count():
            raise IndexError('CDB index out of range')
        self.pos = num

    def load_image(self, pageno=1):
        start = timer()
        nr = self.current_form._form_index
        tiff_data = self.ibf.get_tiff_image(nr)
        ret = read_tiff_page('dummy.tif', tiff_data, pageno)
        # quick hack to get the transform to the real dimensions
        class Correct(tuple):
            correct = (0, 0, 1192, 832)
        result = Correct(ret) # hey what a funny cor-rect ;-)
        duration = timer() - start
        self.log.debug('loading image #%d (page %d) took %.5f seconds', nr, pageno, duration)
        return result

    def _load_forms(self):
        self.form_count = self.cdb.count()
        self.forms = list(CDB_Form(self, i) for i in range(self.form_count))

    @property
    def cdb(self):
        return self.batch.cdb

    @property
    def ibf(self):
        return self.batch.ibf

    @property
    def db(self):
        return self.batch.db

    @property
    def current_form(self):
        return self.forms[self.pos]

    @property
    def cdb_filename(self):
        return self.batch.bunch.cdb

    def commit(self):
        self.batch.commit()

    def close(self, commit=False):
        self.batch.close(commit=commit)


'''
Handling the CDB_Form efficiently is much more complicated than expected:

We may not touch anything of the cdb_tool.Form before actually needed, or the
speed optimization would be lost. cdb_tool.Image also cannot be inspected,
because before that we need to look into the form.

Rules for efficient handling of the form object:

- do not touch fields that are optimized

- do not evaluate the 'rec' fields. The indirection must stay one level
  above, to allow changing the field values.
'''

class CDB_Form(object):

    def __init__(self, parent, index):
        self.parent = parent
        self._form_index = index
        self._dirty_flag = False  # used in the client

    @property
    def _th(self):
        return self.parent.batch.tiff_handler(self._form_index)

    @property
    def _form_data(self):
        return self.parent.cdb.forms[self._form_index]

    @property
    def _form_header(self):
        return self._form_data.form_header

    @property
    def _image_data(self):
        return self.parent.ibf.image_entries[self._form_index]

    def delete(self):
        if not self.is_deleted():
            self._del_operation(False)

    def undelete(self):
        if self.is_deleted():
            self._del_operation(True)

    def _del_operation(self, undel):
        if not self._th:
            # the tiff handler will be installed late, on demand.
            # It's action is to update the value of long_data, the logical name
            # of the tiff file.
            # The special name 'DELETED' is used as a marker for the old
            # software. This also needs to be written to the IBF index and to
            # the CDB fields, as can be seen below.
            # The tiff handler encapsulates the tiff related part of the action.

            self._th = TiffHandler(self.parent.ibf, self._form_index)
        if undel:
            # Note: long_data2 is the second version of the tiff header.
            # This is never written, but kept as a backup for un-deleting.
            repl_str = self._th.long_data2.rec.page_name
        else:
            repl_str = 'DELETED'
        #
        # First, change the data of the form header.
        self._form_header.update_rec(
            imprint_line_long = repl_str,
            imprint_line_short = repl_str)
        #
        # Second, change the data of the tiff header.
        #
        # Note that we update only the first copy of the tiff structure (page 1).
        # The second copy will always remain unchanged, because the old software
        # ignores that. Therefore, the second copy is used to restore a "deleted"
        # scan.
        #
        # This line puts the new record data into the tiff header struc (in memory).
        self._th.long_data.update_rec(page_name = repl_str)
        # This line also patches the new data into the tiff structure (in memory).
        self._image_data.update_rec(codnr = repl_str)
        #
        # Upto here, no data was physically written.
        # We do that now. Actually, that should logically be a transaction,
        # but we ignore this for CDB/IBF.
        #
        # First, we actualize the form data:
        # - move the data in memory back to the structure on disk.
        self.write_back()
        #
        # Then we update the index info in the IBF:
        self.parent.ibf.update_entry(self._image_data)
        #
        # And as the last step, we also ask the tiff handler to write its data to disk.
        self._th.update()

    def is_deleted(self):
        return self._form_data.is_deleted()

    def is_dirty(self):
        return self._dirty_flag

    def set_dirty(self, flag=True):
        assert isinstance(flag, bool)
        self._dirty_flag = flag

    @property
    def pic_nr(self):
        return self.parent.batch.batch_form(self._form_index).pic()

    @property
    def fields(self):
        return self._form_data.fields

    @property
    def field_names(self):
        return self._form_data.field_names

    def write_back(self):
        '''
        Write the current data of the form back to disk
        (redirected from cdb_tool.Form)
        '''
        self._form_data.write_back()
