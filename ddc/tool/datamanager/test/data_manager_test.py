# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from pythonic_testcase import *
from srw.rdblib import DataBunch

from ddc.tool.datamanager import DataManager
from ddc.storage import db_schema, get_model
from ddc.storage.task import TaskType, TaskStatus
from ddc.storage.testhelpers import create_bunch


class DataManagerTest(PythonicTestCase):
    def test_raises_exception_when_adding_incomplete_bunches(self):
        first = DataBunch(cdb='foo.cdb', ibf='foo.ibf', db=None, ask=None)
        second = DataBunch(cdb='bar.cdb', ibf='bar.ibf', db='bar.db', ask=None)
        assert_false(first.is_complete(), message='bad test setup')
        assert_true(second.is_complete())

        data_mgr = DataManager()
        assert_raises(Exception, lambda: data_mgr.add_bunches((first, second)))
        assert_length(0, data_mgr.bunches)

    def test_can_return_batch_with_open_tasks_for_given_task_type(self):
        model = get_model(db_schema.LATEST)
        dummy_task = model.Task(1, 'dummy type', status=TaskStatus.NEW)
        closed_task = model.Task(0, TaskType.VERIFICATION, status=TaskStatus.CLOSED, data={'field_name': 'foo', 'errors': ()})
        verification_task = model.Task(1, TaskType.VERIFICATION, status=TaskStatus.NEW, data={'field_name': 'bar', 'errors': ()})

        first = create_bunch(nr_forms=1, tasks=(dummy_task, closed_task))
        second = create_bunch(nr_forms=2, tasks=(verification_task, ))
        assert_not_equals(first.cdb, second.cdb,
            message='bad test setup - can not tell the difference between CDB fixtures.')
        assert_not_equals(first.ibf, second.ibf,
            message='bad test setup - can not tell the difference between IBF fixtures.')

        data_mgr = DataManager()
        data_mgr.add_bunches((first, second))
        assert_none(data_mgr.find_batch_with_new_tasks('invalid'))

        first.db._new_session()
        second.db._new_session()
        verification_batch = data_mgr.find_batch_with_new_tasks(TaskType.VERIFICATION)
        assert_not_none(verification_batch,
            message='did not find batch with new verification tasks')

    def test_can_return_filenames_for_all_known_bunches(self):
        files = (
            '/foo/123.cdb', '/foo/img/345.ibf', '/bar/123.db', '/bar/123.ask',
            '/bar/ABC.cdb', '/tmp/img/DEF.ibf', '/srv/GHI.db', '/srv/GHI.ask',
        )
        first = DataBunch(files[0], files[1], files[2], files[3])
        second = DataBunch(files[4], files[5], files[6], files[7])
        data_mgr = DataManager()
        data_mgr.add_bunches((first, second))
        # sorted because I want to detected duplicates but don't care about
        # ordering
        assert_equals(sorted(files), sorted(data_mgr.all_files()))

    def test_can_remove_bunches(self):
        first = DataBunch(cdb='foo.cdb', ibf='foo.ibf', db='foo.db', ask=None)
        second = DataBunch(cdb='bar.cdb', ibf='bar.ibf', db='bar.db', ask=None)
        data_mgr = DataManager()
        data_mgr.add_bunches((first, second))
        assert_length(2, data_mgr.bunches)

        data_mgr.remove_bunch(second)
        assert_length(1, data_mgr.bunches)

        data_mgr.remove_bunches((first, ))
        assert_length(0, data_mgr.bunches)

    def test_raises_exception_when_trying_to_remove_unknown_bunch(self):
        bunch = DataBunch(cdb='foo.cdb', ibf='foo.ibf', db='foo.db',
                          ask='foo.ask')
        data_mgr = DataManager()
        assert_raises(ValueError, lambda: data_mgr.remove_bunch(bunch))

    def test_can_set_current_bunches(self):
        first = DataBunch(cdb='foo.cdb', ibf='foo.ibf', db='foo.db', ask='foo.ask')
        second = DataBunch(cdb='bar.cdb', ibf='bar.ibf', db='bar.db', ask='bar.ask')
        data_mgr = DataManager()
        data_mgr.add_bunch(first)
        assert_equals(sorted(first), sorted(data_mgr.all_files()))

        data_mgr.set_bunches([second])
        assert_equals(sorted(second), sorted(data_mgr.all_files()))
