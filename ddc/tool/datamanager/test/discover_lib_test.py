# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os
import shutil
from tempfile import mkdtemp

from pythonic_testcase import *
from srw.rdblib import ibf_subdir

from ddc.tool.datamanager.discover_lib import find_data_files


class FindDataFilesTest(PythonicTestCase):
    def setUp(self):
        super(FindDataFilesTest, self).setUp()
        self.tempdir = mkdtemp()

    def teardown(self):
        shutil.rmtree(self.tempdir)
        super(FindDataFilesTest, self).teardown()

    def test_can_find_cdb_ibf_and_sqlite_files_without_filesystem_layout_assumptions(self):
        subdir1 = os.path.join(self.tempdir, 'foo')
        subdir2 = os.path.join(subdir1, 'sqlite')
        os.makedirs(subdir2)

        cdb = self._filename('foobar.cdb')
        self._create_cdb(cdb)
        sqlite = os.path.join(subdir2, '12345.db')
        self._create_sqlite(sqlite)
        ibf = os.path.join(subdir1, 'baz.ibf')
        self._create_ibf(ibf)

        self.assert_find_data_files((cdb, ibf, sqlite))

    def test_preserves_filename_casing(self):
        file1 = self._filename('foo.cdb')
        file2 = self._filename('bar.CDB')
        cdb_files = (file1, file2)
        for filepath in cdb_files:
            self._create_cdb(filepath)

        self.assert_find_data_files(cdb_files)

    def test_can_limit_search_to_files_in_base_directory(self):
        subdir = os.path.join(self.tempdir, 'foo')
        os.makedirs(subdir)
        cdb = os.path.join(subdir, 'foobar.cdb')
        self._create_cdb(cdb)
        ibf = os.path.join(self.tempdir, 'baz.ibf')
        self._create_ibf(ibf)

        self.assert_find_data_files((cdb, ibf), recursive=True)
        self.assert_find_data_files((ibf, ), recursive=False)

    def test_non_recursive_search_still_looks_in_ibf_subdirectory(self):
        cdb = os.path.join(self.tempdir, 'foobar.cdb')
        self._create_cdb(cdb)
        ibf_path = os.path.join(self.tempdir, ibf_subdir)
        os.makedirs(ibf_path)
        ibf = os.path.join(ibf_path, 'foobar.ibf')
        self._create_ibf(ibf)

        self.assert_find_data_files((cdb, ibf), recursive=True)
        self.assert_find_data_files((cdb, ibf), recursive=False)

    # --- custom assertions ---------------------------------------------------
    def assert_find_data_files(self, filepaths, **kwargs):
        found_paths = self._find_data_files(**kwargs)
        # sorted because I want to detected duplicates but don't care about
        # ordering
        assert_equals(sorted(filepaths), sorted(found_paths))

    # --- internal helpers ----------------------------------------------------

    def _filename(self, name):
        return os.path.join(self.tempdir, name)

    def _find_data_files(self, **kwargs):
        return find_data_files(self.tempdir, **kwargs)

    def _create_file(self, filepath):
        with open(filepath, 'wb') as fp:
            fp.write(b'')
    # These methods are currently just aliases but later on we could create
    # files with correct contents easily if necessary.
    _create_cdb = _create_file
    _create_ibf = _create_file
    _create_sqlite = _create_file

