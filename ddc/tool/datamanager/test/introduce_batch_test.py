# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os

from pythonic_testcase import *
from srw.rdblib import guess_path, DataBunch, MMapFile
from srw.rdblib.cdb import create_cdb_with_dummy_data
from srw.rdblib.ibf import create_ibf

from ddc.client.config import ALL_FIELD_NAMES
from ddc.storage.testhelpers import use_tempdir
from ddc.tool.datamanager.introducer import create_and_validate_batch_from_bunch
from ddc.validation.testutil import build_context


class IntroduceBatchTest(PythonicTestCase):
    def test_can_validate_batch_with_preopened_formbatch(self):
        with use_tempdir() as env_dir:
            cdb_path = os.path.join(env_dir, 'foo.cdb')
            ibf_path = guess_path(cdb_path, type_='IBF')
            cdb_fp = create_cdb_with_dummy_data(nr_forms=1, filename=cdb_path, field_names=ALL_FIELD_NAMES)
            cdb_fp.close()
            create_ibf(nr_images=1, filename=ibf_path, create_directory=True)

            cdb_fp = MMapFile(cdb_path, access='write')
            bunch = DataBunch(cdb=cdb_fp, ibf=ibf_path, db=None, ask=None)
            context = build_context()

            # This also happens in the GUI where we check the CDB structure
            # first
            batch = create_and_validate_batch_from_bunch(bunch, context)
            assert_not_none(batch)
