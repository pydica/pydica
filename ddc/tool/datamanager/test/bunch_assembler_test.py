# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os

from pythonic_testcase import *
from srw.rdblib import DataBunch

from ddc.tool.datamanager.bunch_assembler import BunchAssembler

def fix_path(path):
    os_path = path.replace('/', os.path.sep)
    if os_path.startswith('\\'):
        # Windows: add drive letter for absolute paths without drive letter
        # qualification
        os_path = os.path.abspath(os_path)
    return os_path
# short-hand alias to keep the code as readable as possible
f = fix_path


class DiscoverLibAssembleBunchesTest(PythonicTestCase):
    def test_can_assemble_matching_files_into_bunches(self):
        bunch1 = DataBunch(cdb=f('/foo/bar.cdb'), ibf=f('/foo/00000001/bar.ibf'),
                           db=f('/foo/bar.db'), ask=None)
        bunch2 = DataBunch(cdb=f('/baz.cdb'), ibf=f('/00000001/baz.ibf'),
                           db=f('/baz.db'), ask=None)
        result = self._assemble_bunches(bunch1 + bunch2)
        assert_equals(set([bunch1, bunch2]), set(result.complete))
        assert_length(0, result.incomplete)
        assert_length(0, result.errors)

    def test_ignores_casing_when_assembling_bunches(self):
        bunch1 = DataBunch(cdb=f('/foo/bar.cDb'), ibf=f('/foo/00000001/bar.IBF'),
                           db=f('/foo/BaR.Db'), ask=None)
        # ensure that all components are treated case-insensitively
        assert_not_equals(bunch1.cdb.lower(), bunch1.cdb)
        assert_not_equals(bunch1.ibf.lower(), bunch1.ibf)
        assert_not_equals(bunch1.db.lower(), bunch1.db)

        result = self._assemble_bunches(bunch1)
        assert_equals((bunch1, ), result.complete)
        assert_length(0, result.incomplete)
        assert_length(0, result.errors)

    def test_can_also_return_information_about_incomplete_bunches(self):
        bunch1 = DataBunch(cdb=f('/foo/bar.cdb'), ibf=None,
                           db=f('/foo/bar.db'), ask=None)
        bunch2 = DataBunch(cdb=None, ibf=f('/00000001/baz.ibf'),
                           db=f('/baz.db'), ask=None)
        bunch3 = DataBunch(cdb=None, ibf=None,
                           db=f('/foo/baz.db'), ask=None)

        result = self._assemble_bunches(filter(None, bunch1+bunch2+bunch3))
        assert_length(0, result.complete)
        assert_equals(set([bunch1, bunch2, bunch3]), set(result.incomplete))
        assert_length(0, result.errors)

    def test_pays_attention_to_directories_while_assembling(self):
        bunch1 = DataBunch(cdb=f('/foo/bar.cdb'), ibf=f('/FOO/00000001/bar.ibf'),
                           db=f('/foo/bar.db'), ask=None)
        bunch2 = DataBunch(cdb=f('/foo.cdb'), ibf=f('/00000001/foo.ibf'),
                           db=None, ask=None)
        bunch3 = DataBunch(cdb=None, ibf=None,
                           db=f('/baz/foo.db'), ask=None)

        result = self._assemble_bunches(filter(None, bunch1+bunch2+bunch3))
        assert_equals((bunch1, ), result.complete)
        assert_equals(set([bunch2, bunch3]), set(result.incomplete))
        assert_length(0, result.errors)

    def test_returns_error_when_encountering_ambiguous_filenames(self):
        result = self._assemble_bunches((f('/FOO.cdb'), f('/foo.cdb')))
        assert_length(0, result.complete)
        assert_length(0, result.incomplete)
        assert_length(1, result.errors)
        error = result.errors[0]
        assert_contains(f('FOO.cdb'), error.text)
        assert_contains(f('foo.cdb'), error.text)

    def test_does_not_return_errors_about_ambiguous_filenames_for_different_directories(self):
        bunch1 = DataBunch(cdb=f('/foo/bar.cdb'), ibf=None, db=None, ask=None)
        bunch2 = DataBunch(cdb=f('/bar/bar.cdb'), ibf=None, db=None, ask=None)

        result = self._assemble_bunches(filter(None, bunch1+bunch2))
        assert_length(0, result.complete)
        assert_equals(set([bunch1, bunch2]), set(result.incomplete))
        assert_length(0, result.errors)

    def _assemble_bunches(self, filenames):
        return BunchAssembler(filenames).process_filenames()
