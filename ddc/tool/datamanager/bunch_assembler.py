# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os
from timeit import default_timer as timer

from srw.rdblib import guess_bunch_from_path, guess_path, DataBunch

from ddc.lib.attribute_dict import AttrDict
from ddc.lib.log_proxy import l_
from ddc.lib.result import Result
from . import discover_lib


__all__ = ['bunch_from_cdb_name', 'BunchAssembler']


def build_case_insensitive_filemap(filenames):
    """
    Some filesystems are case-sensitive (notably most "Linux" filesystems).
    On the other hand some code might treat file names case-insensitively
    (and this assumption is fine for Windows and Mac OS).
    This method builds a case-insensitive mapping based on the given file
    names so we can treat file names as case-insensitive even on Linux.

    For maximum robustness we'll detect ambiguous file names (two files with
    the same name but different casing) and return these as errors which
    should be handled in a higher layer.
    """
    fn_lookup = dict()
    errors = []
    duplicate_names = set()
    for name in filenames:
        if not name:
            continue
        normalized_name = name.lower()
        if normalized_name in fn_lookup:
            known_name = os.path.basename(fn_lookup[normalized_name])
            new_name = os.path.basename(name)
            error = AttrDict(
                text='Ambiguous filenames (%s, %s) detected' % (known_name, new_name),
                filename=name
            )
            errors.append(error)
            duplicate_names.add(normalized_name)
            continue
        fn_lookup[normalized_name] = name
    for name in duplicate_names:
        del fn_lookup[name]
    return Result(fn_lookup, errors=errors)


def guess_bunch_from_cdb_name(cdb_filename, require_db=True):
    files = {'cdb': cdb_filename}
    for type_ in ('ibf', 'db', 'ask'):
        filename = guess_path(cdb_filename, type_)
        files[type_] = filename if os.path.exists(filename) else None
    bunch = DataBunch(**files)
    if (bunch.cdb is None) or (bunch.ibf is None):
        return None
    if require_db and (bunch.db is None):
        return None
    return bunch

def bunch_from_cdb_name(cdb_filename, log=None):
    log = l_(log)
    start = timer()
    # discover_lib.find_data_files() might take quite a while as it scans the
    # complete directory structure. In most cases we can use a fast path as the
    # names of the IBF/SQLite components can be derived from the CDB name without
    # any casing issues. We use that to implement a "fast path" (which takes
    # only a few milliseconds as opposed to 2-3 seconds).
    bunch = guess_bunch_from_cdb_name(cdb_filename, require_db=False)
    if bunch is None:
        log.debug('unable to assemble bunch based on filenames alone (fast path), need to scan for available files.')
        filenames = discover_lib.find_data_files(os.path.dirname(cdb_filename))
        filemap = build_case_insensitive_filemap(filenames)
        assert len(filemap.errors) == 0
        bunch = guess_bunch_from_path(cdb_filename, filemap.value)
    else:
        log.debug('bunch assembled only based on filenames (fast path)')
    duration = timer() - start
    log.debug('assembling bunch for "%s" took %.5f seconds', cdb_filename, duration)
    return bunch


class BunchAssembler(object):
    def __init__(self, filenames):
        self.complete = []
        self.incomplete = []
        filemap = build_case_insensitive_filemap(filenames)
        self._fn_lookup = filemap.value
        self.errors = filemap.errors
        good_filenames = self._fn_lookup.values()
        self._cdbs = list(filter(lambda f: f and f.lower().endswith('.cdb'), good_filenames))
        self._ibfs = list(filter(lambda f: f and f.lower().endswith('.ibf'), good_filenames))
        self._db = list(filter(lambda f: f and f.lower().endswith('.db'), good_filenames))
        self._asks = list(filter(lambda f: f and f.lower().endswith('.ask'), good_filenames))

    def process_filenames(self):
        self._assemble_bunches_from(self._cdbs)
        self._assemble_bunches_from(self._ibfs)
        self._assemble_bunches_from(self._db)
        self._assemble_bunches_from(self._asks)

        assert len(self._cdbs) == 0
        assert len(self._ibfs) == 0
        assert len(self._db) == 0
        assert len(self._asks) == 0
        return AttrDict(
            complete=tuple(self.complete),
            incomplete=tuple(self.incomplete),
            errors=tuple(self.errors),
        )

    def _assemble_bunches_from(self, base_list):
        for path in tuple(base_list):
            bunch = guess_bunch_from_path(path, self._fn_lookup)
            self._remove_assembled_files_from_pending_lists(bunch)
            if bunch.is_complete():
                self.complete.append(bunch)
            else:
                self.incomplete.append(bunch)

    def _remove_assembled_files_from_pending_lists(self, bunch):
        if bunch.cdb is not None:
            self._cdbs.remove(bunch.cdb)
        if bunch.ibf is not None:
            self._ibfs.remove(bunch.ibf)
        if bunch.db is not None:
            self._db.remove(bunch.db)
        if bunch.ask is not None:
            self._asks.remove(bunch.ask)

