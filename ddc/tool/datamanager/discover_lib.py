# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os

from pathtools.patterns import match_path
try:
    import scandir
except ImportError:
    scandir = None

from srw.rdblib import ibf_subdir

__all__ = ['find_data_files', 'list_files']

def list_files(dir_pathname, recursive=True):
    # scandir is only included in Python 3.5 and (as of 05/2016) I did not find
    # any Windows scandir modules for Python 3.3. Having a non-optimized
    # fallback avoids a hard dependency on scandir.
    if scandir is None or True:
        for name in os.listdir(dir_pathname):
            path = os.path.join(dir_pathname, name)
            if os.path.isfile(path):
                yield os.path.abspath(path)
            elif recursive and os.path.isdir(path):
                for filename in list_files(path, recursive=recursive):
                    yield filename
        return

    for entry in scandir.scandir(dir_pathname):
        if entry.is_file():
            yield os.path.abspath(entry.path)
        elif recursive and entry.is_dir():
            for filename in list_files(entry.path, recursive=recursive):
                yield filename

def find_data_files(root_path, recursive=True):
    data_files = []
    for filepath in list_files(root_path, recursive=recursive):
        if not match_path(filepath, ('*.cdb', '*.ibf', '*.db', '*.ask'),
                          case_sensitive=False):
            continue
        data_files.append(filepath)
    # even if we don't want to find files recursively we should still find ibf
    # files in the '00000001' subfolder.
    ibf_dir = os.path.join(root_path, ibf_subdir)
    if not recursive and os.path.exists(ibf_dir):
        for filepath in list_files(ibf_dir, recursive=False):
            if not match_path(filepath, ('*.ibf', '*.ask'), case_sensitive=False):
                continue
            data_files.append(filepath)
    return tuple(data_files)

