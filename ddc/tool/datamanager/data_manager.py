# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from ddc.storage.batch import Batch


__all__ = ['DataManager']

class DataManager(object):
    """
    The DataManager is the central part of managing data batches (triplets
    of CDB, IBF, SQLite).
    It can find tasks anywhere in the pool of managed data triplets and manage
    locks for data being worked on to rule out concurrency issues.

    The DataManager only takes care of *complete* data triplets and batches but
    does not do any discovery by itself nor does it handle incomplete bunches
    (e.g. no SQLite DB or error conditions like IBF without CDB).

    In previous design discussions the functionality of this class was referred
    to as "task manager".
    """
    def __init__(self):
        self.bunches = []

    def add_bunch(self, bunch):
        if not bunch.is_complete():
            raise ValueError('DataManager can only work with complete bunches but not %r' % (bunch,))
        self.bunches.append(bunch)

    def add_bunches(self, bunches):
        for bunch in bunches:
            self.add_bunch(bunch)

    def set_bunches(self, bunches):
        # LATER: raise error if there is an active batch for a removed bunch
        #        (also check if one of these batches is locked)
        # empty the old self.bunches list but keep the instance (I assume that
        # this might prevent future bugs when someone still has a reference to
        # '.bunches'.
        for i in range(len(self.bunches)):
            self.bunches.pop()
        for bunch in bunches:
            self.add_bunch(bunch)

    def remove_bunch(self, bunch):
        self.bunches.remove(bunch)

    def remove_bunches(self, bunches):
        for bunch in bunches:
            self.remove_bunch(bunch)

    def all_files(self):
        files = []
        for bunch in self.bunches:
            files.extend(bunch)
        return tuple(files)

    def find_batch_with_new_tasks(self, task_type, lock_batch=True):
        """
        Look for a batch which is not locked and contains at least one open
        task of the specified task type. The batch will be locked if lock_batch
        is True.

        Returns None if no such batch could be found (consider scanning for
        new/updated batches).
        """
        assert lock_batch == True, 'Currently locking is not implemented - ensuring safe defaults'
        for bunch in self.bunches:
            batch = Batch.init_from_bunch(bunch, create_persistent_db=False)
            tasks = batch.new_tasks(task_type)
            if not tasks:
                batch.close()
                continue
            return batch
        return None

    # --- internal API -------------------------------------------------------

