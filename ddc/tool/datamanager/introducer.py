#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import argparse
import logging
import os
import sys
import time

from srw.rdblib import get_path_from_instance

# do not use relative imports because this script can be run directly from the
# command line without setuptools.
from ddc.config import init_pydica
from ddc.lib.log_proxy import l_
from ddc.tool.datamanager.state_keeper import StateKeeper
from ddc.storage import Batch, TaskStatus, TaskType


__all__ = ['Introducer']


def create_and_validate_batch_from_bunch(bunch, context, log=None, progress=None):
    # avoid recursive imports
    # LATER: move this introducer functionality into ddc.storage.paths
    from ddc.validation import validation_module_for_batch
    is_sqlite_db_missing = (bunch.db is None)
    cdb_filename = get_path_from_instance(bunch.cdb)
    is_rdb = (cdb_filename.upper().endswith('.RDB'))
    create_db = is_rdb and is_sqlite_db_missing
    if create_db and progress:
        is_active = progress.update(0, details='Initialisiere Datenbank')
        if is_active == False:
            return None
    batch = Batch.init_from_bunch(bunch, create_persistent_db=create_db, log=log)
    validator = validation_module_for_batch(batch, bunch, context)
    introduce_batch(batch, validator, progress=progress)
    return batch


def introduce_batch(batch, validator, *, progress=None):
    nr_validation_tasks = len(batch.tasks(type_=TaskType.FORM_VALIDATION))
    nr_verification_tasks = len(batch.tasks(type_=TaskType.VERIFICATION))
    if (nr_validation_tasks > 0) or (nr_verification_tasks > 0):
        return
    for i, form_ in enumerate(batch.forms()):
        if form_.is_deleted():
            continue
        batch.db_form(i).add_task(type_=TaskType.FORM_VALIDATION, status=TaskStatus.NEW)
    progress_factory = None
    if progress is not None:
        def progress_wrapper(max_value):
            progress.set_max_value(max_value)
            return progress
        is_active = progress.update(0, details='Validiere Formulardaten')
        if is_active == False:
            return None
        progress_factory = progress_wrapper
    validator.process_tasks(progress_factory=progress_factory)
    if progress and progress.is_cancelled:
        return
    batch.commit()


class Introducer(object):
    def __init__(self, root_path, log=None):
        self.log = l_(log)
        self.state_keeper = StateKeeper(root_path, log=log)
        self.state_keeper._listeners.append(self)

    def start(self):
        """Create a SQLite databases with validation tasks for new CDB files and
        watch the data directory for changes."""
        self.find_data_files()
        self.state_keeper.start_monitoring()

    def stop(self):
        self.state_keeper.stop_monitoring()

    def find_data_files(self):
        self.state_keeper.force_state_refresh()

    # --- event handlers for watchdog -----------------------------------------
    def add_initial_data(self, databunch):
        assert databunch.db is None
        batch = Batch.init_from_bunch(databunch, create_persistent_db=True)
        introduce_batch(batch, log=self.log)
        batch.commit()
        cdb_name = os.path.basename(databunch.cdb)
        self.log.info('new data file: %r', cdb_name)

    def on_incomplete_bunch(self, bunch):
        if bunch.db is not None: # either CDB or IBF are missing
            sys.stderr.write('Found incomplete data while grouping files!\n')
            sys.stderr.write('%s\n' % (bunch,))
            sys.exit(11)
        elif bunch.ibf is None: # only cdb
            sys.stderr.write('Found CDB without IBF file while grouping files!\n')
            sys.stderr.write('%s\n' % (bunch,))
            sys.exit(12)
        elif bunch.cdb is None:
            assert bunch.db is None
            assert bunch.ibf is not None
            # very likely we just have a RDB file here - just skip that one
            return
        assert (bunch.db is None)
        assert (bunch.cdb is not None) and (bunch.ibf is not None)
        self.add_initial_data(bunch)


def run_introducer(config_path, once=False):
    pydica_context = init_pydica(['introducer'], 'no usage yet', config_path=config_path)
    root_path = pydica_context.settings['datadir']
    if not os.path.isdir(root_path):
        raise ValueError("path not found: %r" % root_path)
    log = logging.getLogger('pydica.introducer')
    introducer = Introducer(root_path, log=log)
    if once:
        introducer.find_data_files()
        return
    introducer.start()
    try:
        while True:
            time.sleep(0.4)
    except KeyboardInterrupt:
        pass
    finally:
        introducer.stop()

def scan_and_init():
    parser = argparse.ArgumentParser()
    parser.add_argument('config')
    parser.add_argument('--once',
        dest='once',
        help='exit after scanning the root path once (no monitoring)',
        action='store_true', default=False
    )
    args = parser.parse_args()
    run_introducer(args.config, once=args.once)

if __name__ == '__main__':
    scan_and_init()
