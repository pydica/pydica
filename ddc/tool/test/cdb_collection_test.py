# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os

from ddt import ddt as DataDrivenTestCase, data
from pythonic_testcase import *

import ddc
from ddc.tool.cdb_collection import default_cdb_filename, CDB_Collection
from ddc.storage.testhelpers import temporary_copy


DATABASE_PATH = os.path.join(ddc.rootpath, 'private', 'srw', 'data')
DEMO_SEQ = os.path.join(DATABASE_PATH, '00955412.CDB')

@DataDrivenTestCase
class CDB_CollectionTest(PythonicTestCase):
    # trivial test for now to check the imports
    @data('write', 'copy')
    def test_open_sequence(self, access):
        with temporary_copy(default_cdb_filename()) as cdb_filename:
            seq = CDB_Collection.from_cdb_filename(cdb_filename, access=access)
            seq.close()

    @data('write', 'copy')
    def test_delete(self, access):
        # This test may modify data files so tests become unstable in case the test
        # does not complete successfully. Copy everything to a temporary location
        # to prevent this.
        with temporary_copy(DEMO_SEQ) as cdb_path:
            seq = CDB_Collection.from_cdb_filename(cdb_path, access=access)
            seq.seek(1)
            form = seq.current_form
            form.delete()
            seq.seek(0)
            form = seq.current_form
            form.undelete()
            seq.seek(1)
            form = seq.current_form
            assert_true(form.is_deleted())

            seq.close()

            seq = CDB_Collection.from_cdb_filename(cdb_path, access=access)
            forms = seq.forms
            # direct interface
            forms[1].delete()
            assert_true(forms[1].is_deleted())
            codnr = forms[1].pic_nr
            assert_not_equals('DELETED', codnr)
            assert_equals('DELETED', forms[1]._form_header.rec.imprint_line_short)

            forms[0].delete()
            assert_true(forms[0].is_deleted())
            codnr = forms[0].pic_nr
            assert_equals('DELETED', forms[0]._form_header.rec.imprint_line_short)
            seq.close()

    @data('write', 'copy')
    def test_undelete(self, access):
        with temporary_copy(DEMO_SEQ) as cdb_path:
            seq = CDB_Collection.from_cdb_filename(cdb_path, access=access)
            seq.seek(1)
            form = seq.current_form
            form.delete()
            assert_true(form.is_deleted())
            form.undelete()
            assert_false(form.is_deleted())
            seq.close()
