# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from functools import partial
import os
import sys

from pythonic_testcase import *

import ddc
from ddc.config.pyside_fix import QtCore
from ddc.tool.cdb_collection import CDB_Collection
from ddc.tool.remote_control import (COMMAND_FN, HANDSHAKE_FN,
     CDB_Reader, ConfigError, InvalidCmdError, RemoteControl)
from ddc.storage.testhelpers import temporary_copy
from ddc.validation.testutil import build_context


DEFAULT_IDLE = 5
DEBUG = True

prefixer = lambda p: os.path.join(ddc.rootpath, p)
TESTPATHS = tuple(map(prefixer, (
    'ddc/tool/test/data/form_batch/00035100.CDB',
    'private/srw/data/00099201.CDB',
    os.path.join('private/srw/data', '..', 'data', '00099201.CDB'),
)))

class TestRemote(PythonicTestCase):
    def setUp(self):
        self._result = None
        self.testpath = os.path.join(ddc.rootpath, 'ddc/tool/test/data')
        if not os.path.exists(self.testpath):
            os.makedirs(self.testpath)
        self.cmd_fn = os.path.join(self.testpath, COMMAND_FN)
        self.hsh_fn = os.path.join(self.testpath, HANDSHAKE_FN)
        app = QtCore.QCoreApplication.instance()
        if not app:
            app = QtCore.QCoreApplication([])
        self.app = app
        self.idle_expect = DEFAULT_IDLE
        self.debug = DEBUG

        self.testpaths = list(TESTPATHS)
        # we now have
        # - demo path (short)
        # - real path (300)
        # - fake path (== real)
        self.setup_remote_control()

    def setup_remote_control(self):
        for fn in self.cmd_fn, self.hsh_fn:
            if os.path.exists(fn):
                os.remove(fn)
        context = build_context()
        try:
            self.rem = RemoteControl(context, watch_path=self.testpath, debug=True)
        except ConfigError:
            # set it up, but don't barf here
            self.rem = None

    def tearDown(self):
        if self.rem:
            self.rem.stop()
            del self.rem

    def _log(self, *args, **kw):
        if self.debug:
            for v in args:
                if isinstance(v, Exception):
                    print(repr(args))
            else:
                print(*args, **kw)
            sys.stdout.flush()

    @property
    def have_result(self):
        return (getattr(self, '_result', None) is not None)

    def fetch_result(self):
        result = self._result
        self._result = None
        if isinstance(result, Exception):
            raise result
        return result

    def emulate_eventloop(self, timeout=500):
        interval = 50
        timer = QtCore.QTimer()
        timer.setInterval(interval)
        timer.setSingleShot(True)
        timer.timeout.connect(self.app.quit)
        self._log('LOOP', self)
        self.error_func = self.expect_error()
        self.rem.got_an_error.connect(self.error_func)
        self.result_func = self.expect_result()
        self.rem.got_opendel_cmd.connect(self.result_func)
        self.rem.got_openasc_cmd.connect(self.result_func)

        for i in range(timeout // interval):
            if self.have_result:
                break
            if self.rem.idle_count >= self.idle_expect:
                break
            timer.start()
            self.app.exec_()

    def expect_error(self):
        ''' support for cell variables is broken, so we use explicit binding '''
        @QtCore.Slot(object, Exception)
        def error_func(self, e):
            self._log('got:', e)
            assert_isinstance(e, Exception)
            self._result = e
            self._log('MYSELF', self)
        self._log('EXPECT', self)
        return partial(error_func, self)

    def expect_result(self):
        ''' support for cell variables is broken, so we use explicit binding '''
        @QtCore.Slot(object, str, int, str)
        def result_func(self, *args):
            self._log('got:', args)
            assert_false(isinstance(args, Exception))
            self._result = args
            self._log('MYSELF', self)
        self._log('EXPECT', self)
        return partial(result_func, self)


#------------------------------------
# now the tests
#
    def test_remote_setup(self):
        context = build_context()
        rem = RemoteControl(watch_path=self.testpath, context=context)
        wrong = self.testpath + 'xxx'
        assert_false(os.path.exists(wrong))
        with assert_raises(ConfigError):
            RemoteControl(watch_path=wrong, context=context)

    def test_remote_idle(self):
        rem = self.rem
        # to see this, use
        # bin/python -m pytest . -k remote -v -s
        rem.start()
        self.emulate_eventloop()
        assert_equals(self.idle_expect, rem.idle_count)

    def test_remote_missing_cmdfile(self):
        rem = self.rem
        rem.start()
        self._create_file(self.hsh_fn)
        self._log('TEST', self)
        self.emulate_eventloop()
        with assert_raises(ConfigError):
            assert self.have_result
            assert_isinstance(self._result, Exception)
            self._log(self._result)
            self.fetch_result()

    def test_remote_open_cmd_error(self):
        for i, (wrongarg, exc) in enumerate( (
            ('blurb ', InvalidCmdError),
            ('blurb crap', InvalidCmdError),
            ('opENdel crap', SyntaxError),
            ('openASC too;few', SyntaxError),
            ('OpEndel too;many;things;here', SyntaxError)
            )
        ):
            rem = self.rem
            rem.start()
            with open(self.cmd_fn, 'w') as f:
                f.write(wrongarg)
            self._create_file(self.hsh_fn)
            self._log('TEST', self)
            self.emulate_eventloop()
            with assert_raises(exc):
                self._log('should get', i, exc)
                self._log(self._result)
                self.fetch_result()

    def test_remote_open_cmd_ok(self):
        rem = self.rem
        rem.start()
        with open(self.cmd_fn, 'w') as f:
            f.write('opendel somefile;42;4711')
        self._create_file(self.hsh_fn)
        self.emulate_eventloop()
        self._log(self._result)
        self.fetch_result()

    def _create_file(self, path):
        with open(path, 'w') as fp:
            fp.write('\n')

    def test_remote_open_real_ok(self):
        for path in self.testpaths:
            self._create_file(self.hsh_fn)
            rem = self.rem
            rem.start()
            with open(self.cmd_fn, 'w') as f:
                f.write('openasc {};42;4711'.format(self.testpath))
            self._create_file(self.hsh_fn)
            self.emulate_eventloop()
            self._log(self._result)
            self.fetch_result()
            assert(os.path.isfile(path))

    def test_cdb_open_real_ok(self):
        reader = CDB_Reader(build_context())
        for cdb_path in self.testpaths:
            with temporary_copy(cdb_path) as path:
                rem = self.rem
                rem.start()
                with open(self.cmd_fn, 'w') as f:
                    f.write('openasc {};42;4711'.format(path))
                self._create_file(self.hsh_fn)
                self.emulate_eventloop()
                self._log(self._result)
                self.fetch_result()
                assert(os.path.isfile(path))

                # we now have the file.
                # try to read the first record!
                testseq = CDB_Collection.from_cdb_filename(fname=path)
                pic_nr = testseq.current_form.pic_nr
                testseq.close()
                # call the new function
                res = reader.opendel_cmd(path, 0, pic_nr)
                reader.close_cmd()
                assert_equals(0, res)
                testseq = CDB_Collection.from_cdb_filename(fname=path)
                testseq.seek(1)
                pic_nr = testseq.current_form.pic_nr
                testseq.close()
                res = reader.opendel_cmd(path, 0, pic_nr)
                reader.close_cmd()
                assert_equals(1, res)


    def test_reader_connections(self):
        '''
        This is a special test to check which signals are connected
        with which signatures. We want to be able to check automatically
        if a certain signal has got a connection. This way we can show a message
        pop-up if a signal is not handled!
        '''
        def dummy(self, *args, **kw):
            pass
        reader = CDB_Reader(build_context())
        # note: we need to use 'str' here, because python 2.7 fails with unicode.
        # but I don't change the header!
        assert_equals(0, reader.receivers(str("2signal_goto(int)")))
        reader.signal_goto.connect(dummy)
        assert_equals(1, reader.receivers(str("2signal_goto(int)")))
        assert_equals(0, reader.receivers(str("2signal_open(PyObject, int)")))
        reader.signal_open.connect(dummy)
        assert_equals(1, reader.receivers(str("2signal_open(PyObject, int)")))
        assert_equals(0, reader.receivers(str("2signal_error(PyObject)")))
        reader.signal_error.connect(dummy)
        assert_equals(1, reader.receivers(str("2signal_error(PyObject)")))


    # the actual pic searching algorithm is now tested in "pic_search_test" so we
    # just need to check that OPENDEL and OPENASC work.
    def test_open_del_asc(self):
        # path for issue#7
        p = os.path.join(ddc.rootpath, 'private/srw/data/issue/7/00099102.CDB')

        if not os.path.isfile(p):
            msg = 'private path not found'
            self._log(1000*'X', msg)
            self.skipTest(msg)

        reader = CDB_Reader(build_context())

        # this tests the correct behavior without the remote control
        reader.opendel_cmd(p, 268-1, '30409900600024')
        form = reader.seq.current_form
        # When this test was written it was assumed that the OPENDEL command should
        # always return the first form in a batch for a given PIC (regardless of
        # its deletion state). However it turned out in July 2016 that we should
        # prefer non-deleted forms over deleted ones.
        # Hence the result of OPENDEL and OPENASC is the same in this test.
        #
        # Luckily there is a more comprehensive test (without any private date) for
        # the PIC searching functionality in "pic_search_test".
        assert_equals(268, form._form_index)
        assert_false(form.is_deleted())

        # now the same with openasc
        reader.openasc_cmd(p, 268-1, '30409900600024')
        form = reader.seq.current_form
        assert_false(form.is_deleted())
        assert_equals(268, form._form_index)
