#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from unittest.mock import patch, Mock, MagicMock

from pythonic_testcase import *


class DemoForm(object):
    pass

class DemoFormBatch(object):

    def load_forms(self):
        self.forms = []
        offset = self.form_batch_header.record_size
        len_job_filecontent = len(self.filecontent)
        while offset < len_job_filecontent:
            prescription = DemoForm(self, offset)
            self.forms.append(prescription)
            offset += prescription.record_size
            if len(self.forms) > len(self):
                raise ValueError('prescription count exceeds header info')
        if len(self.forms) != len(self):
            raise ValueError("read prescription count differs from header info")

    def __len__(self):
        return self.count()

    def __eq__(self, other):
        return (self.form_batch_header == other.form_batch_header and
                self.forms == other.forms)

    def __ne__(self, other):
        return not(self == other)

    def __hash__(self):
        return super(DemoFormBatch, self).__hash__()

    # hi Michael
    # uncomment this line to make the test crash
    #__hash__ = None


class DemoFormBatchHeader(object):
    pass

class TestFormBatchMethods(PythonicTestCase):

    def setUp(self):
        patcher = patch.object(DemoFormBatch, '__init__', return_value=None)
        self.form_batch_init_mock = patcher.start()
        self.addCleanup(patcher.stop)

        patcher = patch.object(DemoFormBatchHeader, '__init__',
                               return_value=None)
        self.form_batch_header_init_mock = patcher.start()
        self.addCleanup(patcher.stop)

    def test_load_forms_raises_if_batch_header_image_count_is_higher(self):
        # this is the intended test which works with a defined __hash__
        batch_header_record_size = 2
        form_count = 4
        form_record_size = 5
        filecontent = b'12345678901234567'

        form_batch = self._create_test_form_batch(
            batch_header_record_size, form_count,
            form_record_size, filecontent)

        assert_raises(ValueError, form_batch.load_forms)

    def _create_test_form_batch(self,
                                batch_header_record_size,
                                form_count,
                                form_record_size,
                                filecontent):
        patcher = patch.object(DemoFormBatch, '__len__',
                               return_value=form_count)
        form_batch_len_mock = patcher.start()
        self.addCleanup(patcher.stop)

        patcher = patch('ddc.tool.test.mock_test.DemoForm',
                        new=FormMock)
        form_mock = patcher.start()
        self.addCleanup(patcher.stop)

        form_batch = DemoFormBatch()
        form_batch.form_batch_header = (
            Mock(record_size=batch_header_record_size))
        form_batch.filecontent = filecontent

        return form_batch

class FormMock(MagicMock):

    def __init__(self, *args, **kwargs):
        MagicMock.__init__(self, args, kwargs)
        self.record_size = 5


