# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os

from pythonic_testcase import *
from srw.rdblib import FormBatch

import ddc
from ddc.client.config import ALL_FIELD_NAMES



def test_form_batch_equality_operator():
    # same batches should be equal
    cdb_path = os.path.join(ddc.rootpath, 'ddc/tool/test/data/form_batch/00035100.CDB')
    form_batch_1 = FormBatch(cdb_path, access='read', field_names=ALL_FIELD_NAMES)
    form_batch_2 = FormBatch(cdb_path, access='read', field_names=ALL_FIELD_NAMES)
    assert_equals(form_batch_1, form_batch_2)

    # change form count - batches are not equal
    batch_header_1 = form_batch_1.form_batch_header
    changed_form_count = batch_header_1.rec.form_count + 1
    batch_header_1.rec = batch_header_1.rec._replace(form_count=changed_form_count)
    assert_not_equals(form_batch_1, form_batch_2)

    # changed form count back to the original value - should be equal again
    changed_form_count -= 1
    batch_header_1.rec = batch_header_1.rec._replace(form_count=changed_form_count)
    assert_equals(form_batch_1, form_batch_2)


