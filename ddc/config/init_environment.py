# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from logging.config import fileConfig
import os
import re
import sys

from ..lib.attribute_dict import AttrDict
from ..lib.log_proxy import get_logger
from ..lib.multiconfig import SiteConfig
from ..plugin.pydica_plugins import init_plugin_loader


__all__ = ['init_environment', 'init_pydica']

def load_configuration(config_files, here_dir=None):
    config_file = config_files[0]
    local_config = config_files[1:]
    if here_dir is None:
        here_dir = os.path.dirname(config_file)
    site_config = SiteConfig(mandatory=(config_file, ), optional=local_config)
    config_files = [config_file] if os.path.exists(config_file) else []
    config_files.extend(local_config)
    site_settings = site_config.sections_as_dicts(interpolations={'here': here_dir})
    pydica_settings = site_settings.get('pydica', {})
    pydica_settings.setdefault('env_dir', here_dir)
    site_settings['pydica'] = pydica_settings
    return site_settings

def initialize_logging(pydica_settings):
    """
    Initialize logging configuration by loading the specified log file from
    pydica_settings (key 'logging_conf').
    Return True if the config file exists and logging was configured
    successfully (else False).
    """
    logging_conf = pydica_settings.get('logging_conf')
    if (logging_conf is None) or (not os.path.exists(logging_conf)):
        return False
    fileConfig(logging_conf, disable_existing_loggers=True)
    return True

def init_environment(site_config, log=True):
    """
    setting up the "environment" so that pydica-based scripts can run and
    returns the "context".
    "Set up" is mostly about enabling and loading activated plugins.
    "Context" contains information about the setup which can be accessed by
    various pydica components.
    """
    pydica_settings = site_config['pydica']
    _get_logger = lambda name, **kwargs: get_logger(name, log=log, **kwargs)
    env_dir = os.path.expanduser(pydica_settings['env_dir'])
    enabled_plugins = re.split('\s*,\s*', pydica_settings.get('enabled_plugins', '*'))
    loader_log = _get_logger('pydica.startup.plugins')
    plugin_loader = init_plugin_loader(enabled_plugins=enabled_plugins, log=loader_log)
    plugin_loader.init()
    context = AttrDict({
        'config': site_config,
        'env_dir': env_dir,
        'plugin_manager': plugin_loader,
        'settings': pydica_settings,
        'get_logger': _get_logger,
        'locale': 'de', # German error messages from pycerberus
    })
    plugin_loader.initialize_plugins(context)
    return context

def find_pydica_config_file(argv=None):
    """
    Try to find a configuration file which contains all required settings to
    run a pydica tool (e.g. the GUI validator, remote control, stand-alone
    introducer).

    This can either be a regular file with a name ending in '.ini' or '.cfg',
    or a symlink with a name ending in '.lnk'. By default the code looks for a
    file named 'config.lnk' in the project directory.

    Returns the file path or None if no file was found.
    """
    import ddc
    if len(argv) >= 2:
        fpath = os.path.abspath(argv[1])
    else:
        fpath = os.path.join(ddc.rootpath, 'config.lnk')
    if not os.path.exists(fpath):
        return None
    elif fpath.endswith('.lnk') and not os.path.islink(fpath):
        return None
    elif not os.path.isfile(fpath):
        return None

    if os.path.islink(fpath):
        # note: we stay in the current directory.
        # (only checking, but not jumping by target_name=name)
        target_name = os.readlink(fpath)
        target_ext = os.path.splitext(target_name)[1].lower()
        if target_ext not in ('ini', 'cfg'):
            return None
    return fpath

def find_pydica_local_configs(config_file):
    config_path = os.path.abspath(config_file)
    config_dir = os.path.dirname(config_path)
    for ini_ending in ('ini', 'cfg'):
        local_config_path = os.path.join(config_dir, 'local.'+ini_ending)
        if os.path.exists(local_config_path):
            return (local_config_path, )
    return ()

def init_pydica(argv, usage, config_path=None):
    """
    Primary startup helper for pydica: load settings, configure logging,
    initialize plugins.

    Returns a pydica context after successful initialization. In case of fatal
    errors (e.g. non-existent primary configuration file) a message will be
    printed and the process is terminated.

    This function is built as convenience helper for interactive scripts and
    programs to unify the setup logic.
    """
    if config_path is None:
        config_path = find_pydica_config_file(argv)
        if not config_path:
            print(usage, file=sys.stderr)
            sys.exit(10)
    print('*** starting %s config=%s' % (argv[0], config_path))
    local_overrides = find_pydica_local_configs(config_path)
    if local_overrides:
        print('+++ local overrides from %s' % (', '.join(local_overrides)))
    config_files = (config_path, ) + local_overrides
    site_settings = load_configuration(config_files)
    pydica_settings = site_settings['pydica']
    is_logging_initialized = initialize_logging(pydica_settings)
    if not is_logging_initialized:
        logging_conf = pydica_settings['logging_conf']
        msg = 'unable to find logging configuration in file %s' % logging_conf
        print(msg, file=sys.stderr)
        sys.exit(11)
    pydica_context = init_environment(site_settings, log=is_logging_initialized)
    log = pydica_context.get_logger('pydica.startup')
    log.info('starting %s with config from %s', argv[0], config_path)
    if local_overrides:
        log.info('overriding with local configuration from %s', ', '.join(local_overrides))
    return pydica_context
