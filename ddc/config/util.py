# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals


__all__ = ['parse_stringbool']

def parse_stringbool(str_value):
    if str_value is None:
        return False
    lower_value = str_value.lower()
    if lower_value in ('false', 'f', '0'):
        return False
    elif lower_value in ('true', 't', '1'):
        return True
    raise ValueError('Unknown boolean value %r' % str_value)
