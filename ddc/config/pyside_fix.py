# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

"""
PySide bootstrap.

This module tried to support PyQT and PySide, but the differences are
to much. The PyQT version is therefore dropped. (2012-12-06)

We now use PySide explicitly and ensure that loading an image always works.

@Daniel:
The functionality test is automatically performed at import time.
We can change that if you like an explicit call better.
"""

import os, sys, ddc

if sys.platform == 'darwin':
    try:
        import PySide.QtCore
    except ImportError:
        if hasattr(sys, 'real_prefix'):
            # virtualenv: trying to borrow from the python installation
            try:
                origpath = sys.path[:]
                site_p = os.path.realpath(os.path.join(sys.real_prefix,
                                      'lib/python{major}.{minor}/site-packages'
                                      .format(major=sys.version_info.major,
                                              minor=sys.version_info.minor)))
                if not os.path.exists(os.path.join(site_p, 'PySide')):
                    # maybe it is in an .egg (dir or zip)
                    candidates = os.listdir(site_p)
                    eggs = [entry for entry in candidates
                            if entry.startswith('PySide-')
                            and entry.endswith('.egg') ]
                    # must be a single entry
                    if len(eggs) == 1:
                        site_p = os.path.join(site_p, eggs[0])
                print('trying to borrow from ' + site_p)
                sys.path.append(site_p)
                import PySide.QtCore
            except ImportError:
                raise
            finally:
                # undo the change to sys.path
                sys.path[:] = origpath
        else:
            raise

import PySide
from PySide import QtCore, QtGui

# fix the plugins path if we are using a virtualenv setup
def adjust_pluginpath():
    ppath = os.path.join(os.path.dirname(QtCore.__file__), 'plugins')
    if os.path.isdir(ppath):
        libpaths = []
        for lp in QtCore.QCoreApplication.libraryPaths():
            if lp.endswith('/plugins'):
                print('removed:"{}"'.format(lp))
            else:
                libpaths.append(lp)
        # if 'plugins' exists as a folder in QtCore's dir, enforce it
        libpaths.append(ppath)
        QtCore.QCoreApplication.setLibraryPaths(libpaths)
        print('added:  "{}"'.format(ppath))

adjust_pluginpath()

# basic test to see if the gui is able to open an image.
# PySide seems to implement QtCore and QtGui basis alone.
# No error message when you try to load an image :-(

def _test_load_image():
    this_dir = os.path.dirname(__file__)
    tiffpath = os.path.join(this_dir, 'test', '1bpp.tiff')
    assert os.path.exists(tiffpath)
    image = QtGui.QImage(tiffpath)
    assert not image.isNull(), 'I think Qt is not correctly installed'

_test_load_image()
