# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

"""
definitions for the CDB database
"""

dos_encoding = 'cp850'
encoding = dos_encoding



"""
Commented IFD dump

Tools used:
    tiffinfo (OS X)
    hex fiend

Websites used:
    http://www.awaresystems.be/imaging/tiff/faq.html
    http://partners.adobe.com/public/developer/en/tiff/TIFF6.pdf

>>> for i in range(0, len(d), 12):
...   print(struct.unpack('HHII', d[i:i+12]))
...
(254, 4, 1, 0)     # subfile_type = 0
(256, 3, 1, 1264)  # image_width
(257, 3, 1, 825)   # image_length
(258, 3, 1, 1)     # bits_per_sample
(259, 3, 1, 4)     # compression = 4 FAX
(262, 3, 1, 0)     # Photometric Interpretation: min-is-white
(266, 3, 1, 1)     # FillOrder: msb-to-lsb
(269, 2, 80, 354)  # DocumentName: REZEPT
(270, 2, 20, 434)  # ImageDescription: F-_RNN_DPI200_B/W
(271, 2, 40, 454)  # Make: WALTHER DATA GmbH Scan-Solutions
(272, 2, 40, 494)  # Model: WALTHER MDT100/SM100U Image-System
(273, 4, 1, 840)   # StripOffsets
(274, 3, 1, 1)     # Orientation: row 0 top, col 0 lhs
(280, 3, 1, 0)     # Min Sample Value: 0
(281, 3, 1, 1)     # Max Sample Value: 1
(277, 3, 1, 1)     # Samples/Pixel: 1
(278, 4, 1, 825)   # Rows/Strip: 825
(279, 4, 1, 796)   # Strip Bytecounts
(282, 5, 1, 338)   # XResolution, Rational
(283, 5, 1, 346)   # YResolution, Rational
(285, 2, 80, 534)  # PageName: 30330200002024
(293, 4, 1, 0)     # Group 4 Options: (0 = 0x0)  (T6Options)
(296, 4, 1, 2)     # ResolutionUnit: Inch
(305, 2, 40, 614)  # Software: WALTHER MDT100/SM100U Windows-Library
(306, 2, 20, 654)  # DateTime: 27.03.2013 08:42:25
(315, 2, 80, 674)  # Artist: D. Wünsch Scanprogramm
(316, 2, 80, 754)  # HostComputer: ARZ Wünsch GmbH
"""

"""
Types
The field types and their sizes are:
1 = BYTE 8-bit unsigned integer.
2 = ASCII 8-bit byte that contains a 7-bit ASCII code; the last byte
    must be NUL (binary zero).
3 = SHORT 16-bit (2-byte) unsigned integer.
4 = LONG 32-bit (4-byte) unsigned integer.
5 = RATIONAL Two LONGs: the first represents the numerator of a
    fraction; the second, the denominator.
6 = SBYTE An 8-bit signed (twos-complement) integer.
7 = UNDEFINED An 8-bit byte that may contain anything, depending on
    the definition of the field.
8 = SSHORT A 16-bit (2-byte) signed (twos-complement) integer.
9 = SLONG A 32-bit (4-byte) signed (twos-complement) integer.
10 = SRATIONAL Two SLONG’s: the first represents the numerator of a
     fraction, the second the denominator.
11 = FLOAT Single precision (4-byte) IEEE format.
12 = DOUBLE Double precision (8-byte) IEEE format.
     These new field types are also governed by the byte order (II or MM) in the
     TIFF header
"""

"""
Summary of Tiff problems: (see the dump above)

- The Tiff tags are not sorted as specified in
    http://partners.adobe.com/public/developer/en/tiff/TIFF6.pdf
    First occurence: Tag #277

- The Tiff data type is not always correct as well, for instance:
    Tag #296 should be a short, but is a long.

- The Tiff tags should be in ascending order.
    First violation: Tag #277

- Tiff string data can only contain the final NULL byte.
    This is violated by all strings in this format, which uses a fixed
    maximum length for strings.

All these issues are handled by Tiffany.
But in the context of CDB_Tool, we try to mimick the flawed behavior
of the proprietary software.

Without handling these issues, PySide/PyQt ignores the Tiff information
when the first wrong tag is encountered, causing weird effects.
"""
