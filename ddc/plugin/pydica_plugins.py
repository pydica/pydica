# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from srw.formvalidation.generic.form_validation import FormValidation
from srw.formvalidation.generic.pydica_schema import PydicaSchema

from ddc.lib.log_proxy import l_
from .loader import PluginLoader


__all__ = ['init_plugin_loader', 'PydicaPlugin']

class PydicaPlugin(object):
    display_name = 'generic pydica plugin'

    def initialize(self, context, **kwargs):
        """
        Plugin loader will call this method at startup after all enabled
        plugins are loaded and the basic context was populated.
        """
        pass

    def batch_metadata(self, batch=None, bunch=None):
        """
        Return batch-specific meta data. The information is passed into the
        validation context (as "batch_meta").
        """
        return {}

    def form_metadata(self, form, form_position, context):
        """
        Return form-specific meta data. The information is passed into the
        validation context (as "form_meta").
        """
        return {}

    def detect_formtype(self, formdata, *, context=None):
        """
        Return the likely form type based on the form values.
        """
        return None


    def form_validator(self, *, formtype, context):
        """
        Return a FormValidation instance for the given form type.
        """
        return FormValidation(schema=PydicaSchema())


def init_plugin_loader(enabled_plugins=('*', ), log=None):
    return PluginLoader('pydica.plugin', enabled_plugins=enabled_plugins, log=l_(log))
