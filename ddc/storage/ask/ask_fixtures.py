# -*- coding: utf-8 -*-
"""
ASK Fixtures can generate ASK binary structures completely in memory for
testing purposes.
"""
from __future__ import division, absolute_import, print_function, unicode_literals

from srw.rdblib.fixture_helpers import UnclosableBytesIO


__all__ = ['create_ask']

def create_ask():
    return UnclosableBytesIO(b'')

