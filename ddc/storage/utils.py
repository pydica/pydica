# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals


__all__ = ['DELETE', 'FIELDS_INI']

import pkg_resources
FIELDS_INI = pkg_resources.resource_filename('srw.pydica.fields', 'FIELDS.INI')

class DELETE(object):
    pass
