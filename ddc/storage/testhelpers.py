# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from contextlib import contextmanager
from io import BytesIO
import os
import shutil
from tempfile import mkdtemp

from srw.rdblib import assemble_new_path, guess_path, ibf_subdir, path_info_from_cdb, DataBunch
from srw.rdblib.cdb import create_cdb_with_dummy_data, CDBFile, CDBForm
from srw.rdblib.cdb.cdb_fixtures import create_cdb_with_form_values
from srw.rdblib.ibf import create_ibf

from ddc.client.config import ALL_FIELD_NAMES
from ddc.lib.attribute_dict import AttrDict
from ddc.storage.ask import create_ask
from .batch import Batch
from ddc.storage.sqlite import create_sqlite_db
from ddc.validation.testutil import generate_pic, valid_prescription_values


__all__ = [
    'batch_with_pic_forms',
    'create_bunch',
    'create_cdb_and_ibf_file',
    'fake_tiff_handler',
    'ibf_mock',
    'temporary_copy',
    'use_tempdir',
]

def create_bunch(nr_forms, tasks=(), model=None):
    return DataBunch(
        cdb=create_cdb_with_dummy_data(nr_forms=nr_forms, field_names=ALL_FIELD_NAMES),
        ibf=create_ibf(nr_images=nr_forms),
        db=create_sqlite_db(tasks=tasks, model=model),
        ask=create_ask(),
    )

def _update_attr(container, **kwargs):
    for attr_name, value in kwargs.items():
        setattr(container, attr_name, value)

def ibf_mock(pics):
    def _create_entry(pic):
        if not isinstance(pic, str):
            pic = pic[0]

        data = AttrDict({
            'rec': AttrDict(codnr=pic),
        })
        data['update_rec'] = lambda codnr=None: _update_attr(data.rec, codnr=codnr)
        return data
    return AttrDict(
        image_entries=[_create_entry(pic) for pic in pics],
        update_entry = lambda x: None
    )

def fake_tiff_handler(pic):
    if not isinstance(pic, str):
        pic = pic[1]

    long_data = AttrDict({
        'rec': AttrDict(page_name=pic),
    })
    long_data['update_rec'] = lambda page_name=None: _update_attr(long_data.rec, page_name=page_name)
    return AttrDict(
        long_data=long_data,
        long_data2=AttrDict(
            rec=AttrDict(page_name=pic)
        ),
    )


def create_cdb_and_ibf_file(cdb_path, form_data=None, *, ibf_dir=None, pic_nrs=None):
    """Create a xDB file and a corresponding IBF."""
    assert (pic_nrs is None) ^ (form_data is None)
    if form_data is None:
        form_data = pic_nrs
    _form_data = []
    for i, data in enumerate(form_data):
        is_pic = isinstance(data, str)
        if is_pic:
            pic_nr = data
            extra_fields = {}
        else:
            pic_nr = generate_pic(scan_nr=i+1)
            extra_fields = data
        form_values = valid_prescription_values(**extra_fields, with_pic=pic_nr)
        _form_data.append(form_values)
    pic_nrs = [form_values['pic'] for form_values in _form_data]

    cdb_fp = create_cdb_with_form_values(_form_data, filename=cdb_path)
    cdb_fp.close()

    if ibf_dir is None:
        ibf_path = guess_path(cdb_path, 'IBF')
        ibf_dir = os.path.dirname(ibf_path)
    else:
        ibf_path = assemble_new_path(cdb_path, new_dir=ibf_dir, new_extension='IBF')
    os.makedirs(ibf_dir, exist_ok=True)
    ibf_fp = create_ibf(
        nr_images=len(pic_nrs),
        pic_nrs=pic_nrs,
        fake_tiffs=False,
        filename=ibf_path
    )
    ibf_fp.close()
    return (cdb_path, ibf_path)


def batch_with_pic_forms(pics, *, model=None):
    def _form(pic):
        fields = [
            {'name': 'AUSSTELLUNGSDATUM', 'corrected_result': '01.01.2016'}
        ]
        if not isinstance(pic, str):
            pic = pic[0]
        cdb_form = CDBForm(fields, imprint_line_short=pic)
        return cdb_form

    forms = [_form(pic) for pic in pics]
    cdb_fp = BytesIO(CDBFile(forms).as_bytes())
    databunch = DataBunch(
        cdb=cdb_fp,
        ibf=create_ibf(nr_images=len(pics)),
        db=create_sqlite_db(model=model),
        ask=None,
    )
    batch = Batch.init_from_bunch(databunch, create_persistent_db=False, access='read')
    batch.ibf = ibf_mock(pics)
    batch._tiff_handlers = [fake_tiff_handler(pic) for pic in pics]
    return batch


@contextmanager
def use_tempdir():
    tempdir_path = mkdtemp()
    yield tempdir_path
    shutil.rmtree(tempdir_path)

@contextmanager
def temporary_copy(cdb_path):
    """
    Copy the specified CDB file (and the corresponding IBF) to a temporary
    location and yield its path.

    Some of the tests here either modify CDB data (delete) or may fail to run
    concurrently due to exclusive locking of the data files.
    The real solution is to synthesize fixtures using 'create_cdb()' et al
    but the quick fix works for now (as these tests should be ported anyway
    to test cdb_tool:FormBatch instead)
    didn't use discover_lib + BunchAssembler to keep the code as simple as
    possible right now (and using guess_* methods means I have to deal with
    casing issues on Linux).
    """
    cdb_source_path = cdb_path
    with use_tempdir() as temp_path:
        cdb_source_dir, basename = path_info_from_cdb(cdb_source_path)
        ibf_source_path = os.path.join(cdb_source_dir, ibf_subdir, basename+'.IBF')
        shutil.copy(cdb_source_path, temp_path)

        ibf_exists = os.path.exists(ibf_source_path)
        if ibf_exists:
            ibf_target_dir = os.path.join(temp_path, ibf_subdir)
            os.makedirs(ibf_target_dir)
            shutil.copy(ibf_source_path, ibf_target_dir)

        cdb_target_path = os.path.join(temp_path, os.path.basename(cdb_source_path))
        yield cdb_target_path

