# -*- coding: utf-8 -*-
# Copyright 2016, 2017 Felix Schwarz
# The source code in this file is licensed under the MIT license.

import warnings

try:
    import progressbar
except ImportError:
    progressbar = None


__all__ = ['CLIProgress', 'ProgressRecorder', 'SuppressProgress']

class SuppressProgress(object):
    def __init__(self, max_value=None):
        self.max_value = max_value
        self.is_cancelled = False
        self.result = None
        self.current_value = 0

    @classmethod
    def as_factory(cls):
        return SuppressProgress

    def set_max_value(self, max_value):
        pass

    def update(self, i, details=None):
        self.current_value = i

    def increment_by(self, n):
        self.current_value += n

    def increment_by_one(self):
        self.current_value += 1

    def finish(self, result=None):
        self.result = result

    def abort(self):
        self.is_cancelled = True


class CLIProgress(SuppressProgress):
    def __init__(self, name, max_value):
        super(CLIProgress, self).__init__(max_value=max_value)
        self.name = name
        self._bar = None

    @classmethod
    def as_factory(cls, name):
        return lambda max_value: cls(name, max_value)

    def set_max_value(self, max_value):
        raise NotImplementedError('not yet implemented')

    def _start(self):
        if progressbar is None:
            self._bar = SuppressProgress(self.name, self.max_value)
            return
        widgets = [
            self.name,
            ' ',
            progressbar.Bar(marker='=', left='[', right=']'),
            '  ', progressbar.ETA()
        ]
        self._bar = progressbar.ProgressBar(widgets=widgets, maxval=self.max_value)
        self._bar.start()

    def update(self, i, details=None):
        if self._bar is None:
            self._start()
        # The progressbar library will raise an exception if "i" is set to a
        # higher value than "max_value". While this is a bug in our code I
        # don't to abort the process due to that (which might render some hours
        # computation useless).
        if i > self.max_value:
            msg = 'CLIProgress: i=%r is higher than max value (%r) set previously' % (i, self.max_value)
            warnings.warn(msg)
            i = self.max_value
        self._bar.update(i)

    def increment_by(self, n):
        super(CLIProgress, self).increment_by(n)
        self.update(self.current_value)

    def increment_by_one(self):
        super(CLIProgress, self).increment_by_one()
        self.update(self.current_value)

    def finish(self, result=None):
        self._stop_progress(value=self.max_value)
        self.result = result

    def _stop_progress(self, value=None):
        if self._bar is None:
            return
        if value is not None:
            self._bar.update(value)
        self._bar.finish()
        self._bar = None
        self.name = None
        self.max_value = None

    def abort(self):
        self.is_cancelled = True


class ProgressRecorder(object):
    def __init__(self):
        self.updates = []
        self.max_value = None
        self.current_value = 0
        self.result = None
        self.is_cancelled = False
        self.is_finished = False

    def __call__(self, max_value):
        # This is helpful to use a class instance as "factory"
        self.set_max_value(max_value)
        return self

    def set_max_value(self, max_value):
        self.max_value = max_value

    def update(self, i, details=None):
        assert (self.is_finished == False)
        self.updates.append(i)

    def increment_by(self, n):
        self.current_value += n
        self.update(self.current_value)

    def increment_by_one(self):
        self.current_value += 1
        self.update(self.current_value)

    def finish(self, result=None):
        self.is_finished = True
        self.result = result

    def abort(self):
        self.is_cancelled = True
