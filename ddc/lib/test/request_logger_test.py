# -*- coding: utf-8 -*-
# Copyright (c) 2016 Felix Schwarz
# The source code contained in this file is licensed under the MIT license.

from contextlib import contextmanager
import shutil
import sys
from tempfile import mkdtemp

from pythonic_testcase import *

from ..request_logger import format_exception, RequestLogger
from ..attribute_dict import AttrDict

@contextmanager
def use_tempdir():
    tempdir_path = mkdtemp()
    yield tempdir_path
    shutil.rmtree(tempdir_path)

class RequestLoggerTest(PythonicTestCase):
    def test_can_format_exception(self):
        e = self._exc_info(u'just a simple exception - with umlauts äöüß')
        traceback_bytes = format_exception(e)
        assert_isinstance(traceback_bytes, bytes)
        assert_contains(b'just a simple exception', traceback_bytes)

    def test_can_store_report(self):
        e = self._exc_info('interesting info')
        request = AttrDict(
            method='GET',
            path_qs='/',
            http_version='1.0',
            headers={},
        )

        with use_tempdir() as temp_path:
            logger = RequestLogger(request, exc_info=e)
            report_filename = logger.create_and_store_report(temp_path)
            with open(report_filename, 'rb') as fp:
                report = fp.read()
            assert_contains(b'interesting info', report)

    def _exc_info(self, exception_message='foo'):
        try:
            raise ValueError(exception_message)
        except:
            return sys.exc_info()
