# -*- coding: UTF-8 -*-
# Copyright 2016 Felix Schwarz
# The source code in this file is licensed under the MIT license.

import sys

from .request_logger import format_exception, store_exception_report


__all__ = ['install_except_hook']

class ExceptionReporter(object):
    def __init__(self, application, excepthook, *, store_reports=False, abort_on_error=False):
        self._application = application
        self._previous_excepthook = excepthook
        self._store_reports = store_reports
        self._abort_on_error = abort_on_error

    def __call__(self, *exc_info):
        if self._store_reports:
            traceback_string = format_exception(exc_info)
            summary = traceback_string.strip().rsplit(b'\n', 1)[-1]
            context = self._application.context_for_exception_report()
            env_path = getattr(context, 'env_path', None)
            extra_info = getattr(context, 'extra_info', None)
            report_path = store_exception_report(env_path, summary,
                exc_info=exc_info,
                context_string=extra_info
            )
            if hasattr(self._application, 'notify_about_unhandled_exception'):
                self._application.notify_about_unhandled_exception(report_path)

        if self._previous_excepthook is not None:
            self._previous_excepthook(*exc_info)
        if self._abort_on_error:
            sys.exit(43) # off by one ;-)

def install_except_hook(application, **kwargs):
    old_handler = sys.excepthook
    sys.excepthook = ExceptionReporter(application, old_handler, **kwargs)
