# -*- coding: UTF-8 -*-
# Copyright 2013-2014, 2016 Felix Schwarz
# The source code in this file is licensed under the MIT license.

from __future__ import absolute_import, unicode_literals

try:
    from configparser import SafeConfigParser
except ImportError:
    from ConfigParser import SafeConfigParser
from io import StringIO
import six

import pkg_resources


__all__ = ['SiteConfig']

def is_file_like(fp):
    return hasattr(fp, 'read') and hasattr(fp, 'seek')

class SiteConfigItem(object):
    def __init__(self, filename=None, package_name=None, is_mandatory=False):
        self.filename = filename
        self.package_name = package_name
        self.is_mandatory = is_mandatory
    
    def fp(self):
        try:
            if self.package_name:
                config_fp = pkg_resources.resource_stream(self.package_name, self.filename)
            elif is_file_like(self.filename):
                buffer_ = self.filename
                buffer_.seek(0, 0)
                config_fp = buffer_
            else:
                config_fp = open(self.filename, 'rb')
        except IOError:
            if self.is_mandatory:
                raise
            config_fp = StringIO()
        return config_fp
    
    def clone(self):
        return SiteConfigItem(
            filename=self.filename,
            package_name=self.package_name,
            is_mandatory=self.is_mandatory,
        )
    
    @classmethod
    def from_anything(cls, reference, is_mandatory=False):
        if isinstance(reference, SiteConfigItem):
            config_item = reference.clone()
            config_item.is_mandatory = is_mandatory
        elif isinstance(reference, six.string_types):
            config_item = SiteConfigItem(filename=reference, is_mandatory=is_mandatory)
        elif isinstance(reference, dict):
            fake_config = '[app:main]\n'
            for key, value in reference.items():
                fake_config += '%s = %s\n' % (key, value)
            config_item = SiteConfigItem(filename=StringIO(fake_config), is_mandatory=is_mandatory)
        else:
            package_name, filename = reference
            config_item = SiteConfigItem(
                package_name=package_name,
                filename=filename,
                is_mandatory=is_mandatory
            )
        return config_item


class SiteConfig(object):
    def __init__(self, mandatory=(), optional=(), interpolations=None):
        self._interpolations = interpolations or dict()
        self._config = SafeConfigParser()
        self.update(mandatory=mandatory, optional=optional)
    
    def update(self, mandatory=(), optional=()):
        for reference in mandatory:
            self._update_config(reference, is_mandatory=True)
        for reference in optional:
            self._update_config(reference)
    
    def _update_config(self, reference, is_mandatory=False):
        if reference is None:
            return
        config_item = SiteConfigItem.from_anything(reference, is_mandatory=is_mandatory)
        config = config_item.fp().read()
        if isinstance(config, bytes):
            config = config.decode('utf-8')
        config_fp = StringIO(config)
        self._config.readfp(config_fp)
    
    def to_dict(self, section_name, interpolations=None):
        if not self._config.has_section(section_name):
            return dict()
        settings = dict()
        _interpolations = self._interpolations.copy()
        if interpolations:
            _interpolations.update(interpolations)
        raw_values = (not _interpolations)
        for key in self._config.options(section_name):
            value = self._config.get(section_name, key, raw=raw_values, vars=_interpolations)
            settings[key] = value
        return settings

    def sections_as_dicts(self, interpolations=None):
        """Return all config sections as dictionary (section name as primary key)"""
        configs = {}
        for section_name in self._config.sections():
            section_dict = self.to_dict(section_name, interpolations=interpolations)
            configs[section_name] = section_dict
        return configs

    def to_string(self):
        buffer_ = StringIO()
        for section_name in self._config.sections():
            buffer_.write('\n[%s]\n' % section_name)
            for key in self._config.options(section_name):
                value = self._config.get(section_name, key, raw=True)
                setting_string = '%s = %s\n' % (key, value)
                buffer_.write(setting_string)
        buffer_.seek(0)
        return buffer_.read()

