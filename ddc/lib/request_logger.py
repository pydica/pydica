# -*- coding: UTF-8 -*-
# Copyright 2013-2014, 2016 Felix Schwarz
# The source code in this file is licensed under the MIT license.
#
# basic idea and some code copied from the TurboGears 1 manual:
# http://turbogears.org/1.0/docs/ErrorReporting.html#method-3-application-wide-catch-all-with-cherrypy-filter

from __future__ import unicode_literals

from datetime import datetime as DateTime
import getpass
from io import BytesIO, StringIO
import os
import sys
import time
import traceback

from .attribute_dict import AttrDict


__all__ = ['format_exception', 'store_exception_report', 'RequestLogger']


def format_exception(exc_info):
    """
    Return a traceback for the given exception as bytes (byte string in Python 2)
    """
    exc_type, exc_value, exc_traceback = exc_info
    PY2 = (sys.version_info < (3,0))
    if PY2:
        buffer_ = BytesIO()
        traceback.print_exception(exc_type, exc_value, exc_traceback, file=buffer_)
        tb_bytes = buffer_.getvalue()
    else:
        buffer_ = StringIO()
        traceback.print_exception(exc_type, exc_value, exc_traceback, file=buffer_)
        tb_bytes = buffer_.getvalue().encode('utf8')
    return tb_bytes.strip() + b'\n\n'


def store_exception_report(env_path, summary, *, exc_info=None, headers={}, context_string=None, username=None):
    if not env_path:
        return
    exception_dir = os.path.join(env_path, 'exceptions')
    if not os.path.exists(exception_dir):
        os.makedirs(exception_dir)

    fake_request = AttrDict(
        method=summary,
        path_qs='',
        http_version='',
        headers=headers,
    )
    if username is None:
        # os.getlogin() returned a FileNotFoundError with Python 3.5 (Fedora 24)
        # "getpass" was the most portable replacement which worked for me.
        username = getpass.getuser()
    logger = RequestLogger(fake_request,
        username=username,
        exc_info=exc_info,
        extra_info=(context_string or u'').encode('utf8'),
    )
    return logger.create_and_store_report(exception_dir)


class RequestLogger(object):
    def __init__(self, request, response_code=None, body=None, username=None,
                 exc_info=None, extra_info=None, response=None, ignore_headers=()):
        self.request = request
        self.response_code = response_code
        self.body = body
        self.username = username
        self.exc_info = exc_info
        self.extra_info = extra_info
        self.response = response
        self.ignore_headers = ignore_headers

    def create_report(self, buffer_=None):
        if buffer_ is None:
            buffer_ = BytesIO()
        self._build_report(buffer_)
        buffer_.seek(0)
        return buffer_

    def create_and_store_report(self, base_directory):
        now = DateTime.now()
        storage_path = os.path.join(base_directory, '%04d' % now.year, '%02d' % now.month, '%02d' % now.day)
        if not os.path.exists(storage_path):
            os.makedirs(storage_path)
        logging_filename = os.path.join(storage_path, str(time.time())+'.txt')
        with open(logging_filename, 'wb') as report_fp:
            self._build_report(report_fp)
        return logging_filename

    def _build_report(self, buffer_):
        request_line = '%s %s %s' % (self.request.method, self.request.path_qs, self.request.http_version)
        lines = [
             str(DateTime.now()),
             request_line
        ]
        if self.username:
            lines += ['User: %s' % self.username]
        if self.response_code:
            lines.append('=> %s' % self.response_code)
        lines += ['', 'Request Headers'] + self._format_headers() + ['', '']
        lines_string = '\n'.join(lines)
        buffer_.write(lines_string.encode('utf-8'))
        self._format_body(buffer_)
        self._store_extra_info(buffer_)
        self._store_response(buffer_)
        if self.exc_info:
            formatted_traceback = None
            if isinstance(self.exc_info, tuple):
                formatted_traceback = format_exception(self.exc_info)
            else:
                formatted_traceback = self.exc_info
            buffer_.write(formatted_traceback)

    def _format_headers(self):
        data = []
        for key, value in self.request.headers.items():
            if key in self.ignore_headers:
                continue
            data.append('  %s: %r' % (key, value))
        return data

    def _format_body(self, buffer_):
        if not self.body:
            return

        buffer_.write(b',--- REQUEST BODY -----------------------------------------------\n',)
        has_seek_and_tell = hasattr(self.body, 'tell') and hasattr(self.body, 'seek')
        if has_seek_and_tell:
            previous_file_position = self.body.tell()
            self.body.seek(0)
        buffer_.write(self.body.read())
        if has_seek_and_tell:
            self.body.seek(previous_file_position)
        buffer_.write(b'\n`----------------------------------------------------------------\n',)
        buffer_.write(b'\n')

    def _store_extra_info(self, buffer_):
        if not self.extra_info:
            return

        buffer_.write(b',--- additional info-----------------------------------------------\n',)
        extra_info = self.extra_info
        if hasattr(self.extra_info, 'read'):
            extra_info = self.extra_info.read()
        bytes_info = extra_info
        if hasattr(extra_info, 'encode'):
            bytes_info = extra_info.encode('utf-8')
        buffer_.write(bytes_info)
        buffer_.write(b'\n`----------------------------------------------------------------\n',)
        buffer_.write(b'\n')

    def _store_response(self, buffer_):
        if not self.response:
            return

        buffer_.write(b',--- RESPONSE BODY -----------------------------------------------\n',)
        buffer_.write(self.response.body)
        buffer_.write(b'\n`----------------------------------------------------------------\n',)
        buffer_.write(b'\n')

