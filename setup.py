#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import sys

if sys.version_info < (3,3):
    sys.stderr.write('setup.py must be run with Python 3.3+.\n')
    sys.exit(1)

from setuptools import setup, find_packages


def requires_from_file(filename):
    requirements = []
    with open(filename, 'r') as requirements_fp:
        for line in requirements_fp.readlines():
            match = re.search('^\s*([a-zA-Z][^#]+?)(\s*#.+)?\n$', line)
            if match:
                name = match.group(1)
                comment = match.group(2) or ''
                if comment.startswith('#egg='):
                    name = comment.rsplit('=', 1)[1]
                requirements.append(name)
    return requirements

setup(
    name='pydica',
    version='2018.6.4',

    zip_safe=False,
    packages=find_packages(),
    include_package_data=True,
    tests_require=requires_from_file('dev_requirements.txt'),
    install_requires=requires_from_file('requirements.txt'),
    entry_points = {
        'console_scripts': [
            'start-introducer = ddc.tool.datamanager.introducer:scan_and_init',
            'pydica-gui = ddc.client.val_app.__main__:main',
            'pydica-ediview = ddc.client.val_app.ediview:main',
            'find-broken-form = ddc.tool.util.find_broken_form:main'
        ],
    }

)
