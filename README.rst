Pydica
------

This repository contains the GUI for our document capture software.
The GUI helps verifying the captured data (e.g. because of OCR errors
or the actual input data is incorrect).

