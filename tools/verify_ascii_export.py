#!/usr/bin/env python3

from datetime import datetime as DateTime
import hashlib
import os
import sys
from tempfile import NamedTemporaryFile

from srw.walther.ascii_export import export_ascii_to_path

from ddc.storage import guess_path, FIELDS_INI
from ddc.tool.cdb_tool import FormBatch

MIN_DIFFERENCE_SECONDS = 5


def main(argv=None):
    if argv is None:
        argv = sys.argv
    cdb_dir = argv[1]

    for filename in sorted(os.listdir(cdb_dir)):
        cdb_path = os.path.join(cdb_dir, filename)
        if not os.path.isfile(cdb_path) or not cdb_path.upper().endswith('.CDB'):
            continue
        ascii_path = guess_path(cdb_path, 'asc')
        if not os.path.exists(ascii_path):
            print('no ASCII found for %s' % filename)
        walther_data = open(ascii_path, 'rb').read()
        walther_md5 = hashlib.md5(walther_data).hexdigest()

        cdb = FormBatch(cdb_path, delay_load=False, access='read')
        ascii_filename = os.path.basename(ascii_path)
        with NamedTemporaryFile(delete=True) as temp_fp:
            export_ascii_to_path(cdb, temp_fp, FIELDS_INI)
            temp_fp.seek(0)
            pydica_data = temp_fp.read()

        pydica_md5 = hashlib.md5(pydica_data).hexdigest()
        if walther_md5 == pydica_md5:
            print('%s: OK (%s)' % (ascii_filename, walther_md5))
        else:
            print('%s: DIFFERENT (Walther: %s / pydica: %s)' % (ascii_filename, walther_md5, pydica_md5))
            cdb_mtime = os.stat(cdb_path).st_mtime
            asc_mtime = os.stat(ascii_path).st_mtime
            cdb_dt = DateTime.fromtimestamp(cdb_mtime)
            asc_dt = DateTime.fromtimestamp(asc_mtime)
            if (cdb_mtime > asc_mtime) and (cdb_mtime - asc_mtime > MIN_DIFFERENCE_SECONDS):
                print('    CDB modified after ASC (%s vs. %s)' % (cdb_dt, asc_dt))



if __name__ == '__main__':
    main(sys.argv)
