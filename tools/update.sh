#!/bin/bash

# this nice bash functionality enables us to call this script
# from any directory (previously it assumed the current working
# directory was the directory where this script is located).
THISDIR="$( dirname "${BASH_SOURCE[0]}" )"
cd "${THISDIR}"/..

pybabel update \
	--locale=de --previous \
	--input-file=ddc/i18n/pydica.pot \
	--output-file=ddc/i18n/de/LC_MESSAGES/pydica.po

