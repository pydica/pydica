#!/usr/bin/env python3
"""cdb-to-ascii

Usage:
  cdb-to-ascii [options] <CDB>

Options:
  -h --help        Show this screen.
  --overwrite      overwrite ASC file if it exists already
  --asc-in-current-dir  store the ASC file in the current working directory
"""

import os
import sys

try:
    from docopt import docopt
except ImportError:
    sys.stderr.write('Bitte manuell das "docopt"-Modul nachinstallieren.\n')
    sys.exit(1)
from srw.walther.ascii_export import export_ascii_to_path

from ddc.storage import guess_path, FIELDS_INI
from ddc.tool.cdb_tool import FormBatch


def main(argv=None):
    if argv is None:
        argv = sys.argv
    arguments = docopt(__doc__, argv=argv[1:])
    cdb_path = arguments['<CDB>']
    overwrite = arguments.get('--overwrite', False)
    if not os.path.exists(cdb_path):
        sys.stderr.write('Datei nicht gefunden: %s\n' % cdb_path)
        sys.exit(10)
    if not cdb_path.upper().endswith('.CDB'):
        sys.stderr.write('%s ist vermutlich keine CDB-Datei.\n' % cdb_path)
        sys.exit(11)
    guessed_asc_path = guess_path(cdb_path, 'asc')
    asc_filename = os.path.basename(guessed_asc_path)
    if arguments.get('--asc-in-current-dir'):
        asc_path = os.path.abspath(asc_filename)
    else:
        asc_path = guessed_asc_path
    if os.path.exists(asc_path) and (not overwrite):
        sys.stderr.write('ASCII-Datei existiert bereits: %s\n' % asc_path)
        sys.exit(12)

    cdb = FormBatch(cdb_path, delay_load=False, access='read')
    with open(asc_path, 'wb') as asc_fp:
        export_ascii_to_path(cdb, asc_fp, FIELDS_INI)
    print('ASC-Datei %s geschrieben' % asc_path)


if __name__ == '__main__':
    main(sys.argv)
