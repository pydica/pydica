Links
=====

Home
----

- `pydica <http://www.pydica.org>`_

bitbucket
---------

- `pydica <https://bitbucket.org/pydica/pydica>`_

OpenHub
--------

- `pydica <https://www.openhub.net/p/pydica>`_

