.. pydica-docs documentation master file, created by
   sphinx-quickstart on Sun Feb  5 13:26:29 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

pydica-Dokumentation
====================

Inhalt:

.. toctree::
   :maxdepth: 2

   big-picture
   components/index
   installation/index
   development
   config_design
   notes/index
   links

Index und Suche
===============

* :ref:`genindex`

.. * :ref:`modindex`

* :ref:`search`

