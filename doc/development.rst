
Changing UI definitions (XML)
=============================

After you updated `.ui` files (e.g. `mainwindow.ui`) you need to regenerate the
corresponding `.py` file (e.g. `ui_mainwindow.py`). Unfortunately pyside's uic
does not use the correct relative import for our custom widgets so that needs
to be fixed separately.

For convenience we have a helper shell script which automates all of that
for `mainwindow.ui`::

    cd ddc/client/val_app/gui/
    ./gen_main_py.sh
    # now ui_mainwindow.py has been updated and can be committed




