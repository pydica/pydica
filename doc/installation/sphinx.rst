Sphinx Documentation Server
===========================

Nachdem das Projekt kopiert wurde, kann die Dokumentation einfach erstellt
werden mit::

    $ cd pydica/doc
    $ python bootstrap.py
    $ ./bin/buildout

#. Erstellen der Dokumentation::

    $ ./bin/sphinxbuilder

Weitere Informationen
---------------------

- `Sphinx documentation <http://sphinx.pocoo.org/contents.html>`_
- `Sphinx reStructuredText Primer <http://sphinx.pocoo.org/rest.html>`_
- `collective.recipe.sphinxbuilder <http://pypi.python.org/pypi/collective.recipe.sphinxbuilder/>`_
- `reStructuredText - Markup Syntax and Parser Component of Docutils <http://docutils.sourceforge.net/rst.html>`_

