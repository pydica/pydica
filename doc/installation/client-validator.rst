Client Validator
================

.. note::
   Zur Installation von Mercurial siehe :doc:`mercurial`.

Windows 7 64 Bit
----------------

#. `python-2.7.3.msi <http://www.python.org/ftp/python/2.7.3/python-2.7.3.msi>`_
   installiert (32 Bit)
#. `setuptools-0.6c11.win32-py2.7.exe <http://pypi.python.org/packages/2.7/s/setuptools/setuptools-0.6c11.win32-py2.7.exe>`_
   installiert
#. `PySide-1.1.0qt474.win32-py2.7.exe <http://www.pyside.org/files/PySide-1.1.0qt474.win32-py2.7.exe>`_
   installiert
#. ``$ python get-pip.py``
#. ``$ pip install virtualenv``
#. ``$ hg clone https://hg.veit-schiele.de/private/pydica``
#. ``$ cd pydoca``
#. ``$ virtualenv .``
#. ``$ Scripts\active.bat``
#. ``$ pip install pycparser configobj pytest mock``
#. die Verzeichnisse ``C:\Python27\Lib\site-packages\PySide`` und
   ``C:\Python27\Lib\site-packages\PySide-1.1.0qt474-py2.7.egg-info``
   in das site-packages-Verzeichnis des virtualenv kopiert
#. die Datei ``tiffcp.exe`` ins Scripts-Verzeichnis des virtualenv
   kopiert
#. ausserhalb des virtualenv ausgefuehrt: ``$ pip uninstall pyside``
#. im globalen site-packages noch alles gelöscht, was nach pyside
   aussieht
#. wieder innerhalb des virtualenv die Umgebungsvariable
   ``QT_PLUGIN_PATH`` auf den Pfad zum Plugin-Verzeichnis des pyside
   im virtualenv gesetzt
#. jetzt klappen sowohl die Tests als auch der Client-Validator

Mac OS X
--------

#. ``$ brew install qt``
#. ``$ brew install python``
#. ``$ pip install mercurial # do _not_ use the brew version -- wrong !!``
#. ``$ brew install python3``
#. ``$ brew install virtualenv``
#. ``$ virtualenv -p python3``
#. ``$ source bin/activate``
#. ``(pydica) $ wget https://bitbucket.org/pydica/pyside-setup/downloads/PySide-1.1.2-py3.3.egg``
#. ``(pydica) $ easy_install PySide-1.1.2-py3.3.egg``
#. ``(pydica) $ bin/pyside_postinstall.py -install``
#. ``(pydica) $ pip install pytest six``

