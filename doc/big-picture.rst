The Big Picture
===============

- Distributed Document Capture
- Plattform für verteiltes Scannen, Erkennen, automatisierte und manuelle Überprüfung, Archivierung
- Skalierbare Client-Server-Architektur
- Verteilung der Aufgaben
- Administration und Monitoring

