Validatoren
===========

Prüfziffernverfahren
--------------------

Institutionskennzeichen (IK)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Zitat von http://de.wikipedia.org/wiki/Institutionskennzeichen:

  Die Institutionskennzeichen (kurz: IK) sind eindeutige, neunstellige Zahlen,
  mit deren Hilfe Abrechnungen im Bereich der deutschen Sozialversicherung
  einrichtungsübergreifend abgewickelt werden können. Hierbei erhalten alle
  Einrichtungen, die Leistungen nach dem Sozialgesetzbuch (SGB) erbringen,
  auf Antrag ein IK.

Im konkreten Fall der Rezeptabrechnung wird das IK für die Leistungserbringer
(Apotheken) und die Kostenträger (Krankenkassen) verwendet.

Das Prüfziffernverfahren ist gut auf Wikipedia dokumentiert:
http://de.wikipedia.org/wiki/Institutionskennzeichen#Pr.C3.BCfverfahren

Krankenversichertennummer (KVNR)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Einstiegspunkte:

 * http://de.wikipedia.org/wiki/Krankenversichertennummer
 * https://kvnummer.gkvnet.de/(S(okkxpu55jdhixt452nidvaqj))/pubpages/krankenversichertennummer.aspx

Relevant für die Prüfung im Rahmen der Rezeptabrechnung sind nur die ersten 10 Stellen
der Krankenversichertennummer, da nur diese auf dem Rezept stehen.

Aus
http://www.gematik.de/cms/media/dokumente/release_0_5_2/release_0_5_2_egk/gematik_eGK_Spezifikation_Musterkarten_und_Testkarten_V280.pdf
wurden von Seite 60 folgende Information entnommen:

  Die erste Stelle der 10 Stellen ist ein Großbuchstabe (A-Z), die nächsten 8 Stellen sind Ziffern (0-9) und
  die letzte Ziffern ist eine Prüfziffer (0-9).

  Der Buchstabe und die 8 Ziffern sind für jede Person „zufällig“, aber eindeutig,  vergeben.
  Werte mit mehr als drei aufeinander folgenden gleichen Ziffern werden ausgeschlossen.
  „Zufällig“ meint hier, dass keine weitere Semantik enthalten ist. ...

  Die Prüfziffer wird mit dem Modulo-10-Verfahren und den Gewichtungen 1-2-1-2-1-2-1-2-1-2
  berechnet. Der Buchstabe wird dabei durch eine zweistellige Zahl ersetzt,
  das A mit 01, das B mit 02, …, und das Z mit 26.

Beispiel: Aus Z629410041 wird für die Prüfung 2662941004, woraus sich dann die
Prüfziffer 1 ergibt.

lebenslange Arztnummer (LANR)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Auszug aus http://de.wikipedia.org/wiki/LANR:

  Die lebenslange Arztnummer, kurz LANR, ist eine neunstellige Nummer, die die zuständige
  Kassenärztliche Vereinigung bundesweit an jeden Arzt vergibt, der an der vertragsärztlichen
  Versorgung (siehe auch GKV) teilnimmt.

Das Prüfziffernverfahren ist unter http://de.wikipedia.org/wiki/LANR#Aufbau
dokumentiert.

Pharmazentralnummer (PZN)
~~~~~~~~~~~~~~~~~~~~~~~~~

Auszug aus http://de.wikipedia.org/wiki/Pharmazentralnummer:

  Die Pharmazentralnummer (PZN) ist ein in Deutschland bundeseinheitlicher
  Identifikationsschlüssel für Arzneimittel und andere Apothekenprodukte.

Seit 01.01.2013 ist die PZN 8-stellig, vorher war sie 7-stellig. Aus den bisherigen
7-Stellern wurde ein 8-Steller, in dem eine 0 vorangestellt wurde. Durch den Aufbau
des Prüfziffernverfahrens ist sichergestellt, dass die Prüfziffer bei der
Erweiterung von 7 auf 8 Stellen nach wie vor die gleiche ist.

Das Prüfziffernverfahren ist in http://www.ifaffm.de/download/IFA-Info_Funktion_und_Aufbau_PZN.pdf
dokumentiert.

Transaktionsnummer (TID)
~~~~~~~~~~~~~~~~~~~~~~~~

Im Rahmen der Rezeptabrechnung erhält ein Rezept eine Transaktionsnummer, wenn parallel
zum Rezept eine Datenlieferung von der Apotheke an das Rechenzentrum erfolgt. Diese
Transaktionsnummer wird auf das Rezept aufgedruckt. Im Rechenzentrum können dann anhand
dieser aufgedruckten Transaktionsnummer die elektronisch gelieferten Daten dem Rezept
zugeordnet werden.

Das Prüfziffernverfahren für die Transaktionsnummer ist in der Technischen Anlage 1
zur Vereinbarung über die Übermittlung von Daten im Rahmen der Arzneimittelabrechnung
gemäß § 300 SGB V beschrieben. Die aktuelle Version des Dokuments ist immer unter
http://www.gkv-datenaustausch.de/, dort unter Leistungserbringer - Apotheken zu finden.

Die zum Zeitpunkt aktuelle Version der Technischen Anlage 1 ist unter
http://www.gkv-datenaustausch.de/media/dokumente/leistungserbringer_1/apotheken/technische_anlagen_aktuell/TA1_023_20121127.pdf
zu finden. Dort wird im Abschnitt 7 auf Seite 25 das Prüfziffernverfahren beschrieben.
