Server-Validator
================

The validation of the forms generates tasks that are passed to the task manager.

Order
-----

Die Reihenfolge der Validierungen ist eindeutig, es muss nicht zwischen
Pre- und Post-Replacements unterschieden werden:

The sequence of the validation is unique. A distinction between prior and
subsequent substitution is not required.

#. Field replacements
#. Validation
#. Form set choice

   The form type is chosen for which a minimum of tasks are available.

Validators
----------

Methods of the field validators:

- Boolean
- Date
- Integer
- Decimal Numbers
- Regular expression
- Minimalwert
- Maximalwert

Methods for the form validators:

- Sum
- Same date or earlier
- Same date or later

Messages
--------

In the messages the reasons for the manual checking should be localizable.

Traceability
------------

In a format to be specified more precisely all validations, replacements
and manual changes has to be saved. Conventional services should be
used for analysis.

Allgemeine Prüfungen
--------------------

Sonderfälle, wie zum Beispiel komplette weiße oder schwarze Images oder
OCR-Ergebnisse, die weit außerhalb des "Normalen" liegen, sollten ein
extra Event werfen.

Innerhalb eines Belegstapels soll geprüft werden, ob es die gleiche PIC-Nr.
mehrmals gibt - das darf auf keinen Fall passieren (kommt aber trotzdem
selten vor, zuletzt im Abrechnungsmonat 04/2012). Gibt es eine PIC-Nr.
mehrmals, so müssen sicherheitshalber alle Belege mit dieser PIC-Nr. gelöscht
werden. Die Prüfung auf doppelte PIC-Nr. wird in nachfolgenden Prozessen über
alle Monatsrezepte durchgeführt, je eher ein solcher Fehler auffällt, desto
besser. Das Löschen der betroffenen Rezepte muß interaktiv erfolgen.

Für Felder, die nur Text enthalten und sonst nicht weiter überprüft werden,
muß es trotzdem eine Prüfung auf gültige Zeichen geben. Für die Felder Name,
Straße und Ort sind zum Beispiel die Zeichen ``:``, ``+``, ``?`` und ``'``
nicht zulässig, da diese Zeichen bei der Datenlieferung an die Krankenkassen
eine besondere Bedeutung haben (Feldtrenner etc.) und nicht an allen Stellen
in der bestehenden Software sichergestellt ist, daß diese Zeichen ordentlich
escaped werden.

Feldübergreifende Prüfungen
---------------------------

PZN mehrfach auf dem Rezept
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Kommt eine PZN innerhalb eines Belegs mehrfach vor, so muß darauf hingewiesen
werden und dem Benutzer überlassen werden, ob das in Ordnung ist. Nur in sehr
seltenen Fällen kommt das vor, meistens ist es ein Fehler der aktuellen OCR,
die eine Zeile verdoppelt hat.

Sonderbehandlung Wirkstoff Isotretinoin
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Arzneimittel, die den Wirkstoff Isotretinoin enthalten(zum Beispiel Isoderm)
dürfen für Frauen im gebährfähigen Alter nur abgegeben werden, wenn das
Ausstellungsdatum des Rezepts nicht älter als 7 Tage ist.

Anhand der Daten auf dem Rezept ist nicht eindeutig bestimmbar, ob es sich
um eine Frau handelt. Deswegen sollte ein Rezept zur Anzeige gebracht werden,
wenn alle folgenden Bedingungen erfüllt sind:

- Wirkstoff einer PZN auf dem Rezept ist Isotretinoin
- Zwischen Abgabedatum und Ausstellungsdatum des Rezepts liegen mehr als
  7 Tage
- Anhand des Geburtsdatums wird ein Alter ermittelt, mit welchem sich eine
  Frau im gebärfähigen Alter befindet

Weitere Infos gibt es unter
http://www.hautzone.ch/dermatologie/Seborrhoic/aknebehandlung.htm unter dem
Stichwort "Tetragenes Risiko".

Als Beispielrezept, welches von der Kasse wegen Überschreitung der Abgabefrist
abgesetzt wurde, kann die PIC-Nr. 10518100436 dienen.

