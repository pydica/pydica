================
Client-Validator
================

Configuration
=============

The client can be configured so that it can view and edit only tasks with
certain classifications.

User Interface
==============

Dialogs
-------

Open
~~~~

#. Only those files are available for selection, for which tickets of the
   configured classifications are available.
#. When opening the first page is displayed for which a ticket with an
   appropriate classification will be available.

Keyboard shortcuts
------------------

**Ctrl-O (Strg-O)**
    Oeffnen einer RDB- bzw. CDB-Datei, die alle Daten enthaelt
**F3**
    Springt innerhalb der gerade geoffneten RDB- bzw. CDB-Datei zu einem
    bestimmten Beleg. Dazu wird das Feld unten in der Statusleiste, in dem die
    aktuelle Belegnummer innerhalb der Datei angezeigt wird, aktiviert, so das
    dort Belegnummer eingegeben werden kann. Durch druecken von Enter wird dann
    der entsprechende Beleg geladen und angezeigt.
**PgUp** (Bild-nach-oben)
    springt zum vorhergehenden Bele
**PgDn** (Bild-nach-unten)
    springt zum naechsten Beleg
**Cursor up** (Cursor-nach-oben)
    springt zum vorhergehenden Feld innerhalb des Belegs; ist das erste Feld im
    Beleg erreicht, so wird der vorhergehende Beleg geladen und dort in das
    letzte Feld gesprungen
**Cursor down** (Cursor-nach-unten)
    springt zum naechsten Feld innerhalb des Belegs; ist das letzte Feld im
    Beleg erreicht, wird der naechste Beleg geladen und dort in das erste Feld
    gesprungen
**CR** (Enter)
    gleiche Wirkung wie "Cursor down"

Misc
----

- Ein großer Abstand zwischen Scan und Text verlangsamt die Überprüfung.
- Heute besitzen die Monitore üblicherweise eine deutlich höhere Auflösung, sodass ein besserer Überblick gegeben werden kann.
- So kann das Originalbild links dargestellt werden und rechts daneben der zu korrigierende Text im annähernd selben Layout.

  Hier würde sich ein Configuration-Editor empfehlen um Layoutanpassungen schnell vornehmen zu können. 

- Anzeige von leicht verständlichen Statusmeldungen mit den Gründen für die manuelle Überprüfung.


Sonderbefehle
-------------

PZN / Faktor / Taxe ab 4. Zeile ergänzen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Aktuell werden von der OCR nur die ersten drei Zeilen erkannt. Bei vielen
Rezepten, die 4 Positionen oder mehr haben, stimmt dadurch die Bruttosumme
nicht (wenn nicht eine Taxe durch die OCR automatisch korrigiert wurde -
das sollte unbedingt bald abgeschaltet werden!).

Es sollte in einem solchen Fall eine einfache Möglichkeit geben, um PZN,
Faktor und Taxe ab der 4. Zeile nachzuerfassen.

Ein Sonderfall tritt ein, wenn ein Betrag von 26, 52 oder 78 Cent am
Brutto fehlen - dann ist es mit ziemlicher Sicherheit ein BTM-Rezept
mit der Sonder-PZN 02567024 und dem Faktor 1, 2 oder 3. Hierfür sollte
es noch einen besonderen Modus geben, der einem die Tipparbeit für
diesen recht häufig vorkommenden Fall so weit wie möglich abnimmt. Ein
gutes Beispiel ist der Stapel 55/304 im Abrechnungsmonat 02/2013.
