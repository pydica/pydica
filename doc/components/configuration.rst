Konfiguration
=============

Der Configuration-Editor erlaubt die Konfiguration der Komponenten :doc:`ocr`,
:doc:`server-validator` und :doc:`client-validator` über ``*.ini``-Dateien.

Im Einzelnen enthalten diese Dateien folgende Angaben:

Form Sets
---------

- Die Konfiguration kann mehrere Arten von Formularen (Form Sets)
  enthalten.
- Jedes Form Set kann von einem anderen abgeleitet werden und nur die
  abweichenden Angaben enthalten.
- Jedes Form Set besteht aus folgenden Angaben:

  - eine oder mehrere Seiten
  - Feldern
  - Feldübergreifende Validatoren
  - Feldübergreifende Ersetzungen

Formularfelder
--------------

Folgende Angaben sind für Felder möglich

… für den Client-Validator
~~~~~~~~~~~~~~~~~~~~~~~~~~

- ID und lokalisierbares Label eines Feldes
- Jedes Formularfeld enthält die exakte Position des Feldes, deren Höhe
  und Breite.
  Zum aktuellen Zeitpunkt ist Pixel (px) die einzig zulässige Maßangabe.
- Darüberhinaus kann für jedes Feld angegeben werden, in welchem Bereich
  um das Feld die OCR durchgeführt werden soll.
- Konfiguration der Anzeige

  - Standardschrift (ist keine angegeben, wird die Standardschrift des
    Betriebssystems verwendet).
  - Für jedes Feld soll die Standardschrift mit einer eigenen Schriftangabe
    überschrieben werden können
  - Mindestbreite eines Feldes in Anzahl der Zeichen

… für den Server-Validator
~~~~~~~~~~~~~~~~~~~~~~~~~~

- Liste der Klassennamen der Validatoren für ein einzelnes Feld.

… für die Ersetzungen
~~~~~~~~~~~~~~~~~~~~~

- Liste der Klassennamen der Ersetzungen, wobei die Reihenfolge
  bedeutend ist.

  Dabei wird unterschieden zwischen Ersetzungen von Feldern und
  Form Sets.

