Task Manager
============

Initially, the task manager shall provide the paths to the data bases and
some useful methods.

Paths
-----

The following attributes are configurable according to the
Uniform Naming Convention (UNC):

Protocol
  This may be for instance ``file:`` for access to the local file system,
  but may also set conventions for real database access.
Storage Server
  In case of the ``file:`` protocol, it may be the identifier of the drive
  (i.E. drive letter on windows).
Path Components
  Path to the data base as specification of the partial component
File name
  Calculated name of the data base file

Categories
==========

Categories are extensible. The set of available categories is individual
to a context.

The lowest set of categories has the following default hierarchy:

- ``batch_ident``

  identification of a batch of forms

- ``form_ident``

  identification of a form in a batch

- ``page_ident``

  identification of a page in a form

Contexts
========

A context is associated with a name space, typically negotiated
with a customer.

The categories in a context define the way how this context is
organized.

Examples for categories in a context are:

- ``dsz.pzn``
- ``dsz.date_format``
- ``dsz.date_of_issue``

Generic API
===========

The generic api is flexible and gets its specialization by a
customer plugin. Every plugin has a set of required categories
which need to be inquired in order to use the plugin.


API
====

The current API is oriented by a hierarchy of categories which is
extensible.

The Task Manager provides the following interface for enquiring individual
contexts, batches and forms:

``batch_list``
  lists the data bases of batches in a path
``form_list``
  lists the form identifiers in a data bases of batches in a path
``open_form``
  opens a form for processing
``form_last_editor``
  returns the ID of the last editor of a form
``form_state``
  returns the current state of the form

  Possible states are:

  - initial *
  - processing/locked
  - finished


