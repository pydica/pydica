Ersetzungen
===========

Ersetzungen innerhalb von Feldern
---------------------------------

- Ersetzen von Werten, z.B. 'G' durch '6'
- Ersetzen von regulären Ausdrücken

Ersetzungen innerhalb von Formularen
------------------------------------

- Ersetze Werte eines Feldes durch die Werte eines anderen Feldes

Ersetzung von Formularen
------------------------

- Ersetze den aktuellen Formulartyp durch einen anderen

