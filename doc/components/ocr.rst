.. index::
   single: OCR

OCR
===

.. index::
   single: Tesseract
   pair: OCR; Tesseract

Tesseract
---------

Tesseract ist eine freie Implementierung, die von HP entwickelt wurde und
jetzt von Google übernommen wurde.

 * Home: https://code.google.com/p/tesseract-ocr/
 * Python-Bindings: http://code.google.com/p/python-tesseract/

weitere Links:

 * http://isbullsh.it/2012/06/Automatic-tesseract-training/
 * http://stackoverflow.com/questions/4763956/performance-issues-using-tesseract-ocr-from-a-python-application
 * http://wiki.ubuntuusers.de/OCRFeeder

