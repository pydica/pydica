.. index::
   single: Config Design

Config Design
=============


About Configuration
-------------------

Configuration is a concept that has several different aspects. Here are some
general thoughts, which are currently in preparation for the application.

- config of individual GUI objects

  Despite of general decisions about when, where and how to specify
  configuration info, the configuration of certain objects needs to be done,
  regardless.
  
  Before driving any global decisions about the application, the following
  structure makes sense:
  
  - a GUI object has a config object, where its default options come from
    (font size, colors, font family, ...)

  - these settings do not need to be specified in the first place. Instead,
    every GUI entity initializes itself by using settings stored in some
    Python class. The settings can be supplied at object creation time. A
    default settings object should be defined in the same module. The GUI
    object should use this settings object as default, unless a different one is
    supplied by the invoking application.
    
    
- config of the whole application

  The application needs to know certain settings. While for many of the settings
  some defaults can be provided, certain things need to be defined, somewhere.
  Our approach is to use .ini files to make config data persistent.
  
  There are many different approaches, including configparser, ConfigObj,
  Qt's QSettings and others. We try to use a minimalistic compromise which does
  not rely on a certain feature of one implementation.

- storage of configuration

  We are using the ideas of Qt's QSettings objects, without being dependent of
  the Qt implementation. For the location of config files, the Qt documentation
  can be consulted. We use a similar hierarchy of .ini file lookups, stored in
  OS dependent default locations.
  
  We use a flat hierarchy of config sections this way to build trees:
  A config section may be named [default], or it has a special name syntax.
  There is no deeper nesting level than one.

  [to be continued]
