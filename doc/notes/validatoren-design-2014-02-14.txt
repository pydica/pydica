
Konzept Validatoren

Definition: "Validatoren" sind automatische Prozesse zur Datenprüfung. Ein
Validator gibt nur ein Prüfergebnis zurück, startet aber keine eigenen Aktionen
bzw. verändert keine Werte.
Technisch ist die "Validierung" wie im folgenden beschrieben eine "library",
daher auch keine direkte Nutzerinteraktion.

Ziel des Dokuments:
 - grundsätzliche Design-Überlegungen zu Validatoren dokumentieren
 - gemeinsames Verständnis schaffen
 - Diskussionsgrundlage, damit wir Probleme möglichst früh erkennen und nicht
   erst nach der Entwicklung


- Wir wollen die Validierungsalgorithmen so gestalten, dass wir die sowohl in
  einem eigenständigen Prozess (Validierung im pydica-Workflow) also auch ggf.
  direkt im pydica GUI-Client (direktes Feedback für den Nutzer) verwenden
  können (später eventuell auch in Verbindung mit dem SOAP-Server).
- Die Validierung arbeitet eng mit der Plausibilisierung zusammen.
- Bei der Prüfung darf das Ergebnis nicht nur "korrekt/falsch" sein, sondern
  wir benötigen möglichst viele Infos über das Ergebnis, z.B.:
    - Fehlercode ("keine Zahl", "Wert zu klein", ...)
    - idealerweise auch "Fehler beim 2. Zeichen"
- Die Validierung soll mit einer Prüfung immer möglichst viele Fehler erkennen,
  d.h. Feld A und B sind falsch, nicht "Feld A falsch" -> ändern -> "Feld B falsch".
- Am Anfang tendiere ich dazu, die pydica-Validatoren direkt im Code
  hinzuschreiben (nicht vom Administrator per Konfig änderbar), weil dies
  vermutlich einfacher/schneller geht und ich auch noch nicht verstanden habe,
  welche Flexibilität man in einem eventuellen anderen pydica-Einsatzszenario
  benötigt.
- Jede Validierungsregel (z.B. "PZN-Feld") wird modular gebaut (z.B. eigene
  Klasse), so dass man komplexere Validierungen später auch durch Komposition
  von Einzelregeln zusammenbauen kann.
- Idealerweise sollen Mehrfeldvalidatoren (z.B. Summe) auch ablaufen können,
  zumindest solange alle benötigten Einzelfelder valide sind.
  (Alternative: Mehrfeldvalidatoren laufen erst ab, wenn alle Einzelfelder
  valide sind.)
  => Ein Mehrfeldvalidator muss angeben, auf welche Eingabewerte er sich bezieht.
- Es muss möglich sein, nur einen einzelnen Wert zu validieren.


noch unklar:
- Benötigen wir zwingend Prüfungen über einen gesamten Rezeptstapel?
  Eindeutigkeit der vergebenen ID?



