OS X Hints
==========

Bei OS X mit QtSDK 4.8.0 funktioniert PySide nicht.
Abhilfe: ::

    sudo cp -pvR /Developer/QtSDK/Desktop/Qt/4.8.0/gcc/lib/*.framework /Library/Frameworks 

GUI Design Notizen
------------------

Das Gui wird mit Qt Designer entworfen, solange dies effizient moeglich ist.
Der Einstieg in den Designer ist etwas schwierig; der Designer ist teilweise
problematisch zu handhaben.

Die meiste Dokumentation bezieht sich auf C++ - Code, und eine groessere
Huerde ist, die Umsetzung in PySide zu finden.

Einen Einstieg bietet zum Beispiel

    http://developer.qt.nokia.com/doc/qt-4.8/gettingstartedqt.html

Zum Schreiben kleiner erster Programme eignet sich:

    http://zetcode.com/gui/pysidetutorial/firstprograms/

Eine entsprechende PySide-Dokumentation findet man unter

    http://developer.qt.nokia.com/wiki/PySideDocumentation/
    http://developer.qt.nokia.com/wiki/PySide_Tutorials_by_Experience_Level


Das Wichtigste im Kürze
-----------------------

Es gestaltet sich relativ einfach, eine Oberflaeche Zusammenzuklicken.

Layouts:

  Schwieriger wird es, wenn sich Widgets aneinander ausrichten sollen. Die
  groesste Huerde ist das Verstehen der Layouts. Layouts funktionieren nur
  dann richtig, wenn die Kette von innen nach aussen zusammenhaengt.

Es ist hilfreich, diese Dokument durchzuarbeiten:

    http://developer.qt.nokia.com/doc/qt-4.8/designer-layouts.html

Splitter:
---------

  Es dauert eine Weile, bis man versteht dass Splitter im Designer nicht
  direkt zu sehen sind. Stattdessen sind sie eine spezielle Art Layout.
  
  Stretch-Faktoren:
  
    Jedes Widget hat einen vertikalen und horizontalen Stretch.
    Das ist die Gewichtung der Widgets beim Layout.
    Sehr wichtiger Punkt: innerhalb eines Layouts schnurren widgets mit
    Stretch Faktor 0 auf das Minimum zusammen, wenn andere mehr als 0 haben.


Tips zu Layouts:
----------------

    Wenn ein Widget in einem uebergeordneten Layout sitzt, sind die von diesem
    Layout kontrollierten Attribute gesperrt. Auch ist es oft unmoeglich, in
    diesem Zustand neue Widgets hinzuzufuegen oder deren Anordnung zu aendern.
    
    Dazu gibt es als Kontext-Menue und auch als Tool Button oben "Break Layout".
    Nach "Break Layout" kann es passieren, dass die Controls zu einem Punkt 
    zusammenschnurren, das macht aber nichts da die Controls jetzt wieder bewegt
    werden koennen.
    
    Beachte dass es beliebige Undos gibt. Es ist oft einfacher, eine Reihe von
    Aenderungen zu wiederholen, als es nachtraeglich zu korrigieren.
    
    Es lohnt sich, etwas fummelid zu erstellende Layouts als Template zu
    speichern, um einen definierten Anfang zu haben.


Projektstruktur
---------------

Das GUI soll mit Designer bearbeitbar bleiben, aber im Python/PySide
entwickelt werden.
Damit der Designer weiterbenutzt werden kann, soll sein Output nicht direkt bearbeitet werden. 

Konvention:
Der Designer erzeugt name.ui 
Mit pyside-uic erzeugt man

pyside-uic -x -o name_ui.py name.ui

Man schreibt dann von Hand eine

name.py. (nicht zwingend, empfehlung)

Es gibt verschiedene Möglichkeiten, den Hybrid zu bauen. 

Einfach und etwas "hackish":

    Zusätzliche Methoden definieren und an die Ui Klasse dran-definieren. Siehe das letzte Beispiel. 

Bei grösseren Sachen:

    Klasse ableiten, Methoden überschreiben. 

Als einfaches Template kann das Hauptprogramm kopiert werden, welches pyside-uic durch "-x" generiert. 

Davor importiert man das generierte Form und leitet ein neues ab. 

Um das Zusammenspiel zw. QtDesigner und Python zu vereinfachen, folgender Vorschlag:

Benennung bei allen Widgets veraendern, auf die von Python Bezug genommen wird. Benennung in Englisch. Namen sind small_with_underscore

Widgets die bei der Erzeugung gelöscht werden sollen, haben eine Kennung tmp\_ vornedran. 

Dieser Punkt ist noch nicht ausgereift. Es scheint leicht zu sein, Strukturen zu kopieren, etwa als kleine Widget-Datei. 

Design der dynamischen LineEdits
--------------------------------

Qt hat bei allen Widgets ein Style-Sheet. Dieses konfiguriert das Aussehen der Widgets total und erlaubt zum Beispiel Ändern der Rahmenfarbe. 

Beispiel zum Einstieg:

    http://developer.qt.nokia.com/doc/qt-4.8/stylesheet-examples.html

Alles Weitere dazu:

    http://developer.qt.nokia.com/doc/qt-4.8/stylesheet.html

Funktionierender Prototyp in PySide:

    https://hg.veit-schiele.de/dsz/dsz/file/2407ebb6bf2f/src/designer_test


Neues Markierungsverfahren mit Stylesheets
------------------------------------------

Die Verwendung von Stylesheets ergibt eine bessere Moeglichkeit zur Markierung von
einzelnen Widgets:

Jedem Widget kann ein Stylesheet zogeordnet werden. Auch wenn gar kein Style benutzt wird,
eroeffnet dies beliebige Parametrisierung ueber Kommentare.

Beispiel eines Stylesheets mit Kommentar::

    /* BEGIN tmp_line_input_1 */
    QLineEdit {
         border: 2px solid green;
    }
    QLineEdit:focus {
         border: 2px solid lightgreen ;
	    margin: 1px ;
    }
    QLineEdit:hover {
         border: 2px solid green ;
	    margin: 1px ;
    }

Zum Parsen dieses Strings greift man ueber das Codeobjekt zu, siehe css_test.py::

    css_dict = {}

    # find css arguments with a 'BEGIN' comment
    for const in Ui_Form.setupUi.im_func.func_code.co_consts:
        if isinstance(const, str) and '/* BEGIN' in const:
            widget_name = const.split()[2]
            css_dict[widget_name] = const

