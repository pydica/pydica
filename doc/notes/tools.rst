Tools
=====

Hier sind Merkberger zum Projekt. Weiss noch nicht wohin damit.

Allgemeiner Tip zum Kommando *locate* unter Mac OS X:
-----------------------------------------------------

  das Aktualisieren der Locate-Datenbank geht so:
  
  `sudo /usr/libexec/locate.updatedb`
  
  (aus http://superuser.com/questions/109590/whats-the-equivalent-of-linuxs-updatedb-command-for-the-mac)
  

Über Installation und __main__.py
---------------------------------

  http://sayspy.blogspot.de/2010/03/various-ways-of-distributing-python.html
  
  http://docs.python.org/2/using/cmdline.html#interface-options
  
  http://www.boredomandlaziness.org/2011/03/what-is-python-script.html

Wichtiger Hint zum Bauen von PySide:
------------------------------------

`cmake` hat einen Bug in Version 2.8.10.1:

  ::
  
    Linking CXX executable shiboken
    ld: framework not found QtCore
    clang: error: linker command failed with exit code 1 (use -v to see invocation)
    make[2]: *** [generator/shiboken] Error 1
    make[1]: *** [generator/CMakeFiles/shiboken.dir/all] Error 2
    make: *** [all] Error 2
    error: Error compiling shiboken

Dieser Fehler tritt auf, wenn man mit der aktuellen Version von `homebrew` arbeitet.

Auf die Lösung bin ich durch folgenden Link von Matthew Brett gestossen:

http://permalink.gmane.org/gmane.comp.programming.tools.cmake.user/44595

Der Fehler kann vermieden werden, wenn man die Version folgendermassen zurücksetzt:

  ::
  
    cd /usr/local
    brew versions cmake

      2.8.10.1 git checkout b5942ec Library/Formula/cmake.rb
      2.8.9    git checkout 54ff55c Library/Formula/cmake.rb
      2.8.10   git checkout d6d8b3e Library/Formula/cmake.rb
      ...

    git checkout 54ff55c Library/Formula/cmake.rb
    brew switch cmake 2.8.9
    
    cmake --version
    
      cmake version 2.8.9
      
Der Hinweis stammt aus

  http://stackoverflow.com/questions/3987683/homebrew-install-specific-version-of-formula
  
  Hinweis mit 5 Steps.
  
Eine Versionsprüfung wird in meine neue Version des PySide - Installers eingebaut.
Dieser Installer wird unter

  https://bitbucket.org/pydica/pyside-setup
  
verfügbar.


..
    Tiff-Tools
    ----------
    
    Zum Zugriff auf Multipage-Tiff wurden diverse Ansaetze getestet.
       
    Es gibt einen Patch zu PIL, mit dem sich Fax 3/4 Objekte lesen lassen.
    Diese koennen aber nur gelesen werden.
       
       Das momentane Mittel der Wahl ist *tiffcp*. Leider ist tiffcp nicht
       per default in OS X verfuegbar, kann aber per MacPorts installiert werden.
       Alternativ empfehle ich das in OS X vorinstallierte *tiffutil*. Die
       Konfiguration des Projekts benutzt *tiffcp* unter Linux und Windows,
       und *tiffutil* unter OS X.
       
       Bereitstellung von *tiffcp* unter Windows
       -----------------------------------------
       
       Um Selber-Kompilieren zu vermeiden empfiehlt sich der Download von einer
       Verifizierten Website. Folgende Url enthaelt ein Demo eines kommerziellen
       Produkts, das aber *tiffcp.exe* als binary enthaelt:
       
       ::
       
          http://www.verypdf.com/tif2pdf/image2pdf_emf2pdf_cmd.zip
    
       Man kopiere dann *tiffcp.exe* in den Pfad.
    
    Befehl zum Auskopieren einer bestimmten Seite:
       
       Windows/Linux:
       
       ::
       
          tiffcp Image000.TIF,0 seite1.tiff
          tiffcp Image000.TIF,1 seite2.tiff
    
    
       OS X:
       
       ::
       
          tiffutil -extract 0 Image000.TIF -out seite1.tiff
          tiffutil -extract 1 Image000.TIF -out seite2.tiff
   
Zu installierende Pakete
------------------------

   tools zum Testen:  
   
   ::
   
      pip install mock pytest
   
   tools zum Parsen:

   ::
   
      pip install pycparser
      
   Dabei wird automatisch das phantastische *ply* Packet von David
   Beazley mitinstalliert. Um die Beispiele von *pycparser* auszuprobieren,
   sollte man dennoch das Quellpaket laden:
   
   ::
   
      http://code.google.com/p/pycparser/
      
   Das Quellpaket enthält auch den Praeprozessor *cpp.exe*. 
      
      
   Zur Installation von *pip* siehe
   
   ::
   
      http://www.pip-installer.org/en/latest/installing.html#prerequisites
      
Noch nicht endgültig entschiede Pakete
--------------------------------------

Lesen der Ini-Dateien geht ganz gut mit dem eingebauten *ConfigParser*. Eine
gute Alternative ist evtl. *ConfigObj* von Michael Foord.

::

   pip install configobj

