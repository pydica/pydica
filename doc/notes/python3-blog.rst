Migration zu Python 3.3
=======================

Voraussetzung war, dass es problemlos mit Homebrew geht.

Leider trat bei

    brew install python3

Das Problem auf, dass immer diese Meldung kam:

    Undefined symbols for architecture i386:
      "_libintl_bindtextdomain", referenced from:
          _PyIntl_bindtextdomain in libpython3.3m.a(_localemodule.o)
      "_libintl_dcgettext", referenced from:
          _PyIntl_dcgettext in libpython3.3m.a(_localemodule.o)
      "_libintl_dgettext", referenced from:
          _PyIntl_dgettext in libpython3.3m.a(_localemodule.o)
      "_libintl_gettext", referenced from:
          _PyIntl_gettext in libpython3.3m.a(_localemodule.o)
      "_libintl_setlocale", referenced from:
          _PyLocale_setlocale in libpython3.3m.a(_localemodule.o)
      "_libintl_textdomain", referenced from:
          _PyIntl_textdomain in libpython3.3m.a(_localemodule.o)
    ld: symbol(s) not found for architecture i386
    clang: error: linker command failed with exit code 1 (use -v to see invocation)
    make: *** [Python.framework/Versions/3.3/Python] Error 1
    ==> Configuration
    HOMEBREW_VERSION: 0.9.3
    HEAD: b14a896325a6be555462f11f12466cbdf2959e43
    CPU: 8-core 64-bit sandybridge
    OS X: 10.8.2-x86_64
    Xcode: 4.5.2
    CLT: 4.5.0.0.1.1249367152
    X11: 2.7.4 => /opt/X11
    ==> ENV
    CC: cc
    CXX: c++
    MAKEFLAGS: -j8
    CMAKE_PREFIX_PATH: /usr/local/opt/readline:/usr/local
    CMAKE_INCLUDE_PATH: /usr/include/libxml2:/System/Library/Frameworks/OpenGL.framework/Versions/Current/Headers/
    CMAKE_LIBRARY_PATH: /System/Library/Frameworks/OpenGL.framework/Versions/Current/Libraries
    PKG_CONFIG_PATH: /usr/local/lib/pkgconfig:/usr/local/Library/ENV/pkgconfig/mountain_lion
    ACLOCAL_PATH: /usr/local/share/aclocal
    OBJC: cc
    PATH: /usr/local/Library/ENV/4.3:/usr/local/opt/pkg-config/bin:/usr/local/opt/sqlite/bin:/usr/local/opt/gdbm/bin:/usr/local/opt/python/bin:/usr/bin:/bin:/usr/sbin:/sbin
    
    Error: python3 did not build
    Logs:       /Users/tismer/Library/Logs/Homebrew/python3/config.log


Auf die Lösung bin ich indirekt hier gestossen:
http://mail.python.org/pipermail/python-dev/attachments/20100307/3fa5390b/attachment.html

Das Problem was gettext, was schon von irgendwo installiert war.
Also:

    brew install gettext

Homebrew erzählt dann, dass das Paket schon da sei und es nicht gelinkt würde!
Aha, also

    brew link gettext

Das hat sich einigermassen gesträubt, bis ich gettext weggeputzt hatte.

Danach kamen diverse Fehler durch Überreste einer vorherigen python3 Installation,
aber das war alles schrittweise behebbar.